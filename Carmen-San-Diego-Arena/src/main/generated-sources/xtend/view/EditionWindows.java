package view;

import Operations.Operations;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure0;
import org.uqbar.arena.layout.HorizontalLayout;
import org.uqbar.arena.layout.VerticalLayout;
import org.uqbar.arena.widgets.Button;
import org.uqbar.arena.widgets.Panel;
import org.uqbar.arena.windows.Dialog;
import org.uqbar.arena.windows.WindowOwner;

@SuppressWarnings("all")
public abstract class EditionWindows<T extends Object> extends Dialog<T> {
  public EditionWindows(final WindowOwner owner, final T model) {
    super(owner, model);
  }
  
  public void createMainTemplate(final Panel mainPanel) {
    this.giveTitle();
    final Panel editorPanel = new Panel(mainPanel);
    VerticalLayout _verticalLayout = new VerticalLayout();
    editorPanel.setLayout(_verticalLayout);
    this.addList(editorPanel);
    final Panel botonEliminar = new Panel(editorPanel);
    HorizontalLayout _horizontalLayout = new HorizontalLayout();
    botonEliminar.setLayout(_horizontalLayout);
    this.addBotonDelete(botonEliminar);
    this.addInputYBoton(editorPanel);
    final Panel botonAgregar = new Panel(editorPanel);
    HorizontalLayout _horizontalLayout_1 = new HorizontalLayout();
    botonAgregar.setLayout(_horizontalLayout_1);
    Operations.addBoton(botonAgregar, "Aceptar", this.aceptarMetodo());
  }
  
  public Button addInputYBoton(final Panel panel) {
    Button _xblockexpression = null;
    {
      final Panel panelAux = new Panel(panel);
      HorizontalLayout _horizontalLayout = new HorizontalLayout();
      panelAux.setLayout(_horizontalLayout);
      Operations.addTextBox(panelAux, this.caracteristicaEnModel());
      _xblockexpression = Operations.addBoton(panelAux, "Agregar", this.metodoAgregarEnModel());
    }
    return _xblockexpression;
  }
  
  public String caracteristicaEnModel() {
    return "nuevaCaracteristica";
  }
  
  public String componenteEnModel() {
    return "caracteristicaSeleccionada";
  }
  
  public void addBotonDelete(final Panel panel) {
    Operations.addBoton(panel, "Eliminar", this.metodoEliminarEnModel());
  }
  
  public void addList(final Panel panel) {
    Operations.addList(panel, this.tituloDeListaAEditar(), this.listaAEditarEnModel(), this.componenteEnModel());
  }
  
  public abstract void giveTitle();
  
  public abstract String listaAEditarEnModel();
  
  public abstract Procedure0 metodoAgregarEnModel();
  
  public abstract Procedure0 metodoEliminarEnModel();
  
  public abstract String tituloDeListaAEditar();
  
  protected void createFormPanel(final Panel mainPanel) {
  }
  
  public Procedure0 aceptarMetodo() {
    final Procedure0 _function = new Procedure0() {
      public void apply() {
        EditionWindows.this.close();
      }
    };
    return _function;
  }
}
