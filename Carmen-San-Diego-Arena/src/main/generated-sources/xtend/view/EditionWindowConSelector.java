package view;

import Operations.Operations;
import org.uqbar.arena.bindings.Adapter;
import org.uqbar.arena.layout.HorizontalLayout;
import org.uqbar.arena.widgets.Button;
import org.uqbar.arena.widgets.Panel;
import org.uqbar.arena.windows.WindowOwner;
import view.EditionWindows;

@SuppressWarnings("all")
public abstract class EditionWindowConSelector<T extends Object> extends EditionWindows<T> {
  public EditionWindowConSelector(final WindowOwner owner, final T model) {
    super(owner, model);
  }
  
  public Button addInputYBoton(final Panel panel) {
    Button _xblockexpression = null;
    {
      final Panel panelAux = new Panel(panel);
      HorizontalLayout _horizontalLayout = new HorizontalLayout();
      panelAux.setLayout(_horizontalLayout);
      Operations.addSelector(panelAux, this.listaDondeSeleccionarEnModel(), this.componenteEnModel(), this.adapterNecesario());
      _xblockexpression = Operations.addBoton(panelAux, "Agregar", this.metodoAgregarEnModel());
    }
    return _xblockexpression;
  }
  
  public void addList(final Panel panel) {
    Operations.addList(panel, this.tituloDeListaAEditar(), this.listaAEditarEnModel(), this.caracteristicaSeleccionada(), 
      this.adapterNecesario());
  }
  
  public abstract String caracteristicaSeleccionada();
  
  public abstract Adapter adapterNecesario();
  
  public abstract String componenteEnModel();
  
  public abstract String listaDondeSeleccionarEnModel();
  
  public abstract String caracteristicaEnModel();
}
