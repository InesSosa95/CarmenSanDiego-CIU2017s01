package view;

import Operations.Operations;
import applicationModel.ExpedienteApplicationModel;
import applicationModel.JuegoApplicationModel;
import dominio.Lugar;
import dominio.Pais;
import org.eclipse.xtext.xbase.lib.ObjectExtensions;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure0;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1;
import org.uqbar.arena.bindings.NotNullObservable;
import org.uqbar.arena.bindings.PropertyAdapter;
import org.uqbar.arena.layout.ColumnLayout;
import org.uqbar.arena.layout.HorizontalLayout;
import org.uqbar.arena.layout.VerticalLayout;
import org.uqbar.arena.widgets.Label;
import org.uqbar.arena.widgets.Panel;
import org.uqbar.arena.windows.Dialog;
import org.uqbar.arena.windows.WindowOwner;
import org.uqbar.lacar.ui.model.ControlBuilder;
import view.ExpedienteWindowVista;
import view.OrdenDeArrestoWindow;
import view.ViajarWindow;
import view.VisitarLugarWindow;

@SuppressWarnings("all")
public class ResolverMisterioWindow extends Dialog<JuegoApplicationModel> {
  public ResolverMisterioWindow(final WindowOwner owner, final JuegoApplicationModel model) {
    super(owner, model);
  }
  
  protected void createFormPanel(final Panel mainPanel) {
    throw new UnsupportedOperationException("TODO: auto-generated method stub");
  }
  
  public void createMainTemplate(final Panel mainPanel) {
    String _objeto = this.getModelObject().getCaso().getObjeto();
    String _plus = ("Resolviendo: " + _objeto);
    this.setTitle(_plus);
    Panel editorPanel = new Panel(mainPanel);
    VerticalLayout _verticalLayout = new VerticalLayout();
    editorPanel.setLayout(_verticalLayout);
    Panel botonera = new Panel(editorPanel);
    ColumnLayout _columnLayout = new ColumnLayout(2);
    botonera.setLayout(_columnLayout);
    Panel labelIzq = new Panel(botonera);
    VerticalLayout _verticalLayout_1 = new VerticalLayout();
    labelIzq.setLayout(_verticalLayout_1);
    Panel nombrePanel = new Panel(labelIzq);
    HorizontalLayout _horizontalLayout = new HorizontalLayout();
    nombrePanel.setLayout(_horizontalLayout);
    Operations.addText(nombrePanel, "Estás en: ");
    Operations.addTextProperty(nombrePanel, "detective.paisActual.nombre");
    final Procedure0 _function = new Procedure0() {
      public void apply() {
        JuegoApplicationModel _modelObject = ResolverMisterioWindow.this.getModelObject();
        new OrdenDeArrestoWindow(ResolverMisterioWindow.this, _modelObject).open();
      }
    };
    Operations.addBoton(labelIzq, "Orden de Arresto", _function);
    Panel ordenPanel = new Panel(labelIzq);
    HorizontalLayout _horizontalLayout_1 = new HorizontalLayout();
    ordenPanel.setLayout(_horizontalLayout_1);
    Label _addText = Operations.addText(ordenPanel, "Orden ya emitida: ");
    final Procedure1<Label> _function_1 = new Procedure1<Label>() {
      public void apply(final Label it) {
        NotNullObservable _notNullObservable = new NotNullObservable("ordenDeArresto.nombre");
        it.<Object, ControlBuilder>bindEnabled(_notNullObservable);
      }
    };
    ObjectExtensions.<Label>operator_doubleArrow(_addText, _function_1);
    Label _addTextProperty = Operations.addTextProperty(ordenPanel, "ordenDeArresto.nombre");
    final Procedure1<Label> _function_2 = new Procedure1<Label>() {
      public void apply(final Label it) {
        NotNullObservable _notNullObservable = new NotNullObservable("detective.casoAResolver.ordenDeArresto");
        it.<Object, ControlBuilder>bindEnabled(_notNullObservable);
      }
    };
    ObjectExtensions.<Label>operator_doubleArrow(_addTextProperty, _function_2);
    final Procedure0 _function_3 = new Procedure0() {
      public void apply() {
        JuegoApplicationModel _modelObject = ResolverMisterioWindow.this.getModelObject();
        new ViajarWindow(ResolverMisterioWindow.this, _modelObject).open();
      }
    };
    Operations.addBoton(labelIzq, "Viajar", _function_3);
    final Procedure0 _function_4 = new Procedure0() {
      public void apply() {
        ExpedienteApplicationModel _expedienteApplicationModel = new ExpedienteApplicationModel();
        new ExpedienteWindowVista(ResolverMisterioWindow.this, _expedienteApplicationModel).open();
      }
    };
    Operations.addBoton(labelIzq, "Expedientes", _function_4);
    Panel labelDer = new Panel(botonera);
    VerticalLayout _verticalLayout_2 = new VerticalLayout();
    labelDer.setLayout(_verticalLayout_2);
    Operations.addText(labelDer, "Lugares");
    final Procedure0 _function_5 = new Procedure0() {
      public void apply() {
        ResolverMisterioWindow.this.crearVentanaLugar(ResolverMisterioWindow.this.getModelObject().getLugar(0), ResolverMisterioWindow.this.getModelObject().getCaso().getNombreDelCaso()).open();
      }
    };
    Operations.addBoton(labelDer, this.getModelObject().getNombreLugar(0), _function_5);
    final Procedure0 _function_6 = new Procedure0() {
      public void apply() {
        ResolverMisterioWindow.this.crearVentanaLugar(ResolverMisterioWindow.this.getModelObject().getLugar(1), ResolverMisterioWindow.this.getModelObject().getCaso().getNombreDelCaso()).open();
      }
    };
    Operations.addBoton(labelDer, this.getModelObject().getNombreLugar(1), _function_6);
    final Procedure0 _function_7 = new Procedure0() {
      public void apply() {
        ResolverMisterioWindow.this.crearVentanaLugar(ResolverMisterioWindow.this.getModelObject().getLugar(2), ResolverMisterioWindow.this.getModelObject().getCaso().getNombreDelCaso()).open();
      }
    };
    Operations.addBoton(labelDer, this.getModelObject().getNombreLugar(2), _function_7);
    Panel recorridoPanel = new Panel(editorPanel);
    VerticalLayout _verticalLayout_3 = new VerticalLayout();
    recorridoPanel.setLayout(_verticalLayout_3);
    PropertyAdapter _propertyAdapter = new PropertyAdapter(Pais.class, "nombre");
    Operations.addList(recorridoPanel, "Recorrido criminal: ", "recorridoCriminal", _propertyAdapter);
    Panel destinosFallados = new Panel(editorPanel);
    VerticalLayout _verticalLayout_4 = new VerticalLayout();
    destinosFallados.setLayout(_verticalLayout_4);
    Operations.addText(destinosFallados, "Destinos fallidos: ");
    PropertyAdapter _propertyAdapter_1 = new PropertyAdapter(Pais.class, "nombre");
    Operations.addList(destinosFallados, "País", "destinosFallidos", _propertyAdapter_1);
  }
  
  public VisitarLugarWindow crearVentanaLugar(final Lugar primerLugar, final String nombreDelCaso) {
    VisitarLugarWindow visitarLugarDialog = new VisitarLugarWindow(this, primerLugar);
    visitarLugarDialog.setTitle("Resolviendo: ".concat(nombreDelCaso));
    return visitarLugarDialog;
  }
}
