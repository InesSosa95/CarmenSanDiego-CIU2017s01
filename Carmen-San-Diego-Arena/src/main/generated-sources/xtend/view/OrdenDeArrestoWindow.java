package view;

import Operations.Operations;
import applicationModel.JuegoApplicationModel;
import dominio.Villano;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.ObjectExtensions;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure0;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1;
import org.uqbar.arena.bindings.NotNullObservable;
import org.uqbar.arena.bindings.PropertyAdapter;
import org.uqbar.arena.layout.HorizontalLayout;
import org.uqbar.arena.widgets.Label;
import org.uqbar.arena.widgets.Panel;
import org.uqbar.arena.windows.WindowOwner;
import org.uqbar.lacar.ui.model.ControlBuilder;
import view.EditionGameWindow;

@SuppressWarnings("all")
public class OrdenDeArrestoWindow extends EditionGameWindow {
  public OrdenDeArrestoWindow(final WindowOwner owner, final JuegoApplicationModel model) {
    super(owner, model);
  }
  
  public void obtenerTitulo() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("Resolviendo: ");
    String _objeto = this.getModelObject().getDetective().getCasoAResolver().getObjeto();
    _builder.append(_objeto);
    this.setTitle(_builder.toString());
  }
  
  public void agregartexto(final Panel panel) {
    Panel ordenPanel = new Panel(panel);
    HorizontalLayout _horizontalLayout = new HorizontalLayout();
    ordenPanel.setLayout(_horizontalLayout);
    Label _addText = Operations.addText(ordenPanel, "Orden de arresto emitida contra: ");
    final Procedure1<Label> _function = new Procedure1<Label>() {
      public void apply(final Label it) {
        NotNullObservable _notNullObservable = new NotNullObservable("ordenDeArresto");
        it.<Object, ControlBuilder>bindEnabled(_notNullObservable);
      }
    };
    ObjectExtensions.<Label>operator_doubleArrow(_addText, _function);
    Label _addTextProperty = Operations.addTextProperty(ordenPanel, "ordenDeArresto.nombre");
    final Procedure1<Label> _function_1 = new Procedure1<Label>() {
      public void apply(final Label it) {
        NotNullObservable _notNullObservable = new NotNullObservable("ordenDeArresto");
        it.<Object, ControlBuilder>bindEnabled(_notNullObservable);
      }
    };
    ObjectExtensions.<Label>operator_doubleArrow(_addTextProperty, _function_1);
  }
  
  public void agregarBotones(final Panel panel) {
    final Procedure0 _function = new Procedure0() {
      public void apply() {
        OrdenDeArrestoWindow.this.getModelObject().pedirOrdenDeArresto();
        OrdenDeArrestoWindow.this.close();
      }
    };
    Operations.addBoton(panel, "Generar Orden de Arresto", _function);
  }
  
  public void agregarInPut(final Panel panel) {
    Panel selectorPanel = new Panel(panel);
    HorizontalLayout _horizontalLayout = new HorizontalLayout();
    selectorPanel.setLayout(_horizontalLayout);
    Operations.addText(selectorPanel, "Villanos:");
    PropertyAdapter _propertyAdapter = new PropertyAdapter(Villano.class, "nombre");
    Operations.addSelector(selectorPanel, "todosLosVillanos", "villanoSeleccionado", _propertyAdapter);
  }
}
