package view;

import Operations.Operations;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure0;
import org.uqbar.arena.bindings.PropertyAdapter;
import org.uqbar.arena.layout.HorizontalLayout;
import org.uqbar.arena.layout.VerticalLayout;
import org.uqbar.arena.widgets.Panel;
import org.uqbar.arena.windows.Dialog;
import org.uqbar.arena.windows.WindowOwner;

@SuppressWarnings("all")
public abstract class DialogoWindow<T extends Object> extends Dialog<T> {
  public DialogoWindow(final WindowOwner owner, final T model) {
    super(owner, model);
  }
  
  public void botonAceptar(final Panel panel) {
    final Panel botonera = new Panel(panel);
    HorizontalLayout _horizontalLayout = new HorizontalLayout();
    botonera.setLayout(_horizontalLayout);
    Operations.addBoton(botonera, "Aceptar", this.aceptarMetodo());
  }
  
  public void presentarCaracteristica(final Panel editorPanel, final String nombreCaracteristica, final String nombreBoton, final Procedure0 accionBoton, final String nombreDeLista, final String listaEnModelObject, final PropertyAdapter adapter) {
    final Panel caracteristicaPanel = new Panel(editorPanel);
    HorizontalLayout _horizontalLayout = new HorizontalLayout();
    caracteristicaPanel.setLayout(_horizontalLayout);
    Operations.addText(caracteristicaPanel, nombreCaracteristica);
    Operations.addBoton(caracteristicaPanel, nombreBoton, accionBoton);
    final Panel listaCaracteristicaPanel = new Panel(editorPanel);
    VerticalLayout _verticalLayout = new VerticalLayout();
    listaCaracteristicaPanel.setLayout(_verticalLayout);
    Operations.addList(listaCaracteristicaPanel, nombreDeLista, listaEnModelObject, adapter);
  }
  
  public void presentarCaracteristica(final Panel editorPanel, final String nombreCaracteristica, final String nombreBoton, final Procedure0 accionBoton, final String nombreDeLista, final String listaEnModelObject) {
    final Panel caracteristicaPanel = new Panel(editorPanel);
    HorizontalLayout _horizontalLayout = new HorizontalLayout();
    caracteristicaPanel.setLayout(_horizontalLayout);
    Operations.addText(caracteristicaPanel, nombreCaracteristica);
    Operations.addBoton(caracteristicaPanel, nombreBoton, accionBoton);
    final Panel listaCaracteristicaPanel = new Panel(editorPanel);
    VerticalLayout _verticalLayout = new VerticalLayout();
    listaCaracteristicaPanel.setLayout(_verticalLayout);
    Operations.addList(listaCaracteristicaPanel, nombreDeLista, listaEnModelObject);
  }
  
  public abstract Procedure0 aceptarMetodo();
  
  public abstract void giveTitle();
  
  public abstract void addName(final Panel panel);
}
