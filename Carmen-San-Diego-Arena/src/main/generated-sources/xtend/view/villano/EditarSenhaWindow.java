package view.villano;

import applicationModel.VillanoApplicationModel;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure0;
import org.uqbar.arena.windows.WindowOwner;
import view.EditionWindows;

@SuppressWarnings("all")
public class EditarSenhaWindow extends EditionWindows<VillanoApplicationModel> {
  public EditarSenhaWindow(final WindowOwner owner, final VillanoApplicationModel model) {
    super(owner, model);
  }
  
  public void giveTitle() {
    this.setTitle("Editar Señas");
  }
  
  public Procedure0 metodoEliminarEnModel() {
    final Procedure0 _function = new Procedure0() {
      public void apply() {
        EditarSenhaWindow.this.getModelObject().eliminarSenha();
      }
    };
    return _function;
  }
  
  public Procedure0 metodoAgregarEnModel() {
    final Procedure0 _function = new Procedure0() {
      public void apply() {
        EditarSenhaWindow.this.getModelObject().agregarSenha();
      }
    };
    return _function;
  }
  
  public String listaAEditarEnModel() {
    return "villanoSeleccionado.senhas";
  }
  
  public String tituloDeListaAEditar() {
    return "Señas";
  }
}
