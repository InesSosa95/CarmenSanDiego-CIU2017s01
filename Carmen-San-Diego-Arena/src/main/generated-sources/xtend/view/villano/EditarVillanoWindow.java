package view.villano;

import Operations.Operations;
import applicationModel.VillanoApplicationModel;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure0;
import org.uqbar.arena.layout.HorizontalLayout;
import org.uqbar.arena.widgets.Panel;
import org.uqbar.arena.windows.WindowOwner;
import view.villano.VillanoWindow;

@SuppressWarnings("all")
public class EditarVillanoWindow extends VillanoWindow {
  public EditarVillanoWindow(final WindowOwner owner, final VillanoApplicationModel model) {
    super(owner, model);
  }
  
  public void giveTitle() {
    this.setTitle("Expediente - Editar Villano");
  }
  
  public void addName(final Panel panel) {
    final Panel panelAux = new Panel(panel);
    HorizontalLayout _horizontalLayout = new HorizontalLayout();
    panelAux.setLayout(_horizontalLayout);
    Operations.addText(panelAux, "Nombre: ");
    Operations.addTextProperty(panelAux, "villanoSeleccionado.nombre");
  }
  
  public Procedure0 aceptarMetodo() {
    final Procedure0 _function = new Procedure0() {
      public void apply() {
        EditarVillanoWindow.this.close();
      }
    };
    return _function;
  }
}
