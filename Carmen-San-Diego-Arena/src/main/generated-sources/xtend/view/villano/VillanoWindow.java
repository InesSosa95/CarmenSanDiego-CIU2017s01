package view.villano;

import applicationModel.VillanoApplicationModel;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure0;
import org.uqbar.arena.layout.VerticalLayout;
import org.uqbar.arena.widgets.Panel;
import org.uqbar.arena.windows.WindowOwner;
import view.DialogoWindow;
import view.villano.EditarHobbiesWindow;
import view.villano.EditarSenhaWindow;

@SuppressWarnings("all")
public abstract class VillanoWindow extends DialogoWindow<VillanoApplicationModel> {
  public VillanoWindow(final WindowOwner owner, final VillanoApplicationModel model) {
    super(owner, model);
  }
  
  protected void createFormPanel(final Panel mainPanel) {
  }
  
  public void createMainTemplate(final Panel mainPanel) {
    this.giveTitle();
    final Panel editorPanel = new Panel(mainPanel);
    VerticalLayout _verticalLayout = new VerticalLayout();
    editorPanel.setLayout(_verticalLayout);
    this.addName(editorPanel);
    final Procedure0 _function = new Procedure0() {
      public void apply() {
        VillanoApplicationModel _modelObject = VillanoWindow.this.getModelObject();
        new EditarSenhaWindow(VillanoWindow.this, _modelObject).open();
      }
    };
    this.presentarCaracteristica(editorPanel, "Señas", "EditarSeñas", _function, "Señas", "villanoSeleccionado.senhas");
    final Procedure0 _function_1 = new Procedure0() {
      public void apply() {
        VillanoApplicationModel _modelObject = VillanoWindow.this.getModelObject();
        new EditarHobbiesWindow(VillanoWindow.this, _modelObject).open();
      }
    };
    this.presentarCaracteristica(editorPanel, "Hobbies", "Editar Hobbies", _function_1, "Hobbies", "villanoSeleccionado.hobbies");
    this.botonAceptar(editorPanel);
  }
}
