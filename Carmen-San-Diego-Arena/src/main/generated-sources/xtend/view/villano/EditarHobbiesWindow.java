package view.villano;

import applicationModel.VillanoApplicationModel;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure0;
import org.uqbar.arena.windows.WindowOwner;
import view.EditionWindows;

@SuppressWarnings("all")
public class EditarHobbiesWindow extends EditionWindows<VillanoApplicationModel> {
  public EditarHobbiesWindow(final WindowOwner owner, final VillanoApplicationModel model) {
    super(owner, model);
  }
  
  public void giveTitle() {
    this.setTitle("Editar Hobbies");
  }
  
  public Procedure0 metodoEliminarEnModel() {
    final Procedure0 _function = new Procedure0() {
      public void apply() {
        EditarHobbiesWindow.this.getModelObject().eliminarHobbies();
      }
    };
    return _function;
  }
  
  public Procedure0 metodoAgregarEnModel() {
    final Procedure0 _function = new Procedure0() {
      public void apply() {
        EditarHobbiesWindow.this.getModelObject().agregarHobbies();
      }
    };
    return _function;
  }
  
  public String listaAEditarEnModel() {
    return "villanoSeleccionado.hobbies";
  }
  
  public String tituloDeListaAEditar() {
    return "Hobbies";
  }
}
