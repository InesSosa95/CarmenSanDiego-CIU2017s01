package view.villano;

import Operations.Operations;
import applicationModel.VillanoApplicationModel;
import dominio.Expediente;
import dominio.Villano;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure0;
import org.uqbar.arena.layout.HorizontalLayout;
import org.uqbar.arena.widgets.Panel;
import org.uqbar.arena.windows.WindowOwner;
import view.villano.VillanoWindow;

@SuppressWarnings("all")
public class NuevoVillanoWindow extends VillanoWindow {
  public NuevoVillanoWindow(final WindowOwner owner, final VillanoApplicationModel model) {
    super(owner, model);
    Villano _villano = new Villano();
    model.setVillanoSeleccionado(_villano);
  }
  
  public void addName(final Panel panel) {
    Panel panelAux = new Panel(panel);
    HorizontalLayout _horizontalLayout = new HorizontalLayout();
    panelAux.setLayout(_horizontalLayout);
    Operations.addText(panelAux, "Nombre: ");
    Operations.addTextBox(panelAux, "nombre");
  }
  
  public Procedure0 aceptarMetodo() {
    final Procedure0 _function = new Procedure0() {
      public void apply() {
        NuevoVillanoWindow.this.getModelObject().cambiarNombreVillano();
        Expediente.obtenerInstancia().agregarVillano(NuevoVillanoWindow.this.getModelObject().getVillanoSeleccionado());
        NuevoVillanoWindow.this.close();
      }
    };
    return _function;
  }
  
  public void giveTitle() {
    this.setTitle("Expediente - Nuevo Villano");
  }
}
