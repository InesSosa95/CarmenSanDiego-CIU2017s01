package view;

import Operations.Operations;
import applicationModel.JuegoApplicationModel;
import dominio.Pais;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure0;
import org.uqbar.arena.bindings.PropertyAdapter;
import org.uqbar.arena.layout.HorizontalLayout;
import org.uqbar.arena.widgets.Panel;
import org.uqbar.arena.windows.WindowOwner;
import view.EditionGameWindow;

@SuppressWarnings("all")
public class ViajarWindow extends EditionGameWindow {
  public ViajarWindow(final WindowOwner owner, final JuegoApplicationModel model) {
    super(owner, model);
  }
  
  public void obtenerTitulo() {
    this.setTitle("Viajar");
  }
  
  public void agregartexto(final Panel panel) {
    Panel panelAux = new Panel(panel);
    HorizontalLayout _horizontalLayout = new HorizontalLayout();
    panelAux.setLayout(_horizontalLayout);
    Operations.addText(panelAux, "Estas en: ");
    Operations.addTextProperty(panelAux, "detective.paisActual.nombre");
  }
  
  public void agregarInPut(final Panel panel) {
    Panel selectorPanel = new Panel(panel);
    HorizontalLayout _horizontalLayout = new HorizontalLayout();
    selectorPanel.setLayout(_horizontalLayout);
    Operations.addText(selectorPanel, "Conexiones:");
    PropertyAdapter _propertyAdapter = new PropertyAdapter(Pais.class, "nombre");
    Operations.addSelector(selectorPanel, "todasLasConexiones", "paisSeleccionado", _propertyAdapter);
  }
  
  public void agregarBotones(final Panel panel) {
    Panel panelAux = new Panel(panel);
    HorizontalLayout _horizontalLayout = new HorizontalLayout();
    panelAux.setLayout(_horizontalLayout);
    final Procedure0 _function = new Procedure0() {
      public void apply() {
        ViajarWindow.this.close();
      }
    };
    Operations.addBoton(panelAux, "Volver al Pais anterior", _function);
    final Procedure0 _function_1 = new Procedure0() {
      public void apply() {
        ViajarWindow.this.getModelObject().viajar();
        ViajarWindow.this.close();
      }
    };
    Operations.addBoton(panelAux, "Viajar", _function_1);
  }
}
