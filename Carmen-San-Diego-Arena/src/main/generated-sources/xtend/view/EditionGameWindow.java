package view;

import applicationModel.JuegoApplicationModel;
import org.uqbar.arena.layout.VerticalLayout;
import org.uqbar.arena.widgets.Panel;
import org.uqbar.arena.windows.Dialog;
import org.uqbar.arena.windows.WindowOwner;

@SuppressWarnings("all")
public abstract class EditionGameWindow extends Dialog<JuegoApplicationModel> {
  public EditionGameWindow(final WindowOwner owner, final JuegoApplicationModel model) {
    super(owner, model);
  }
  
  protected void createFormPanel(final Panel mainPanel) {
  }
  
  public void createMainTemplate(final Panel mainPanel) {
    this.obtenerTitulo();
    Panel panelAux = new Panel(mainPanel);
    VerticalLayout _verticalLayout = new VerticalLayout();
    panelAux.setLayout(_verticalLayout);
    this.agregartexto(panelAux);
    this.agregarInPut(panelAux);
    this.agregarBotones(panelAux);
  }
  
  public abstract void obtenerTitulo();
  
  public abstract void agregarBotones(final Panel panel);
  
  public abstract void agregarInPut(final Panel panel);
  
  public abstract void agregartexto(final Panel panel);
}
