package view;

import Operations.Operations;
import applicationModel.MapamundiApplicationModel;
import applicationModel.PaisApplicationModel;
import data.DataWindow;
import dominio.Lugar;
import dominio.Pais;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure0;
import org.uqbar.arena.bindings.PropertyAdapter;
import org.uqbar.arena.layout.HorizontalLayout;
import org.uqbar.arena.layout.VerticalLayout;
import org.uqbar.arena.widgets.Panel;
import org.uqbar.arena.windows.WindowOwner;
import view.pais.EditarPaisWindow;
import view.pais.NuevoPaisWindow;

@SuppressWarnings("all")
public class MapamundiWindow extends DataWindow<MapamundiApplicationModel> {
  public MapamundiWindow(final WindowOwner owner, final MapamundiApplicationModel model) {
    super(owner, model);
  }
  
  public void giveTitle() {
    this.setTitle("Mapamundi");
  }
  
  public void addList(final Panel panel) {
    PropertyAdapter _propertyAdapter = new PropertyAdapter(Pais.class, "nombre");
    Operations.addListWithTsize(panel, "Paises", "mapamundi.paises", "paisSeleccionado", _propertyAdapter, Integer.valueOf(300), Integer.valueOf(100));
  }
  
  public void addBotons(final Panel panel) {
    Panel botonsPanel = new Panel(panel);
    VerticalLayout _verticalLayout = new VerticalLayout();
    botonsPanel.setLayout(_verticalLayout);
    final Procedure0 _function = new Procedure0() {
      public void apply() {
        MapamundiWindow.this.getModelObject().eliminarPais();
      }
    };
    Operations.addBoton(botonsPanel, "Eliminar", "paisSeleccionado", _function);
    final Procedure0 _function_1 = new Procedure0() {
      public void apply() {
        Pais _paisSeleccionado = MapamundiWindow.this.getModelObject().getPaisSeleccionado();
        PaisApplicationModel _paisApplicationModel = new PaisApplicationModel(_paisSeleccionado);
        new EditarPaisWindow(MapamundiWindow.this, _paisApplicationModel).open();
      }
    };
    Operations.addBoton(botonsPanel, "Editar", "paisSeleccionado", _function_1);
    final Procedure0 _function_2 = new Procedure0() {
      public void apply() {
        Pais _nuevoPais = MapamundiWindow.this.getModelObject().nuevoPais();
        PaisApplicationModel _paisApplicationModel = new PaisApplicationModel(_nuevoPais);
        new NuevoPaisWindow(MapamundiWindow.this, _paisApplicationModel).open();
      }
    };
    Operations.addBoton(botonsPanel, "Nuevo", _function_2);
  }
  
  public void addCharacteristic(final Panel panel) {
    Panel nombrePanel = new Panel(panel);
    HorizontalLayout _horizontalLayout = new HorizontalLayout();
    nombrePanel.setLayout(_horizontalLayout);
    Operations.addText(nombrePanel, "Nombre: ");
    Operations.addTextProperty(nombrePanel, "paisSeleccionado.nombre");
    final Panel caracteristicaPanel = new Panel(panel);
    HorizontalLayout _horizontalLayout_1 = new HorizontalLayout();
    caracteristicaPanel.setLayout(_horizontalLayout_1);
    Operations.addText(caracteristicaPanel, "Características");
    final Panel listaCaracteristicaPanel = new Panel(panel);
    VerticalLayout _verticalLayout = new VerticalLayout();
    listaCaracteristicaPanel.setLayout(_verticalLayout);
    Operations.addList(listaCaracteristicaPanel, "Características", "paisSeleccionado.caracteristicas");
    final Panel conexionesPanel = new Panel(panel);
    HorizontalLayout _horizontalLayout_2 = new HorizontalLayout();
    conexionesPanel.setLayout(_horizontalLayout_2);
    Operations.addText(conexionesPanel, "Conexiones");
    final Panel listaConexionesPanel = new Panel(panel);
    VerticalLayout _verticalLayout_1 = new VerticalLayout();
    listaConexionesPanel.setLayout(_verticalLayout_1);
    PropertyAdapter _propertyAdapter = new PropertyAdapter(Pais.class, "nombre");
    Operations.addList(listaConexionesPanel, "Conexiones", "paisSeleccionado.conexiones", _propertyAdapter);
    final Panel lugaresPanel = new Panel(panel);
    HorizontalLayout _horizontalLayout_3 = new HorizontalLayout();
    lugaresPanel.setLayout(_horizontalLayout_3);
    Operations.addText(lugaresPanel, "Lugares de Interés");
    final Panel listaLugaresPanel = new Panel(panel);
    VerticalLayout _verticalLayout_2 = new VerticalLayout();
    listaLugaresPanel.setLayout(_verticalLayout_2);
    PropertyAdapter _propertyAdapter_1 = new PropertyAdapter(Lugar.class, "nombreDelLugar");
    Operations.addList(listaLugaresPanel, "Lugares de Interés", "paisSeleccionado.lugares", _propertyAdapter_1);
  }
}
