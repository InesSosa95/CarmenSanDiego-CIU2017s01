package view;

import Operations.Operations;
import dominio.Lugar;
import org.eclipse.xtend.lib.annotations.Accessors;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure0;
import org.uqbar.arena.layout.HorizontalLayout;
import org.uqbar.arena.widgets.Button;
import org.uqbar.arena.widgets.Panel;
import org.uqbar.arena.windows.WindowOwner;
import view.DialogoWindow;

@Accessors
@SuppressWarnings("all")
public class VisitarLugarWindow extends DialogoWindow<Lugar> {
  public VisitarLugarWindow(final WindowOwner owner, final Lugar model) {
    super(owner, model);
  }
  
  public void addName(final Panel panel) {
  }
  
  protected void createFormPanel(final Panel panel) {
    Operations.addList(panel, "Pistas", "damePistas");
    this.agregarBotones(panel);
  }
  
  public Button agregarBotones(final Panel panel) {
    Button _xblockexpression = null;
    {
      Panel panelAux = new Panel(panel);
      HorizontalLayout _horizontalLayout = new HorizontalLayout();
      panelAux.setLayout(_horizontalLayout);
      final Procedure0 _function = new Procedure0() {
        public void apply() {
          VisitarLugarWindow.this.close();
        }
      };
      _xblockexpression = Operations.addBoton(panelAux, "Continuar", _function);
    }
    return _xblockexpression;
  }
  
  public Procedure0 aceptarMetodo() {
    return null;
  }
  
  public void giveTitle() {
  }
}
