package view;

import Operations.Operations;
import applicationModel.JuegoApplicationModel;
import java.awt.Color;
import org.eclipse.xtext.xbase.lib.ObjectExtensions;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure0;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1;
import org.uqbar.arena.layout.VerticalLayout;
import org.uqbar.arena.widgets.Label;
import org.uqbar.arena.widgets.Panel;
import org.uqbar.arena.windows.Dialog;
import org.uqbar.arena.windows.WindowOwner;
import view.ResolverMisterioWindow;

@SuppressWarnings("all")
public class InicioDeJuegoWindow extends Dialog<JuegoApplicationModel> {
  public InicioDeJuegoWindow(final WindowOwner owner, final JuegoApplicationModel model) {
    super(owner, model);
  }
  
  protected void createFormPanel(final Panel mainPanel) {
  }
  
  public void createMainTemplate(final Panel mainPanel) {
    this.setTitle(this.getModelObject().nombreCaso());
    Panel editorPanel = new Panel(mainPanel);
    VerticalLayout _verticalLayout = new VerticalLayout();
    editorPanel.setLayout(_verticalLayout);
    Operations.addText(editorPanel, "Detective, tenemos un caso para usted!");
    Operations.addText(editorPanel, this.getModelObject().reporte());
    Label _addText = Operations.addText(editorPanel, this.getModelObject().objetoRobado());
    final Procedure1<Label> _function = new Procedure1<Label>() {
      public void apply(final Label it) {
        it.setFontSize(20);
        it.setForeground(Color.BLUE);
      }
    };
    ObjectExtensions.<Label>operator_doubleArrow(_addText, _function);
    final Procedure0 _function_1 = new Procedure0() {
      public void apply() {
        InicioDeJuegoWindow.this.close();
        JuegoApplicationModel _modelObject = InicioDeJuegoWindow.this.getModelObject();
        new ResolverMisterioWindow(InicioDeJuegoWindow.this, _modelObject).open();
      }
    };
    Operations.addBoton(editorPanel, "Aceptar el caso", _function_1);
  }
}
