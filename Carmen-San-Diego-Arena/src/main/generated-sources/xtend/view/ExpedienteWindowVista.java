package view;

import Operations.Operations;
import applicationModel.ExpedienteApplicationModel;
import dominio.Villano;
import org.eclipse.xtext.xbase.lib.ObjectExtensions;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1;
import org.uqbar.arena.bindings.ObservableProperty;
import org.uqbar.arena.bindings.ObservableValue;
import org.uqbar.arena.bindings.PropertyAdapter;
import org.uqbar.arena.layout.ColumnLayout;
import org.uqbar.arena.layout.VerticalLayout;
import org.uqbar.arena.widgets.Control;
import org.uqbar.arena.widgets.Label;
import org.uqbar.arena.widgets.List;
import org.uqbar.arena.widgets.Panel;
import org.uqbar.arena.widgets.tables.Column;
import org.uqbar.arena.widgets.tables.Table;
import org.uqbar.arena.windows.SimpleWindow;
import org.uqbar.arena.windows.WindowOwner;
import org.uqbar.arena.xtend.ArenaXtendExtensions;
import org.uqbar.commons.model.IModel;
import org.uqbar.lacar.ui.model.ControlBuilder;
import org.uqbar.lacar.ui.model.TableBuilder;
import org.uqbar.lacar.ui.model.bindings.ViewObservable;

@SuppressWarnings("all")
public class ExpedienteWindowVista extends SimpleWindow<ExpedienteApplicationModel> {
  public ExpedienteWindowVista(final WindowOwner parent, final ExpedienteApplicationModel model) {
    super(parent, model);
    this.setTitle("Resolviendo misterios");
  }
  
  protected void addActions(final Panel parent) {
  }
  
  protected void createFormPanel(final Panel parent) {
    final Panel contenedorDeColumnas = new Panel(parent);
    ColumnLayout _columnLayout = new ColumnLayout(2);
    contenedorDeColumnas.setLayout(_columnLayout);
    final Panel villanos = new Panel(contenedorDeColumnas);
    final Panel caracteristicasDeVillanoSeleccionado = new Panel(contenedorDeColumnas);
    Label _label = new Label(caracteristicasDeVillanoSeleccionado);
    final Procedure1<Label> _function = new Procedure1<Label>() {
      public void apply(final Label it) {
        it.setText("Nombre: ");
      }
    };
    ObjectExtensions.<Label>operator_doubleArrow(_label, _function);
    Label _label_1 = new Label(caracteristicasDeVillanoSeleccionado);
    final Procedure1<Label> _function_1 = new Procedure1<Label>() {
      public void apply(final Label it) {
        it.setText("Sexo: ");
      }
    };
    ObjectExtensions.<Label>operator_doubleArrow(_label_1, _function_1);
    Label _label_2 = new Label(caracteristicasDeVillanoSeleccionado);
    final Procedure1<Label> _function_2 = new Procedure1<Label>() {
      public void apply(final Label it) {
        it.setText("Señas: ");
      }
    };
    ObjectExtensions.<Label>operator_doubleArrow(_label_2, _function_2);
    Operations.addLabelColours(caracteristicasDeVillanoSeleccionado, "SEÑA");
    this.listaDeSenhas(caracteristicasDeVillanoSeleccionado);
    Label _label_3 = new Label(caracteristicasDeVillanoSeleccionado);
    final Procedure1<Label> _function_3 = new Procedure1<Label>() {
      public void apply(final Label it) {
        it.setText("Hobbies: ");
      }
    };
    ObjectExtensions.<Label>operator_doubleArrow(_label_3, _function_3);
    Operations.addLabelColours(caracteristicasDeVillanoSeleccionado, "HOBBIE");
    this.listaDeHobbies(caracteristicasDeVillanoSeleccionado);
    Operations.addLabelColours(villanos, "VILLANO");
    this.listaDeVillanos(villanos);
  }
  
  protected List<Object> listaDeVillanos(final Panel parent) {
    List<Object> _xblockexpression = null;
    {
      final Panel listaDeVillanos = new Panel(parent);
      VerticalLayout _verticalLayout = new VerticalLayout();
      listaDeVillanos.setLayout(_verticalLayout);
      PropertyAdapter _propertyAdapter = new PropertyAdapter(Villano.class, "nombre");
      _xblockexpression = Operations.addListWithTsize(parent, "Villanos", "expediente.villanos", "villanoSeleccionado", _propertyAdapter, Integer.valueOf(300), Integer.valueOf(100));
    }
    return _xblockexpression;
  }
  
  public Column<Villano> describeTablaVillanos(final Table<Villano> table) {
    Column<Villano> _column = new Column<Villano>(table);
    final Procedure1<Column<Villano>> _function = new Procedure1<Column<Villano>>() {
      public void apply(final Column<Villano> it) {
        it.setTitle("Villano");
        it.bindContentsToProperty("nombre");
      }
    };
    return ObjectExtensions.<Column<Villano>>operator_doubleArrow(_column, _function);
  }
  
  protected Column<String> tablaDeSenhas(final Panel parent) {
    Column<String> _xblockexpression = null;
    {
      Table<String> _table = new Table<String>(parent, String.class);
      final Procedure1<Table<String>> _function = new Procedure1<Table<String>>() {
        public void apply(final Table<String> it) {
          ViewObservable<Table<String>, TableBuilder<String>> _items = it.items();
          ArenaXtendExtensions.operator_spaceship(_items, "expediente.villanos");
          ObservableValue<Control, ControlBuilder> _value = it.<ControlBuilder>value();
          ArenaXtendExtensions.operator_spaceship(_value, "villanoSeleccionado");
        }
      };
      final Table<String> tablaDeSenhas = ObjectExtensions.<Table<String>>operator_doubleArrow(_table, _function);
      _xblockexpression = this.describeTablaSenhas(tablaDeSenhas);
    }
    return _xblockexpression;
  }
  
  public Column<String> describeTablaSenhas(final Table<String> table) {
    Column<String> _column = new Column<String>(table);
    final Procedure1<Column<String>> _function = new Procedure1<Column<String>>() {
      public void apply(final Column<String> it) {
        it.setTitle("Señas");
        it.bindContentsToProperty("senha");
      }
    };
    return ObjectExtensions.<Column<String>>operator_doubleArrow(_column, _function);
  }
  
  protected Column<String> tablaDeHobbies(final Panel parent) {
    Column<String> _xblockexpression = null;
    {
      Table<String> _table = new Table<String>(parent, String.class);
      final Procedure1<Table<String>> _function = new Procedure1<Table<String>>() {
        public void apply(final Table<String> it) {
          ViewObservable<Table<String>, TableBuilder<String>> _items = it.items();
          ArenaXtendExtensions.operator_spaceship(_items, "expediente.villanos");
          ObservableValue<Control, ControlBuilder> _value = it.<ControlBuilder>value();
          ArenaXtendExtensions.operator_spaceship(_value, "villanoSeleccionado");
        }
      };
      final Table<String> tablaDeSenhas = ObjectExtensions.<Table<String>>operator_doubleArrow(_table, _function);
      _xblockexpression = this.describeTablaHobbies(tablaDeSenhas);
    }
    return _xblockexpression;
  }
  
  public Column<String> describeTablaHobbies(final Table<String> table) {
    Column<String> _column = new Column<String>(table);
    final Procedure1<Column<String>> _function = new Procedure1<Column<String>>() {
      public void apply(final Column<String> it) {
        it.setTitle("Señas");
        it.bindContentsToProperty("senha");
      }
    };
    return ObjectExtensions.<Column<String>>operator_doubleArrow(_column, _function);
  }
  
  public List<String> listaDeSenhas(final Panel parent) {
    List<String> _xblockexpression = null;
    {
      final Panel senhasAgregadas = new Panel(parent);
      VerticalLayout _verticalLayout = new VerticalLayout();
      senhasAgregadas.setLayout(_verticalLayout);
      List<String> _list = new List<String>(senhasAgregadas);
      final Procedure1<List<String>> _function = new Procedure1<List<String>>() {
        public void apply(final List<String> it) {
          IModel<ExpedienteApplicationModel> _model = ExpedienteWindowVista.this.getModel();
          ObservableProperty<Object> _observableProperty = new ObservableProperty<Object>(_model, "villanoSeleccionado.senhas");
          it.<Object>bindItems(_observableProperty);
          it.setWidth(200);
          it.setHeight(100);
        }
      };
      _xblockexpression = ObjectExtensions.<List<String>>operator_doubleArrow(_list, _function);
    }
    return _xblockexpression;
  }
  
  public List<String> listaDeHobbies(final Panel parent) {
    List<String> _xblockexpression = null;
    {
      final Panel senhasAgregadas = new Panel(parent);
      VerticalLayout _verticalLayout = new VerticalLayout();
      senhasAgregadas.setLayout(_verticalLayout);
      List<String> _list = new List<String>(senhasAgregadas);
      final Procedure1<List<String>> _function = new Procedure1<List<String>>() {
        public void apply(final List<String> it) {
          IModel<ExpedienteApplicationModel> _model = ExpedienteWindowVista.this.getModel();
          ObservableProperty<Object> _observableProperty = new ObservableProperty<Object>(_model, "villanoSeleccionado.hobbies");
          it.<Object>bindItems(_observableProperty);
          it.setWidth(200);
          it.setHeight(100);
        }
      };
      _xblockexpression = ObjectExtensions.<List<String>>operator_doubleArrow(_list, _function);
    }
    return _xblockexpression;
  }
}
