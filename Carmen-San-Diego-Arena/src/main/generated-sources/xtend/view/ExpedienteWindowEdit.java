package view;

import Operations.Operations;
import applicationModel.ExpedienteApplicationModel;
import applicationModel.VillanoApplicationModel;
import data.DataWindow;
import dominio.Villano;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure0;
import org.uqbar.arena.bindings.PropertyAdapter;
import org.uqbar.arena.layout.HorizontalLayout;
import org.uqbar.arena.layout.VerticalLayout;
import org.uqbar.arena.widgets.Panel;
import org.uqbar.arena.windows.WindowOwner;
import view.villano.EditarVillanoWindow;
import view.villano.NuevoVillanoWindow;

@SuppressWarnings("all")
public class ExpedienteWindowEdit extends DataWindow<ExpedienteApplicationModel> {
  public ExpedienteWindowEdit(final WindowOwner owner, final ExpedienteApplicationModel model) {
    super(owner, model);
  }
  
  public void giveTitle() {
    this.setTitle("Expediente");
  }
  
  public void addList(final Panel panel) {
    PropertyAdapter _propertyAdapter = new PropertyAdapter(Villano.class, "nombre");
    Operations.addListWithTsize(panel, "Villanos", "expediente.villanos", "villanoSeleccionado", _propertyAdapter, Integer.valueOf(300), Integer.valueOf(100));
  }
  
  public void addBotons(final Panel panel) {
    Panel botonsPanel = new Panel(panel);
    VerticalLayout _verticalLayout = new VerticalLayout();
    botonsPanel.setLayout(_verticalLayout);
    final Procedure0 _function = new Procedure0() {
      public void apply() {
        Villano _villanoSeleccionado = ExpedienteWindowEdit.this.getModelObject().getVillanoSeleccionado();
        VillanoApplicationModel _villanoApplicationModel = new VillanoApplicationModel(_villanoSeleccionado);
        new EditarVillanoWindow(ExpedienteWindowEdit.this, _villanoApplicationModel).open();
      }
    };
    Operations.addBoton(botonsPanel, "Editar", "villanoSeleccionado", _function);
    final Procedure0 _function_1 = new Procedure0() {
      public void apply() {
        VillanoApplicationModel _villanoApplicationModel = new VillanoApplicationModel();
        new NuevoVillanoWindow(ExpedienteWindowEdit.this, _villanoApplicationModel).open();
      }
    };
    Operations.addBoton(botonsPanel, "Nuevo", _function_1);
  }
  
  public void addCharacteristic(final Panel panel) {
    Panel nombrePanel = new Panel(panel);
    HorizontalLayout _horizontalLayout = new HorizontalLayout();
    nombrePanel.setLayout(_horizontalLayout);
    Operations.addText(nombrePanel, "Nombre: ");
    Operations.addTextProperty(nombrePanel, "villanoSeleccionado.nombre");
    Panel nombrePanel2 = new Panel(panel);
    HorizontalLayout _horizontalLayout_1 = new HorizontalLayout();
    nombrePanel2.setLayout(_horizontalLayout_1);
    Operations.addText(nombrePanel2, "Sexo: ");
    Operations.addTextProperty(nombrePanel2, "villanoSeleccionado.sexo");
    final Panel caracteristicaPanel = new Panel(panel);
    HorizontalLayout _horizontalLayout_2 = new HorizontalLayout();
    caracteristicaPanel.setLayout(_horizontalLayout_2);
    Operations.addText(caracteristicaPanel, "Señas Particulares:");
    final Panel listaCaracteristicaPanel = new Panel(panel);
    VerticalLayout _verticalLayout = new VerticalLayout();
    listaCaracteristicaPanel.setLayout(_verticalLayout);
    Operations.addList(listaCaracteristicaPanel, "Seña", "villanoSeleccionado.senhas");
    final Panel conexionesPanel = new Panel(panel);
    HorizontalLayout _horizontalLayout_3 = new HorizontalLayout();
    conexionesPanel.setLayout(_horizontalLayout_3);
    Operations.addText(conexionesPanel, "Hobbies");
    final Panel listaConexionesPanel = new Panel(panel);
    VerticalLayout _verticalLayout_1 = new VerticalLayout();
    listaConexionesPanel.setLayout(_verticalLayout_1);
    Operations.addList(listaConexionesPanel, "Hobbie", "villanoSeleccionado.hobbies");
  }
}
