package view;

import Operations.Operations;
import applicationModel.ExpedienteApplicationModel;
import applicationModel.JuegoApplicationModel;
import applicationModel.MapamundiApplicationModel;
import dominio.Detective;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure0;
import org.uqbar.arena.layout.HorizontalLayout;
import org.uqbar.arena.layout.VerticalLayout;
import org.uqbar.arena.widgets.Label;
import org.uqbar.arena.widgets.Panel;
import org.uqbar.arena.windows.SimpleWindow;
import org.uqbar.arena.windows.WindowOwner;
import view.ExpedienteWindowEdit;
import view.InicioDeJuegoWindow;
import view.MapamundiWindow;

@SuppressWarnings("all")
public class MenuDeAccionesWindow extends SimpleWindow<Detective> {
  public MenuDeAccionesWindow(final WindowOwner parent, final Detective model) {
    super(parent, model);
  }
  
  protected void addActions(final Panel actionsPanel) {
  }
  
  protected void createFormPanel(final Panel mainPanel) {
  }
  
  public void createMainTemplate(final Panel mainPanel) {
    this.setTitle("¿Donde esta Carmen San Diego?");
    VerticalLayout _verticalLayout = new VerticalLayout();
    mainPanel.setLayout(_verticalLayout);
    new Label(mainPanel).setText("¿Que haremos ahora detective?");
    Panel panel = new Panel(mainPanel);
    HorizontalLayout _horizontalLayout = new HorizontalLayout();
    panel.setLayout(_horizontalLayout);
    final Procedure0 _function = new Procedure0() {
      public void apply() {
        JuegoApplicationModel _juegoApplicationModel = new JuegoApplicationModel();
        new InicioDeJuegoWindow(MenuDeAccionesWindow.this, _juegoApplicationModel).open();
      }
    };
    Operations.addBoton(panel, "Resolver Misterio", _function);
    final Procedure0 _function_1 = new Procedure0() {
      public void apply() {
        MapamundiApplicationModel _mapamundiApplicationModel = new MapamundiApplicationModel();
        new MapamundiWindow(MenuDeAccionesWindow.this, _mapamundiApplicationModel).open();
      }
    };
    Operations.addBoton(panel, "Mapamundi", _function_1);
    final Procedure0 _function_2 = new Procedure0() {
      public void apply() {
        ExpedienteApplicationModel _expedienteApplicationModel = new ExpedienteApplicationModel();
        new ExpedienteWindowEdit(MenuDeAccionesWindow.this, _expedienteApplicationModel).open();
      }
    };
    Operations.addBoton(panel, "Expedientes", _function_2);
  }
}
