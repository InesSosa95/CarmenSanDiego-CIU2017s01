package view.pais;

import applicationModel.PaisApplicationModel;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure0;
import org.uqbar.arena.windows.WindowOwner;
import view.pais.PaisWindow;

@SuppressWarnings("all")
public class EditarPaisWindow extends PaisWindow {
  public EditarPaisWindow(final WindowOwner owner, final PaisApplicationModel model) {
    super(owner, model);
  }
  
  public void giveTitle() {
    this.setTitle("Mapamundi - Editar Pais");
  }
  
  public Procedure0 aceptarMetodo() {
    final Procedure0 _function = new Procedure0() {
      public void apply() {
        EditarPaisWindow.this.close();
      }
    };
    return _function;
  }
}
