package view.pais;

import Operations.Operations;
import applicationModel.PaisApplicationModel;
import dominio.Lugar;
import dominio.Pais;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure0;
import org.uqbar.arena.bindings.PropertyAdapter;
import org.uqbar.arena.layout.HorizontalLayout;
import org.uqbar.arena.layout.VerticalLayout;
import org.uqbar.arena.widgets.Panel;
import org.uqbar.arena.windows.WindowOwner;
import view.DialogoWindow;
import view.pais.EditarCaracteristicaWindow;
import view.pais.EditarConexionesWindow;
import view.pais.EditarLugarWindow;

@SuppressWarnings("all")
public abstract class PaisWindow extends DialogoWindow<PaisApplicationModel> {
  public PaisWindow(final WindowOwner owner, final PaisApplicationModel model) {
    super(owner, model);
  }
  
  protected void createFormPanel(final Panel mainPanel) {
  }
  
  public void createMainTemplate(final Panel mainPanel) {
    this.giveTitle();
    final Panel editorPanel = new Panel(mainPanel);
    VerticalLayout _verticalLayout = new VerticalLayout();
    editorPanel.setLayout(_verticalLayout);
    this.addName(editorPanel);
    final Procedure0 _function = new Procedure0() {
      public void apply() {
        PaisApplicationModel _modelObject = PaisWindow.this.getModelObject();
        new EditarCaracteristicaWindow(PaisWindow.this, _modelObject).open();
      }
    };
    this.presentarCaracteristica(editorPanel, "Caracteristicas", "Editar Caracteristicas", _function, "Caracteristicas", "pais.caracteristicas");
    final Procedure0 _function_1 = new Procedure0() {
      public void apply() {
        PaisApplicationModel _modelObject = PaisWindow.this.getModelObject();
        new EditarConexionesWindow(PaisWindow.this, _modelObject).open();
      }
    };
    PropertyAdapter _propertyAdapter = new PropertyAdapter(Pais.class, "nombre");
    this.presentarCaracteristica(editorPanel, "Conexiones", "Editar Conexiones", _function_1, "Conexiones", "pais.conexiones", _propertyAdapter);
    final Procedure0 _function_2 = new Procedure0() {
      public void apply() {
        PaisApplicationModel _modelObject = PaisWindow.this.getModelObject();
        new EditarLugarWindow(PaisWindow.this, _modelObject).open();
      }
    };
    PropertyAdapter _propertyAdapter_1 = new PropertyAdapter(Lugar.class, "nombreDelLugar");
    this.presentarCaracteristica(editorPanel, "Lugares de Interes", "Editar Lugares", _function_2, "Lugares de Interés", "pais.lugares", _propertyAdapter_1);
    this.botonAceptar(editorPanel);
  }
  
  public void addName(final Panel panel) {
    Panel panelAux = new Panel(panel);
    HorizontalLayout _horizontalLayout = new HorizontalLayout();
    panelAux.setLayout(_horizontalLayout);
    Operations.addText(panelAux, "Nombre: ");
    Operations.addTextBox(panelAux, "pais.nombre");
  }
}
