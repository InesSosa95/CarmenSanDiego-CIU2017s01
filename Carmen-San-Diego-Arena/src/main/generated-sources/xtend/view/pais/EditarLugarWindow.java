package view.pais;

import applicationModel.PaisApplicationModel;
import dominio.Lugar;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure0;
import org.uqbar.arena.bindings.Adapter;
import org.uqbar.arena.bindings.PropertyAdapter;
import org.uqbar.arena.windows.WindowOwner;
import view.EditionWindowConSelector;

@SuppressWarnings("all")
public class EditarLugarWindow extends EditionWindowConSelector<PaisApplicationModel> {
  public EditarLugarWindow(final WindowOwner owner, final PaisApplicationModel model) {
    super(owner, model);
  }
  
  public Procedure0 aceptarMetodo() {
    final Procedure0 _function = new Procedure0() {
      public void apply() {
        EditarLugarWindow.this.getModelObject().verificarLugares();
        EditarLugarWindow.this.close();
      }
    };
    return _function;
  }
  
  public String caracteristicaSeleccionada() {
    return "lugarSeleccionada";
  }
  
  public String listaAEditarEnModel() {
    return "pais.lugares";
  }
  
  public Adapter adapterNecesario() {
    return new PropertyAdapter(Lugar.class, "nombreDelLugar");
  }
  
  public String componenteEnModel() {
    return "nuevoLugar";
  }
  
  public String listaDondeSeleccionarEnModel() {
    return "lugaresPosibles";
  }
  
  public String caracteristicaEnModel() {
    return "nuevoLugar";
  }
  
  public void giveTitle() {
    this.setTitle("Editar Lugares");
  }
  
  public Procedure0 metodoAgregarEnModel() {
    final Procedure0 _function = new Procedure0() {
      public void apply() {
        EditarLugarWindow.this.getModelObject().agregarLugar();
      }
    };
    return _function;
  }
  
  public Procedure0 metodoEliminarEnModel() {
    final Procedure0 _function = new Procedure0() {
      public void apply() {
        EditarLugarWindow.this.getModelObject().eliminarLugar();
      }
    };
    return _function;
  }
  
  public String tituloDeListaAEditar() {
    return "Lugares De Interes";
  }
}
