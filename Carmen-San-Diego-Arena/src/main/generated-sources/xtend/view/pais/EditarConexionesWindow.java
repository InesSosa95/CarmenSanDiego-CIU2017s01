package view.pais;

import applicationModel.PaisApplicationModel;
import dominio.Pais;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure0;
import org.uqbar.arena.bindings.Adapter;
import org.uqbar.arena.bindings.PropertyAdapter;
import org.uqbar.arena.windows.WindowOwner;
import view.EditionWindowConSelector;

@SuppressWarnings("all")
public class EditarConexionesWindow extends EditionWindowConSelector<PaisApplicationModel> {
  public EditarConexionesWindow(final WindowOwner owner, final PaisApplicationModel model) {
    super(owner, model);
  }
  
  public void giveTitle() {
    this.setTitle("Editar Conexiones");
  }
  
  public String listaDondeSeleccionarEnModel() {
    return "paisesPosibles";
  }
  
  public String caracteristicaEnModel() {
    return "nuevaConexion";
  }
  
  public Adapter adapterNecesario() {
    return new PropertyAdapter(Pais.class, "nombre");
  }
  
  public String listaAEditarEnModel() {
    return "pais.conexiones";
  }
  
  public Procedure0 metodoAgregarEnModel() {
    final Procedure0 _function = new Procedure0() {
      public void apply() {
        EditarConexionesWindow.this.getModelObject().agregarConexion();
      }
    };
    return _function;
  }
  
  public Procedure0 metodoEliminarEnModel() {
    final Procedure0 _function = new Procedure0() {
      public void apply() {
        EditarConexionesWindow.this.getModelObject().eliminarConexion();
      }
    };
    return _function;
  }
  
  public String tituloDeListaAEditar() {
    return "Conexiones";
  }
  
  public String componenteEnModel() {
    return "conexionSeleccionada";
  }
  
  public String caracteristicaSeleccionada() {
    return "conexionSeleccionada";
  }
}
