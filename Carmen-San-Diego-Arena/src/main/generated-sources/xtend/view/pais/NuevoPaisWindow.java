package view.pais;

import applicationModel.PaisApplicationModel;
import dominio.Mapamundi;
import dominio.Pais;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure0;
import org.uqbar.arena.windows.WindowOwner;
import view.pais.PaisWindow;

@SuppressWarnings("all")
public class NuevoPaisWindow extends PaisWindow {
  public NuevoPaisWindow(final WindowOwner owner, final PaisApplicationModel model) {
    super(owner, model);
    Pais _pais = new Pais();
    model.setPais(_pais);
  }
  
  public void giveTitle() {
    this.setTitle("Mapamundi - Nuevo Pais");
  }
  
  public Procedure0 aceptarMetodo() {
    final Procedure0 _function = new Procedure0() {
      public void apply() {
        NuevoPaisWindow.this.getModelObject().cambiarNombrePais();
        final Mapamundi mapa = Mapamundi.obtenerInstancia();
        NuevoPaisWindow.this.getModelObject().verificarQueNoExistaPaisConIgualNombre();
        mapa.agregarPais(NuevoPaisWindow.this.getModelObject().getPais());
        NuevoPaisWindow.this.close();
      }
    };
    return _function;
  }
}
