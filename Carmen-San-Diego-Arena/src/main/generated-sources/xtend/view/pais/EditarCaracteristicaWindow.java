package view.pais;

import applicationModel.PaisApplicationModel;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure0;
import org.uqbar.arena.windows.WindowOwner;
import view.EditionWindows;

@SuppressWarnings("all")
public class EditarCaracteristicaWindow extends EditionWindows<PaisApplicationModel> {
  public EditarCaracteristicaWindow(final WindowOwner owner, final PaisApplicationModel model) {
    super(owner, model);
  }
  
  public void giveTitle() {
    this.setTitle("Editar Características");
  }
  
  public Procedure0 metodoEliminarEnModel() {
    final Procedure0 _function = new Procedure0() {
      public void apply() {
        EditarCaracteristicaWindow.this.getModelObject().eliminarCaracteristica();
      }
    };
    return _function;
  }
  
  public Procedure0 metodoAgregarEnModel() {
    final Procedure0 _function = new Procedure0() {
      public void apply() {
        EditarCaracteristicaWindow.this.getModelObject().agregarCaracteristica();
      }
    };
    return _function;
  }
  
  public String listaAEditarEnModel() {
    return "pais.caracteristicas";
  }
  
  public String tituloDeListaAEditar() {
    return "Caracteristicas";
  }
}
