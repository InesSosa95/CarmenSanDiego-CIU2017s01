package runnable;

import dominio.Detective;
import dummyData.DummyData;
import org.uqbar.arena.Application;
import org.uqbar.arena.windows.Window;
import view.MenuDeAccionesWindow;

@SuppressWarnings("all")
public class CarmenSanDiegoApplication extends Application {
  protected Window<?> createMainWindow() {
    MenuDeAccionesWindow _xblockexpression = null;
    {
      final DummyData model = new DummyData();
      Detective _detective = model.getDetective();
      _xblockexpression = new MenuDeAccionesWindow(this, _detective);
    }
    return _xblockexpression;
  }
  
  public static void main(final String[] args) {
    new CarmenSanDiegoApplication().start();
  }
}
