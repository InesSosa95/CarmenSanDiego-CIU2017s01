package Operations;

import java.awt.Color;
import java.util.List;
import java.util.function.Consumer;
import org.eclipse.xtext.xbase.lib.ObjectExtensions;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure0;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1;
import org.uqbar.arena.bindings.Adapter;
import org.uqbar.arena.bindings.NotNullObservable;
import org.uqbar.arena.widgets.Button;
import org.uqbar.arena.widgets.Control;
import org.uqbar.arena.widgets.Label;
import org.uqbar.arena.widgets.Panel;
import org.uqbar.arena.widgets.Selector;
import org.uqbar.arena.widgets.TextBox;
import org.uqbar.lacar.ui.model.Action;
import org.uqbar.lacar.ui.model.ControlBuilder;
import org.uqbar.lacar.ui.model.ListBuilder;
import org.uqbar.lacar.ui.model.bindings.Binding;

@SuppressWarnings("all")
public class Operations {
  /**
   * Crea un boton en el panel
   */
  public static Button addBoton(final Panel panel, final String texto, final Procedure0 onclick) {
    Button _button = new Button(panel);
    final Procedure1<Button> _function = new Procedure1<Button>() {
      public void apply(final Button it) {
        it.setCaption(texto);
        it.onClick(new Action() {
            public void execute() {
              onclick.apply();
            }
        });
      }
    };
    return ObjectExtensions.<Button>operator_doubleArrow(_button, _function);
  }
  
  /**
   * agrega un boton con el nombre que recibe por parametro, realiza la accion que esta en el bloque onclick y tiene un bindEnable
   */
  public static Button addBoton(final Panel panel, final String nombre, final String bindEnabled, final Procedure0 onclick) {
    Button _button = new Button(panel);
    final Procedure1<Button> _function = new Procedure1<Button>() {
      public void apply(final Button it) {
        it.setCaption(nombre);
        it.onClick(new Action() {
            public void execute() {
              onclick.apply();
            }
        });
        NotNullObservable _notNullObservable = new NotNullObservable(bindEnabled);
        it.<Object, ControlBuilder>bindEnabled(_notNullObservable);
        it.disableOnError();
      }
    };
    return ObjectExtensions.<Button>operator_doubleArrow(_button, _function);
  }
  
  public static Button addBotonBind(final Panel panel, final String bind, final Procedure0 onclick) {
    Button _button = new Button(panel);
    final Procedure1<Button> _function = new Procedure1<Button>() {
      public void apply(final Button it) {
        it.bindCaptionToProperty(bind);
        it.onClick(new Action() {
            public void execute() {
              onclick.apply();
            }
        });
      }
    };
    return ObjectExtensions.<Button>operator_doubleArrow(_button, _function);
  }
  
  /**
   * Crea un label en el panel del primer parametro. Con el segundo parametro es el texto del label y este se hace con un color de fondo gris
   */
  public static Label addLabelColours(final Panel panel, final String texto) {
    Label _label = new Label(panel);
    final Procedure1<Label> _function = new Procedure1<Label>() {
      public void apply(final Label it) {
        it.setText(texto);
        it.setWidth(203);
        it.setBackground(Color.lightGray);
      }
    };
    return ObjectExtensions.<Label>operator_doubleArrow(_label, _function);
  }
  
  /**
   * Crea un label en el panel del primer parametro. Con el segundo parametro es el texto del label y este se hace con un color de fundo gris, y con un  ancho
   */
  public static Label addLabelColours(final Panel panel, final String texto, final Integer ancho) {
    Label _label = new Label(panel);
    final Procedure1<Label> _function = new Procedure1<Label>() {
      public void apply(final Label it) {
        it.setText(texto);
        it.setWidth((ancho).intValue());
        it.setBackground(Color.lightGray);
      }
    };
    return ObjectExtensions.<Label>operator_doubleArrow(_label, _function);
  }
  
  /**
   * Crea un label en el panel que se pasa por parametro con el texto del segundo parametro
   */
  public static Label addText(final Panel panel, final String texto) {
    Label _label = new Label(panel);
    final Procedure1<Label> _function = new Procedure1<Label>() {
      public void apply(final Label it) {
        it.setText(texto);
      }
    };
    return ObjectExtensions.<Label>operator_doubleArrow(_label, _function);
  }
  
  public static Label addText(final Panel panel, final CharSequence texto) {
    Label _label = new Label(panel);
    final Procedure1<Label> _function = new Procedure1<Label>() {
      public void apply(final Label it) {
        it.setText(texto.toString());
      }
    };
    return ObjectExtensions.<Label>operator_doubleArrow(_label, _function);
  }
  
  public static void addText(final Panel panel, final List<String> lista) {
    final Consumer<String> _function = new Consumer<String>() {
      public void accept(final String it) {
        Operations.addText(panel, it);
      }
    };
    lista.forEach(_function);
  }
  
  /**
   * Se crea un label con un texto que viene de una propiedad
   */
  public static Label addTextProperty(final Panel panel, final String property) {
    Label _label = new Label(panel);
    final Procedure1<Label> _function = new Procedure1<Label>() {
      public void apply(final Label it) {
        it.<Object, ControlBuilder>bindValueToProperty(property);
        it.setWidth(100);
      }
    };
    return ObjectExtensions.<Label>operator_doubleArrow(_label, _function);
  }
  
  public static Label addTextProperty(final Panel panel, final String property, final Adapter adapter) {
    Label _label = new Label(panel);
    final Procedure1<Label> _function = new Procedure1<Label>() {
      public void apply(final Label it) {
        Binding<Object, Control, ControlBuilder> _bindValueToProperty = it.<Object, ControlBuilder>bindValueToProperty(property);
        _bindValueToProperty.setAdapter(adapter);
        it.setWidth(100);
      }
    };
    return ObjectExtensions.<Label>operator_doubleArrow(_label, _function);
  }
  
  /**
   * Se crea un texBox en un panel horizontal que esta en el pasado por parametro. Con el segundo parametro se hace un bind
   */
  public static TextBox addTextBox(final Panel panel, final String bind) {
    TextBox _textBox = new TextBox(panel);
    final Procedure1<TextBox> _function = new Procedure1<TextBox>() {
      public void apply(final TextBox it) {
        it.setWidth(100);
        it.<Object, ControlBuilder>bindValueToProperty(bind);
      }
    };
    return ObjectExtensions.<TextBox>operator_doubleArrow(_textBox, _function);
  }
  
  /**
   * Crea una lista en el panel del primer parametro. Con el segundo parametro se crea un label de color y con el ultimo
   * parametro se le la referencia de donde esta la lista
   */
  public static org.uqbar.arena.widgets.List<Object> addList(final Panel panel, final String texto, final String bind) {
    org.uqbar.arena.widgets.List<Object> _xblockexpression = null;
    {
      Operations.addLabelColours(panel, texto);
      org.uqbar.arena.widgets.List<Object> _list = new org.uqbar.arena.widgets.List<Object>(panel);
      final Procedure1<org.uqbar.arena.widgets.List<Object>> _function = new Procedure1<org.uqbar.arena.widgets.List<Object>>() {
        public void apply(final org.uqbar.arena.widgets.List<Object> it) {
          it.setWidth(130);
          it.setHeight(30);
          it.bindItemsToProperty(bind);
        }
      };
      _xblockexpression = ObjectExtensions.<org.uqbar.arena.widgets.List<Object>>operator_doubleArrow(_list, _function);
    }
    return _xblockexpression;
  }
  
  public static org.uqbar.arena.widgets.List<Object> addList(final Panel panel, final String texto, final String bind, final Adapter adapter) {
    org.uqbar.arena.widgets.List<Object> _xblockexpression = null;
    {
      Operations.addLabelColours(panel, texto);
      org.uqbar.arena.widgets.List<Object> _list = new org.uqbar.arena.widgets.List<Object>(panel);
      final Procedure1<org.uqbar.arena.widgets.List<Object>> _function = new Procedure1<org.uqbar.arena.widgets.List<Object>>() {
        public void apply(final org.uqbar.arena.widgets.List<Object> it) {
          it.setWidth(130);
          it.setHeight(30);
          Binding<?, Selector<Object>, ListBuilder<Object>> _bindItemsToProperty = it.bindItemsToProperty(bind);
          _bindItemsToProperty.setAdapter(adapter);
        }
      };
      _xblockexpression = ObjectExtensions.<org.uqbar.arena.widgets.List<Object>>operator_doubleArrow(_list, _function);
    }
    return _xblockexpression;
  }
  
  public static org.uqbar.arena.widgets.List<Object> addList(final Panel panel, final String texto, final String bind, final String guardarEn) {
    org.uqbar.arena.widgets.List<Object> _xblockexpression = null;
    {
      Operations.addLabelColours(panel, texto);
      org.uqbar.arena.widgets.List<Object> _list = new org.uqbar.arena.widgets.List<Object>(panel);
      final Procedure1<org.uqbar.arena.widgets.List<Object>> _function = new Procedure1<org.uqbar.arena.widgets.List<Object>>() {
        public void apply(final org.uqbar.arena.widgets.List<Object> it) {
          it.setWidth(130);
          it.setHeight(30);
          it.bindItemsToProperty(bind);
          it.<Object, ControlBuilder>bindValueToProperty(guardarEn);
        }
      };
      _xblockexpression = ObjectExtensions.<org.uqbar.arena.widgets.List<Object>>operator_doubleArrow(_list, _function);
    }
    return _xblockexpression;
  }
  
  public static org.uqbar.arena.widgets.List<Object> addList(final Panel panel, final String texto, final String bind, final String guardarEn, final Adapter adapter) {
    org.uqbar.arena.widgets.List<Object> _xblockexpression = null;
    {
      Operations.addLabelColours(panel, texto);
      org.uqbar.arena.widgets.List<Object> _list = new org.uqbar.arena.widgets.List<Object>(panel);
      final Procedure1<org.uqbar.arena.widgets.List<Object>> _function = new Procedure1<org.uqbar.arena.widgets.List<Object>>() {
        public void apply(final org.uqbar.arena.widgets.List<Object> it) {
          it.setWidth(130);
          it.setHeight(30);
          Binding<?, Selector<Object>, ListBuilder<Object>> _bindItemsToProperty = it.bindItemsToProperty(bind);
          _bindItemsToProperty.setAdapter(adapter);
          it.<Object, ControlBuilder>bindValueToProperty(guardarEn);
        }
      };
      _xblockexpression = ObjectExtensions.<org.uqbar.arena.widgets.List<Object>>operator_doubleArrow(_list, _function);
    }
    return _xblockexpression;
  }
  
  /**
   * Crea una lista, en el panel del primer parametro. Con el segundo parametro se crea un label de color y con el ultimo parametro se le la referencia de donde esta la lista
   */
  public static org.uqbar.arena.widgets.List<Object> addListWithTsize(final Panel panel, final String texto, final String bind, final String guardarEn, final Adapter adapter, final Integer alto, final Integer ancho) {
    org.uqbar.arena.widgets.List<Object> _xblockexpression = null;
    {
      Operations.addLabelColours(panel, texto, ancho);
      org.uqbar.arena.widgets.List<Object> _list = new org.uqbar.arena.widgets.List<Object>(panel);
      final Procedure1<org.uqbar.arena.widgets.List<Object>> _function = new Procedure1<org.uqbar.arena.widgets.List<Object>>() {
        public void apply(final org.uqbar.arena.widgets.List<Object> it) {
          it.setWidth((ancho).intValue());
          it.setHeight((alto).intValue());
          Binding<?, Selector<Object>, ListBuilder<Object>> _bindItemsToProperty = it.bindItemsToProperty(bind);
          _bindItemsToProperty.setAdapter(adapter);
          it.<Object, ControlBuilder>bindValueToProperty(guardarEn);
        }
      };
      _xblockexpression = ObjectExtensions.<org.uqbar.arena.widgets.List<Object>>operator_doubleArrow(_list, _function);
    }
    return _xblockexpression;
  }
  
  /**
   * Se crea un selector en el panel pasado por parametro, bindItems en el segundo parametro , en el tercero bindValue y en el
   * ultimo un adapter
   */
  public static Selector<Object> addSelector(final Panel panel, final String bindItems, final String bindValue, final Adapter adapter) {
    Selector<Object> _selector = new Selector<Object>(panel);
    final Procedure1<Selector<Object>> _function = new Procedure1<Selector<Object>>() {
      public void apply(final Selector<Object> it) {
        Binding<?, Selector<Object>, ListBuilder<Object>> _bindItemsToProperty = it.bindItemsToProperty(bindItems);
        _bindItemsToProperty.setAdapter(adapter);
        it.<Object, ControlBuilder>bindValueToProperty(bindValue);
      }
    };
    return ObjectExtensions.<Selector<Object>>operator_doubleArrow(_selector, _function);
  }
  
  public static Selector<Object> addSelector(final Panel panel, final String bindItems, final String bindValue) {
    Selector<Object> _selector = new Selector<Object>(panel);
    final Procedure1<Selector<Object>> _function = new Procedure1<Selector<Object>>() {
      public void apply(final Selector<Object> it) {
        it.bindItemsToProperty(bindItems);
        it.<Object, ControlBuilder>bindValueToProperty(bindValue);
      }
    };
    return ObjectExtensions.<Selector<Object>>operator_doubleArrow(_selector, _function);
  }
}
