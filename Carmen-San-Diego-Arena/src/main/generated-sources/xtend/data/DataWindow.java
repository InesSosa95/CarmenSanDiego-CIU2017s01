package data;

import org.uqbar.arena.layout.ColumnLayout;
import org.uqbar.arena.layout.VerticalLayout;
import org.uqbar.arena.widgets.Panel;
import org.uqbar.arena.windows.Dialog;
import org.uqbar.arena.windows.WindowOwner;

@SuppressWarnings("all")
public abstract class DataWindow<T extends Object> extends Dialog<T> {
  public DataWindow(final WindowOwner owner, final T model) {
    super(owner, model);
  }
  
  protected void createFormPanel(final Panel mainPanel) {
  }
  
  public void createMainTemplate(final Panel mainPanel) {
    this.giveTitle();
    final Panel panelAEditar = new Panel(mainPanel);
    ColumnLayout _columnLayout = new ColumnLayout(2);
    panelAEditar.setLayout(_columnLayout);
    Panel panelLeft = new Panel(panelAEditar);
    VerticalLayout _verticalLayout = new VerticalLayout();
    panelLeft.setLayout(_verticalLayout);
    this.addList(panelLeft);
    this.addBotons(panelLeft);
    Panel panelRight = new Panel(panelAEditar);
    VerticalLayout _verticalLayout_1 = new VerticalLayout();
    panelRight.setLayout(_verticalLayout_1);
    this.addCharacteristic(panelRight);
  }
  
  public abstract void giveTitle();
  
  public abstract void addList(final Panel panel);
  
  public abstract void addBotons(final Panel panel);
  
  public abstract void addCharacteristic(final Panel panel);
}
