package runnable

import view.MenuDeAccionesWindow
import org.uqbar.arena.Application
import dummyData.DummyData

class CarmenSanDiegoApplication extends Application {

	override protected createMainWindow() {
		val model =  new DummyData
		
		new MenuDeAccionesWindow(this, model.detective)
	}

	def static void main(String[] args) {
		new CarmenSanDiegoApplication().start()
	}
}
