package Operations

import org.uqbar.arena.widgets.Panel
import org.uqbar.arena.widgets.Label
import org.uqbar.arena.widgets.Button
import org.uqbar.arena.widgets.List
import java.awt.Color
import org.uqbar.arena.widgets.TextBox
import org.uqbar.arena.widgets.Selector
import org.uqbar.arena.bindings.NotNullObservable
import org.uqbar.arena.bindings.Adapter

class Operations {

	/**
	 * Crea un boton en el panel
	 */
	def static addBoton(Panel panel, String texto, ()=>void onclick) {
		new Button(panel) => [
			caption = texto
			onClick(onclick)
		]
	}

	/**
	 * agrega un boton con el nombre que recibe por parametro, realiza la accion que esta en el bloque onclick y tiene un bindEnable
	 */
	def static addBoton(Panel panel, String nombre, String bindEnabled, ()=>void onclick) {
		new Button(panel) => [
			caption = nombre
			onClick(onclick)
			bindEnabled(new NotNullObservable(bindEnabled))
			disableOnError
		]
	}

	def static addBotonBind(Panel panel, String bind, ()=>void onclick) {
		new Button(panel) =>[
			bindCaptionToProperty(bind)
			onClick(onclick)
		]
	}
	/**
	 * Crea un label en el panel del primer parametro. Con el segundo parametro es el texto del label y este se hace con un color de fondo gris
	 */
	def static addLabelColours(Panel panel, String texto) {
		new Label(panel) => [
			text = texto
			width = 203
			background = Color.lightGray
		]
	}

	/**
	 * Crea un label en el panel del primer parametro. Con el segundo parametro es el texto del label y este se hace con un color de fundo gris, y con un  ancho 
	 */
	def static addLabelColours(Panel panel, String texto, Integer ancho) {
		new Label(panel) => [
			text = texto
			width = ancho
			background = Color.lightGray
		]
	}

	/**
	 * Crea un label en el panel que se pasa por parametro con el texto del segundo parametro
	 */
	def static addText(Panel panel, String texto) {
		new Label(panel) => [
			text = texto
		]
	}

	def static addText(Panel panel, CharSequence texto) {
		new Label(panel) => [
			text = texto.toString
		]
	}

	def static addText(Panel panel, java.util.List<String> lista) {
		lista.forEach [
			Operations.addText(panel, it)
		]
	}

	/**
	 * Se crea un label con un texto que viene de una propiedad
	 */
	def static addTextProperty(Panel panel, String property) {
		new Label(panel) => [
			bindValueToProperty(property)
			width = 100
		]
	}

	def static addTextProperty(Panel panel, String property, Adapter adapter) {
		new Label(panel) => [
			bindValueToProperty(property).adapter = adapter
			width = 100
		]
	}

	/**
	 * Se crea un texBox en un panel horizontal que esta en el pasado por parametro. Con el segundo parametro se hace un bind
	 */
	def static addTextBox(Panel panel, String bind) {
		new TextBox(panel) => [
			width = 100
			bindValueToProperty(bind)

		]
	}

	/**
	 * Crea una lista en el panel del primer parametro. Con el segundo parametro se crea un label de color y con el ultimo
	 * parametro se le la referencia de donde esta la lista
	 */
	def static addList(Panel panel, String texto, String bind) {
		Operations.addLabelColours(panel, texto)
		new List(panel) => [
			width = 130
			height = 30
			bindItemsToProperty(bind)
		]
	}

	def static addList(Panel panel, String texto, String bind, Adapter adapter) {
		Operations.addLabelColours(panel, texto)
		new List(panel) => [
			width = 130
			height = 30
			bindItemsToProperty(bind).adapter = adapter
		]
	}

	def static addList(Panel panel, String texto, String bind, String guardarEn) {
		Operations.addLabelColours(panel, texto)
		new List(panel) => [
			width = 130
			height = 30
			bindItemsToProperty(bind)
			bindValueToProperty(guardarEn)
		]
	}

	def static addList(Panel panel, String texto, String bind, String guardarEn, Adapter adapter) {
		Operations.addLabelColours(panel, texto)
		new List(panel) => [
			width = 130
			height = 30
			bindItemsToProperty(bind).adapter = adapter
			bindValueToProperty(guardarEn)
		]
	}

	/**
	 * Crea una lista, en el panel del primer parametro. Con el segundo parametro se crea un label de color y con el ultimo parametro se le la referencia de donde esta la lista
	 */
	def static addListWithTsize(Panel panel, String texto, String bind, String guardarEn, Adapter adapter, Integer alto,
		Integer ancho) {
		Operations.addLabelColours(panel, texto, ancho)
		new List(panel) => [
			width = ancho
			height = alto
			bindItemsToProperty(bind).adapter = adapter
			bindValueToProperty(guardarEn)
		]
	}

	/**
	 * Se crea un selector en el panel pasado por parametro, bindItems en el segundo parametro , en el tercero bindValue y en el
	 * ultimo un adapter
	 */
	def static addSelector(Panel panel, String bindItems, String bindValue, Adapter adapter) {
		new Selector(panel) => [
			bindItemsToProperty(bindItems).adapter = adapter
			bindValueToProperty(bindValue)
		]
	}

	def static addSelector(Panel panel, String bindItems, String bindValue) {
		new Selector(panel) => [
			bindItemsToProperty(bindItems)
			bindValueToProperty(bindValue)
		]
	}

}
