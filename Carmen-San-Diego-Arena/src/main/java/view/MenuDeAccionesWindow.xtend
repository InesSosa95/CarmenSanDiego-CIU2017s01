package view

import Operations.Operations
import applicationModel.ExpedienteApplicationModel
import applicationModel.JuegoApplicationModel
import applicationModel.MapamundiApplicationModel
import dominio.Detective
import org.uqbar.arena.layout.HorizontalLayout
import org.uqbar.arena.layout.VerticalLayout
import org.uqbar.arena.widgets.Label
import org.uqbar.arena.widgets.Panel
import org.uqbar.arena.windows.SimpleWindow
import org.uqbar.arena.windows.WindowOwner

class MenuDeAccionesWindow extends SimpleWindow<Detective> {

	new(WindowOwner parent, Detective model) {
		super(parent, model)
	}

	override protected addActions(Panel actionsPanel) {}

	override protected createFormPanel(Panel mainPanel) {}

	override createMainTemplate(Panel mainPanel) {
		this.setTitle("¿Donde esta Carmen San Diego?")
		mainPanel.setLayout(new VerticalLayout)
		new Label(mainPanel).setText("¿Que haremos ahora detective?")

		var panel = new Panel(mainPanel)
		panel.setLayout(new HorizontalLayout)

		Operations.addBoton(panel, "Resolver Misterio", [| new InicioDeJuegoWindow(this, (new JuegoApplicationModel)).open
			
			//ACA TENDRIA QUE IR COMO EL ARRANQUE DEL JUEGO
		])

		Operations.addBoton(panel, "Mapamundi", [|new MapamundiWindow(this, (new MapamundiApplicationModel)).open])
		Operations.addBoton(panel, "Expedientes", [|new ExpedienteWindowEdit(this, new ExpedienteApplicationModel).open])

	}
}
