package view

import Operations.Operations
import org.uqbar.arena.bindings.PropertyAdapter
import org.uqbar.arena.layout.HorizontalLayout
import org.uqbar.arena.widgets.Panel
import org.uqbar.arena.windows.WindowOwner
import org.uqbar.arena.bindings.Adapter

abstract class EditionWindowConSelector<T> extends EditionWindows<T> {
	
	new(WindowOwner owner, T model) {
		super(owner, model)
	}
	
	override addInputYBoton(Panel panel) {
		val panelAux = new Panel(panel)
		panelAux.setLayout(new HorizontalLayout)
		Operations.addSelector(panelAux, listaDondeSeleccionarEnModel, componenteEnModel(), adapterNecesario())
		Operations.addBoton(panelAux, "Agregar", metodoAgregarEnModel())
	}
	
	
	
	override addList(Panel panel) {
		Operations.addList(panel, tituloDeListaAEditar(), listaAEditarEnModel, caracteristicaSeleccionada,
			adapterNecesario())
	}
	
	abstract def String caracteristicaSeleccionada()	
	abstract def Adapter adapterNecesario()
	abstract override def String componenteEnModel()
	abstract def String listaDondeSeleccionarEnModel()
	abstract override def String caracteristicaEnModel()

	
}