package view

import Operations.Operations
import org.uqbar.arena.bindings.PropertyAdapter
import org.uqbar.arena.layout.HorizontalLayout
import org.uqbar.arena.layout.VerticalLayout
import org.uqbar.arena.widgets.Panel
import org.uqbar.arena.windows.Dialog
import org.uqbar.arena.windows.WindowOwner

abstract class DialogoWindow<T> extends Dialog<T> {
	
	new(WindowOwner owner, T model) {
		super(owner, model)
	}
	def void botonAceptar(Panel panel){
		val botonera = new Panel(panel)
		botonera.setLayout(new HorizontalLayout)
		Operations.addBoton(botonera, "Aceptar", aceptarMetodo() )
	}
	
	def void presentarCaracteristica(Panel editorPanel, 
									String nombreCaracteristica, 
									String nombreBoton, 
									()=>void accionBoton, 
									String nombreDeLista, 
									String listaEnModelObject,
									PropertyAdapter adapter){
		
		val caracteristicaPanel = new Panel(editorPanel)
		caracteristicaPanel.setLayout(new HorizontalLayout)

		Operations.addText(caracteristicaPanel, nombreCaracteristica)
		Operations.addBoton(caracteristicaPanel, nombreBoton, accionBoton)

		val listaCaracteristicaPanel = new Panel(editorPanel)
		listaCaracteristicaPanel.setLayout(new VerticalLayout)
		Operations.addList(listaCaracteristicaPanel, nombreDeLista, listaEnModelObject, adapter)
	
	}
	
	
	def void presentarCaracteristica(Panel editorPanel, String nombreCaracteristica, String nombreBoton, ()=>void accionBoton, String nombreDeLista, String listaEnModelObject){
		
		val caracteristicaPanel = new Panel(editorPanel)
		caracteristicaPanel.setLayout(new HorizontalLayout)

		Operations.addText(caracteristicaPanel, nombreCaracteristica)
		Operations.addBoton(caracteristicaPanel, nombreBoton, accionBoton)

		val listaCaracteristicaPanel = new Panel(editorPanel)
		listaCaracteristicaPanel.setLayout(new VerticalLayout)
		Operations.addList(listaCaracteristicaPanel, nombreDeLista, listaEnModelObject)
	
	}

	
	abstract def ()=>void aceptarMetodo()

	abstract def void giveTitle()

	abstract def void addName(Panel panel)
}