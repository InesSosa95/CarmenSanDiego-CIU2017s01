package view

import Operations.Operations
import applicationModel.ExpedienteApplicationModel
import dominio.Villano
import org.uqbar.arena.bindings.ObservableProperty
import org.uqbar.arena.bindings.PropertyAdapter
import org.uqbar.arena.layout.ColumnLayout
import org.uqbar.arena.layout.VerticalLayout
import org.uqbar.arena.widgets.Label
import org.uqbar.arena.widgets.List
import org.uqbar.arena.widgets.Panel
import org.uqbar.arena.widgets.tables.Column
import org.uqbar.arena.widgets.tables.Table
import org.uqbar.arena.windows.SimpleWindow
import org.uqbar.arena.windows.WindowOwner

import static extension org.uqbar.arena.xtend.ArenaXtendExtensions.*

class ExpedienteWindowVista extends SimpleWindow<ExpedienteApplicationModel> {

	new(WindowOwner parent, ExpedienteApplicationModel model) {
		super(parent, model)
		title = "Resolviendo misterios"
	}

	override protected addActions(Panel parent) {
	}

	override protected createFormPanel(Panel parent) {

		val contenedorDeColumnas = new Panel(parent)
		contenedorDeColumnas.layout = new ColumnLayout(2)

		val villanos = new Panel(contenedorDeColumnas)
		val caracteristicasDeVillanoSeleccionado = new Panel(contenedorDeColumnas)

		new Label(caracteristicasDeVillanoSeleccionado) => [
			text = "Nombre: " // + modelObject.villanoSeleccionado.nombre
		]

		new Label(caracteristicasDeVillanoSeleccionado) => [
			text = "Sexo: " // + modelObject.villanoSeleccionado.sexo
		]

		new Label(caracteristicasDeVillanoSeleccionado) => [
			text = "Señas: "
		]
		Operations.addLabelColours(caracteristicasDeVillanoSeleccionado, "SEÑA")
		listaDeSenhas(caracteristicasDeVillanoSeleccionado)

		new Label(caracteristicasDeVillanoSeleccionado) => [
			text = "Hobbies: "
		]

		Operations.addLabelColours(caracteristicasDeVillanoSeleccionado, "HOBBIE")
		listaDeHobbies(caracteristicasDeVillanoSeleccionado)

		Operations.addLabelColours(villanos, "VILLANO")
		listaDeVillanos(villanos)

	}

	def protected listaDeVillanos(Panel parent) {
		val listaDeVillanos = new Panel(parent)
		listaDeVillanos.layout = new VerticalLayout
		Operations.addListWithTsize(parent, "Villanos", "expediente.villanos", "villanoSeleccionado",
			new PropertyAdapter(Villano, "nombre"), 300, 100)
	}

	def describeTablaVillanos(Table<Villano> table) {
		new Column<Villano>(table) => [
			title = "Villano"
			bindContentsToProperty("nombre")
		]
	}

	def protected tablaDeSenhas(Panel parent) {
		val tablaDeSenhas = new Table<String>(parent, typeof(String)) => [
			items <=> "expediente.villanos"
			value <=> "villanoSeleccionado"
		]

		this.describeTablaSenhas(tablaDeSenhas)
	}

	def describeTablaSenhas(Table<String> table) {
		new Column<String>(table) => [
			title = "Señas"
			bindContentsToProperty("senha")
		]
	}

	def protected tablaDeHobbies(Panel parent) {
		val tablaDeSenhas = new Table<String>(parent, typeof(String)) => [
			items <=> "expediente.villanos"
			value <=> "villanoSeleccionado"
		]

		this.describeTablaHobbies(tablaDeSenhas)
	}

	def describeTablaHobbies(Table<String> table) {
		new Column<String>(table) => [
			title = "Señas"
			bindContentsToProperty("senha")
		]

	}

	def listaDeSenhas(Panel parent) {
		val senhasAgregadas = new Panel(parent)
		senhasAgregadas.layout = new VerticalLayout
		new List<String>(senhasAgregadas) => [
			bindItems(new ObservableProperty(model, "villanoSeleccionado.senhas"))
			width = 200
			height = 100
		]
	}

	def listaDeHobbies(Panel parent) {
		val senhasAgregadas = new Panel(parent)
		senhasAgregadas.layout = new VerticalLayout
		new List<String>(senhasAgregadas) => [
			bindItems(new ObservableProperty(model, "villanoSeleccionado.hobbies"))
			width = 200
			height = 100
		]
	}

}
