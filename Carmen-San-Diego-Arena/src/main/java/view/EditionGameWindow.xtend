package view

import org.uqbar.arena.windows.Dialog
import applicationModel.JuegoApplicationModel
import org.uqbar.arena.windows.WindowOwner
import org.uqbar.arena.widgets.Panel
import org.uqbar.arena.layout.VerticalLayout


 abstract  class EditionGameWindow extends Dialog<JuegoApplicationModel> {
	
	new(WindowOwner owner, JuegoApplicationModel model) {
		super(owner, model)
	}
	
	override protected createFormPanel(Panel mainPanel) {}
	
	override createMainTemplate(Panel mainPanel){
		
		this.obtenerTitulo()
			
		var panelAux = new Panel(mainPanel)
		panelAux.layout = new VerticalLayout
		
		this.agregartexto(panelAux)
		this.agregarInPut(panelAux)
		this.agregarBotones(panelAux)
		
	}
	
	abstract def void obtenerTitulo()
	
	abstract def void agregarBotones(Panel panel) 
	
	abstract def void agregarInPut(Panel panel) 
	
	abstract def void agregartexto(Panel panel) 
	
	 
	
}