package view

import Operations.Operations
import applicationModel.MapamundiApplicationModel
import applicationModel.PaisApplicationModel
import data.DataWindow
import dominio.Lugar
import dominio.Pais
import org.uqbar.arena.bindings.PropertyAdapter
import org.uqbar.arena.layout.HorizontalLayout
import org.uqbar.arena.layout.VerticalLayout
import org.uqbar.arena.widgets.Panel
import org.uqbar.arena.windows.WindowOwner
import view.pais.EditarPaisWindow
import view.pais.NuevoPaisWindow

class MapamundiWindow extends DataWindow<MapamundiApplicationModel> {

	new(WindowOwner owner, MapamundiApplicationModel model) {
		super(owner, model)
	}

	override giveTitle() {
		this.setTitle("Mapamundi")
	}

	override addList(Panel panel) {
		Operations.addListWithTsize(panel, "Paises", "mapamundi.paises", "paisSeleccionado",
			new PropertyAdapter(Pais, "nombre"), 300, 100)
	}

	override addBotons(Panel panel) {
		var botonsPanel = new Panel(panel)
		botonsPanel.setLayout(new VerticalLayout)

		Operations.addBoton(botonsPanel, "Eliminar", "paisSeleccionado", [|modelObject.eliminarPais])
		Operations.addBoton(botonsPanel, "Editar", "paisSeleccionado", [|
			new EditarPaisWindow(this, new PaisApplicationModel(modelObject.paisSeleccionado)).open
		])
		Operations.addBoton(botonsPanel, "Nuevo", [|
			new NuevoPaisWindow(this, new PaisApplicationModel(modelObject.nuevoPais())).open
		])
	}

	override addCharacteristic(Panel panel) {
		//Label nombre
		var nombrePanel = new Panel(panel)
		nombrePanel.setLayout(new HorizontalLayout)
		Operations.addText(nombrePanel, "Nombre: ")
		Operations.addTextProperty(nombrePanel, "paisSeleccionado.nombre")

		// Label caracteristica
		val caracteristicaPanel = new Panel(panel)
		caracteristicaPanel.setLayout(new HorizontalLayout)

		Operations.addText(caracteristicaPanel, "Características")

		val listaCaracteristicaPanel = new Panel(panel)
		listaCaracteristicaPanel.setLayout(new VerticalLayout)
		Operations.addList(listaCaracteristicaPanel, "Características", "paisSeleccionado.caracteristicas")

		// Label conexiones
		val conexionesPanel = new Panel(panel)
		conexionesPanel.setLayout(new HorizontalLayout)

		Operations.addText(conexionesPanel, "Conexiones")

		val listaConexionesPanel = new Panel(panel)
		listaConexionesPanel.setLayout(new VerticalLayout)
		Operations.addList(listaConexionesPanel, "Conexiones", "paisSeleccionado.conexiones",
		new PropertyAdapter(Pais, "nombre"))
			

		// Label lugares
		val lugaresPanel = new Panel(panel)
		lugaresPanel.setLayout(new HorizontalLayout)

		Operations.addText(lugaresPanel, "Lugares de Interés")

		val listaLugaresPanel = new Panel(panel)
		listaLugaresPanel.setLayout(new VerticalLayout)
		Operations.addList(listaLugaresPanel, "Lugares de Interés", "paisSeleccionado.lugares",
			new PropertyAdapter(Lugar, "nombreDelLugar"))
	}

}
