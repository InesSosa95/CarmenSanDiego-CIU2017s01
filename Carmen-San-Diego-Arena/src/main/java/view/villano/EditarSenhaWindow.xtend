package view.villano

import Operations.Operations
import applicationModel.VillanoApplicationModel
import org.uqbar.arena.layout.HorizontalLayout
import org.uqbar.arena.widgets.Panel
import org.uqbar.arena.windows.WindowOwner
import view.EditionWindows

class EditarSenhaWindow extends EditionWindows<VillanoApplicationModel>{
	
	new(WindowOwner owner, VillanoApplicationModel model) {
		super(owner, model)
	}

	override giveTitle() {
		title = "Editar Señas"
	}
	
	override metodoEliminarEnModel(){
		[|modelObject.eliminarSenha]
	}

	override metodoAgregarEnModel() {
		[|modelObject.agregarSenha]
	}
	
	override listaAEditarEnModel() {
		"villanoSeleccionado.senhas"
	}
	
	override tituloDeListaAEditar() {
		"Señas"
	}
		
}