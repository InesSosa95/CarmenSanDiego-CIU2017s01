package view.villano

import Operations.Operations
import applicationModel.VillanoApplicationModel
import org.uqbar.arena.layout.HorizontalLayout
import org.uqbar.arena.widgets.Panel
import org.uqbar.arena.windows.WindowOwner

class EditarVillanoWindow extends VillanoWindow {

	new(WindowOwner owner, VillanoApplicationModel model) {
		super(owner, model)
	}
	
	override giveTitle() {
		title= "Expediente - Editar Villano"
}

	override addName(Panel panel) {
		val panelAux = new Panel (panel)
		panelAux.setLayout(new HorizontalLayout)
		
		Operations.addText(panelAux, "Nombre: ")
		Operations.addTextProperty(panelAux, "villanoSeleccionado.nombre")	}
		
		override aceptarMetodo() {
			[|this.close]
		}
		
}

