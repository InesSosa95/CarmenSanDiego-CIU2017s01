package view.villano

import Operations.Operations
import applicationModel.VillanoApplicationModel
import dominio.Expediente
import dominio.Villano
import org.uqbar.arena.layout.HorizontalLayout
import org.uqbar.arena.widgets.Panel
import org.uqbar.arena.windows.WindowOwner

class NuevoVillanoWindow extends VillanoWindow{
	
	new(WindowOwner owner, VillanoApplicationModel model) {
		super(owner, model)
		model.villanoSeleccionado = new Villano()
	}
	
	
	override addName(Panel panel) {
		var panelAux = new Panel(panel)
		panelAux.setLayout(new HorizontalLayout)
		Operations.addText(panelAux, "Nombre: ")
		Operations.addTextBox(panelAux, "nombre")
	}
	
	override ()=>void aceptarMetodo(){
		[|modelObject.cambiarNombreVillano
		  Expediente.obtenerInstancia.agregarVillano(modelObject.villanoSeleccionado)
		  this.close
		] 
	}
	
	override giveTitle() {
		title = "Expediente - Nuevo Villano"
	}
	
}