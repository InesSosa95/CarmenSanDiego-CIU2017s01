package view.villano

import Operations.Operations
import applicationModel.VillanoApplicationModel
import org.uqbar.arena.layout.HorizontalLayout
import org.uqbar.arena.widgets.Panel
import org.uqbar.arena.windows.WindowOwner
import view.EditionWindows

class EditarHobbiesWindow extends EditionWindows<VillanoApplicationModel>{
	
	new(WindowOwner owner, VillanoApplicationModel model) {
		super(owner, model)
	}

	override giveTitle() {
		title = "Editar Hobbies"
	}
	
	override metodoEliminarEnModel(){
		[|modelObject.eliminarHobbies]
	}

	override metodoAgregarEnModel() {
		[|modelObject.agregarHobbies]
	}
	
	override listaAEditarEnModel() {
		 "villanoSeleccionado.hobbies"
	}
	
	override tituloDeListaAEditar() {
		"Hobbies"
	}
		
}