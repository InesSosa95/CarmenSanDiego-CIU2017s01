package view.villano

import Operations.Operations
import applicationModel.VillanoApplicationModel
import org.uqbar.arena.bindings.PropertyAdapter
import org.uqbar.arena.layout.HorizontalLayout
import org.uqbar.arena.layout.VerticalLayout
import org.uqbar.arena.widgets.Panel
import org.uqbar.arena.windows.WindowOwner
import view.DialogoWindow

abstract class VillanoWindow extends DialogoWindow<VillanoApplicationModel> {

	new(WindowOwner owner, VillanoApplicationModel model) {
		super(owner, model)
	}

	override protected createFormPanel(Panel mainPanel) {}

	override createMainTemplate(Panel mainPanel) {

		giveTitle

		val editorPanel = new Panel(mainPanel)
		editorPanel.setLayout(new VerticalLayout)

		addName(editorPanel)

		presentarCaracteristica(editorPanel, "Señas", "EditarSeñas", [|new EditarSenhaWindow(this, modelObject).open], "Señas", "villanoSeleccionado.senhas")

		presentarCaracteristica(editorPanel, "Hobbies", "Editar Hobbies", [|new EditarHobbiesWindow(this, modelObject).open], "Hobbies", "villanoSeleccionado.hobbies" )

		botonAceptar(editorPanel)		
	}
	
		
	
	
	
}

