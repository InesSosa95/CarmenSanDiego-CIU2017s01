package view.pais

import applicationModel.PaisApplicationModel
import dominio.Pais
import org.uqbar.arena.bindings.PropertyAdapter
import org.uqbar.arena.windows.WindowOwner
import view.EditionWindowConSelector

class EditarConexionesWindow extends EditionWindowConSelector<PaisApplicationModel> {

	new(WindowOwner owner, PaisApplicationModel model) {
		super(owner, model)
	}

	override giveTitle() {
		this.setTitle("Editar Conexiones")
	}
	
	override listaDondeSeleccionarEnModel() {
		"paisesPosibles"
	}
	
	override caracteristicaEnModel() {
		"nuevaConexion"
	}
	
	override adapterNecesario() {
		return new PropertyAdapter(Pais,"nombre") 
	}
	
	override listaAEditarEnModel() {
		"pais.conexiones"
	}
	
	override metodoAgregarEnModel() {
		[|modelObject.agregarConexion]
	}
	
	override metodoEliminarEnModel() {
		[|modelObject.eliminarConexion]
	}
	
	override tituloDeListaAEditar() {
		"Conexiones"
	}
	
	override componenteEnModel() {
		"conexionSeleccionada"
	}
	
	override caracteristicaSeleccionada() {
		"conexionSeleccionada"
	}
	
	

}
