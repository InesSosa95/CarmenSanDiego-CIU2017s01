package view.pais

import applicationModel.PaisApplicationModel
import dominio.Mapamundi
import dominio.Pais
import org.uqbar.arena.windows.WindowOwner

class NuevoPaisWindow extends PaisWindow{
	
	new(WindowOwner owner, PaisApplicationModel model) {
		super(owner, model)
		model.pais = new Pais()
	}
	
	override giveTitle() {
		title = "Mapamundi - Nuevo Pais" 
	}
	
	
	override ()=>void aceptarMetodo(){
		[|modelObject.cambiarNombrePais
		  val mapa=Mapamundi.obtenerInstancia
		  modelObject.verificarQueNoExistaPaisConIgualNombre
		  mapa.agregarPais(modelObject.pais)
		  this.close
		] 
	}
}
