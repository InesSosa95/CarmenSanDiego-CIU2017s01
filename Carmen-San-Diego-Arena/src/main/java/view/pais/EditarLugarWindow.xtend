package view.pais

import Operations.Operations
import applicationModel.PaisApplicationModel
import dominio.Lugar
import org.uqbar.arena.bindings.PropertyAdapter
import org.uqbar.arena.layout.HorizontalLayout
import org.uqbar.arena.widgets.Panel
import org.uqbar.arena.windows.WindowOwner
import view.EditionWindowConSelector

class EditarLugarWindow extends EditionWindowConSelector<PaisApplicationModel> {


	new(WindowOwner owner, PaisApplicationModel model) {
		super(owner, model)
		
	}
		
	override aceptarMetodo(){
		return [|modelObject.verificarLugares()
			this.close()
		]
	}
	
	override caracteristicaSeleccionada() {
		"lugarSeleccionada"
	}
	
	override listaAEditarEnModel() {
		"pais.lugares"
	}
	
	override adapterNecesario() {
		new PropertyAdapter(Lugar, "nombreDelLugar")
	}
	
	override componenteEnModel() {
		"nuevoLugar"
		}
	
	override listaDondeSeleccionarEnModel() {
		"lugaresPosibles"
	}
	
	override caracteristicaEnModel() {"nuevoLugar"}
	
	override giveTitle() {
		this.setTitle("Editar Lugares")
	}
	
	override metodoAgregarEnModel() {
		 [|modelObject.agregarLugar]
	}
	
	override metodoEliminarEnModel() {
		 [|modelObject.eliminarLugar]
		 }
	
	override tituloDeListaAEditar() {
		"Lugares De Interes"
	}
	
}
