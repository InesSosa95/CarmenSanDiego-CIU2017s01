package view.pais

import Operations.Operations
import applicationModel.PaisApplicationModel
import org.uqbar.arena.layout.HorizontalLayout
import org.uqbar.arena.widgets.Panel
import org.uqbar.arena.windows.WindowOwner
import view.EditionWindows

class EditarCaracteristicaWindow extends EditionWindows<PaisApplicationModel> {

	new(WindowOwner owner, PaisApplicationModel model) {
		super(owner, model)
	}

	override giveTitle() {
		title = "Editar Características"
	}
	
	override metodoEliminarEnModel(){
		[|modelObject.eliminarCaracteristica]
	}

	override metodoAgregarEnModel() {
		[|modelObject.agregarCaracteristica]
	}
	
	override listaAEditarEnModel() {
		"pais.caracteristicas"
	}
	
	override tituloDeListaAEditar() {
		"Caracteristicas"
	}
	
}
