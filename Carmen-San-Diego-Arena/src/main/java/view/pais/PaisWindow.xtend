package view.pais

import Operations.Operations
import applicationModel.PaisApplicationModel
import dominio.Lugar
import dominio.Pais
import org.uqbar.arena.bindings.PropertyAdapter
import org.uqbar.arena.layout.HorizontalLayout
import org.uqbar.arena.layout.VerticalLayout
import org.uqbar.arena.widgets.Panel
import org.uqbar.arena.windows.WindowOwner
import view.DialogoWindow

abstract class PaisWindow extends DialogoWindow<PaisApplicationModel> {

	new(WindowOwner owner, PaisApplicationModel model) {
		super(owner, model)
	}

	override protected createFormPanel(Panel mainPanel) {}

	override createMainTemplate(Panel mainPanel) {

		giveTitle
	
		val editorPanel = new Panel(mainPanel)
		editorPanel.setLayout(new VerticalLayout)
		addName(editorPanel)
	
	    presentarCaracteristica(editorPanel, "Caracteristicas", "Editar Caracteristicas", [|new EditarCaracteristicaWindow(this, modelObject).open], "Caracteristicas", "pais.caracteristicas")
		presentarCaracteristica(editorPanel, "Conexiones","Editar Conexiones", [|new EditarConexionesWindow(this, modelObject).open], "Conexiones", "pais.conexiones", new PropertyAdapter(Pais, "nombre"))
		presentarCaracteristica(editorPanel, "Lugares de Interes", "Editar Lugares",[|new EditarLugarWindow(this, modelObject).open],  "Lugares de Interés", "pais.lugares",
								new PropertyAdapter(Lugar, "nombreDelLugar"))
		botonAceptar(editorPanel)
		
	}
	
	override addName(Panel panel) {
		var panelAux = new Panel(panel)
		panelAux.setLayout(new HorizontalLayout)
		Operations.addText(panelAux, "Nombre: ")
		Operations.addTextBox(panelAux, "pais.nombre")
	}
}
