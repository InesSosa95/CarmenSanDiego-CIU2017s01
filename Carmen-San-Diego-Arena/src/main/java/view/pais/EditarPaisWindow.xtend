package view.pais

import applicationModel.PaisApplicationModel
import org.uqbar.arena.windows.WindowOwner

class EditarPaisWindow extends PaisWindow {

	new(WindowOwner owner, PaisApplicationModel model) {
		super(owner, model)
	}

	override giveTitle() {
		title = "Mapamundi - Editar Pais"
	}

	override aceptarMetodo() {
		[|this.close]
	}

}
