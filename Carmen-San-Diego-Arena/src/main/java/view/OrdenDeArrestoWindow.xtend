package view

import org.uqbar.arena.windows.WindowOwner
import applicationModel.JuegoApplicationModel
import org.uqbar.arena.widgets.Panel
import org.uqbar.arena.layout.HorizontalLayout
import Operations.Operations
import org.uqbar.arena.bindings.NotNullObservable
import org.uqbar.arena.bindings.PropertyAdapter
import dominio.Villano

class OrdenDeArrestoWindow extends EditionGameWindow {

	new(WindowOwner owner, JuegoApplicationModel model) {
		super(owner, model)
	}

	override obtenerTitulo() {
		title = '''Resolviendo: «modelObject.detective.casoAResolver.objeto»'''

	// detective.casoAResolver.objeto
	}

	override agregartexto(Panel panel) {
		var ordenPanel = new Panel(panel)
		ordenPanel.layout = new HorizontalLayout

		Operations.addText(ordenPanel, "Orden de arresto emitida contra: ") => [
			bindEnabled(new NotNullObservable("ordenDeArresto"))
		]
		Operations.addTextProperty(ordenPanel, "ordenDeArresto.nombre") => [
			bindEnabled(new NotNullObservable("ordenDeArresto"))

		]
	}

	override agregarBotones(Panel panel) {
		Operations.addBoton(panel, "Generar Orden de Arresto", [|modelObject.pedirOrdenDeArresto() this.close])

	}

	override agregarInPut(Panel panel) {
		var selectorPanel = new Panel(panel)
		selectorPanel.layout = new HorizontalLayout

		Operations.addText(selectorPanel, "Villanos:")
		Operations.addSelector(selectorPanel, "todosLosVillanos", "villanoSeleccionado",
			new PropertyAdapter(Villano, "nombre"))

	}

}
