package view

import Operations.Operations
import dominio.Lugar
import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.arena.layout.HorizontalLayout
import org.uqbar.arena.widgets.Panel
import org.uqbar.arena.windows.WindowOwner

@Accessors
class VisitarLugarWindow extends DialogoWindow<Lugar> {

	new(WindowOwner owner, Lugar model) {
		super(owner, model)
	}

	override addName(Panel panel) {
	}

	override protected createFormPanel(Panel panel) {

		Operations.addList(panel, "Pistas", "damePistas")
		agregarBotones(panel)
	}

	def agregarBotones(Panel panel) {

		var panelAux = new Panel(panel)
		panelAux.layout = new HorizontalLayout

		Operations.addBoton(panelAux, "Continuar", [|this.close])
	}

	override aceptarMetodo() {
	}

	override giveTitle() {
	}

}
