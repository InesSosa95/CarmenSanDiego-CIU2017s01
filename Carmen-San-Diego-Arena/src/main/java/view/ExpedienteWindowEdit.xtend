package view

import Operations.Operations
import applicationModel.ExpedienteApplicationModel
import applicationModel.VillanoApplicationModel
import data.DataWindow
import dominio.Villano
import org.uqbar.arena.bindings.PropertyAdapter
import org.uqbar.arena.layout.HorizontalLayout
import org.uqbar.arena.layout.VerticalLayout
import org.uqbar.arena.widgets.Panel
import org.uqbar.arena.windows.WindowOwner
import view.villano.NuevoVillanoWindow
import view.villano.EditarVillanoWindow

class ExpedienteWindowEdit extends DataWindow<ExpedienteApplicationModel> {

	new(WindowOwner owner, ExpedienteApplicationModel model) {
		super(owner, model)
	}

	override giveTitle() {
		this.setTitle("Expediente")
	}

	override addList(Panel panel) {
		Operations.addListWithTsize(panel, "Villanos", "expediente.villanos", "villanoSeleccionado",
			new PropertyAdapter(Villano, "nombre"), 300, 100)
	}

	override addBotons(Panel panel) {
		var botonsPanel = new Panel(panel)
		botonsPanel.setLayout(new VerticalLayout)

		Operations.addBoton(botonsPanel, "Editar", "villanoSeleccionado", [|
			new EditarVillanoWindow(this, new VillanoApplicationModel(modelObject.villanoSeleccionado)).open
		])
		Operations.addBoton(botonsPanel, "Nuevo", [|
			new NuevoVillanoWindow(this, new VillanoApplicationModel()).open
		])
	}

	override addCharacteristic(Panel panel) {
		// Label nombre
		var nombrePanel = new Panel(panel)
		nombrePanel.setLayout(new HorizontalLayout)
		Operations.addText(nombrePanel, "Nombre: ")
		Operations.addTextProperty(nombrePanel, "villanoSeleccionado.nombre")

		// Label sexo
		var nombrePanel2 = new Panel(panel)
		nombrePanel2.setLayout(new HorizontalLayout)
		Operations.addText(nombrePanel2, "Sexo: ")
		Operations.addTextProperty(nombrePanel2, "villanoSeleccionado.sexo")

		// Label caracteristica
		val caracteristicaPanel = new Panel(panel)
		caracteristicaPanel.setLayout(new HorizontalLayout)

		Operations.addText(caracteristicaPanel, "Señas Particulares:")

		val listaCaracteristicaPanel = new Panel(panel)
		listaCaracteristicaPanel.setLayout(new VerticalLayout)
		Operations.addList(listaCaracteristicaPanel, "Seña", "villanoSeleccionado.senhas")

		// Label conexiones
		val conexionesPanel = new Panel(panel)
		conexionesPanel.setLayout(new HorizontalLayout)

		Operations.addText(conexionesPanel, "Hobbies")

		val listaConexionesPanel = new Panel(panel)
		listaConexionesPanel.setLayout(new VerticalLayout)
		Operations.addList(listaConexionesPanel, "Hobbie", "villanoSeleccionado.hobbies")
	}
}
