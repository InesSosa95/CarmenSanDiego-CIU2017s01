package view

import org.uqbar.arena.windows.Dialog
import org.uqbar.arena.windows.WindowOwner
import org.uqbar.arena.widgets.Panel
import org.uqbar.arena.layout.VerticalLayout
import org.uqbar.arena.layout.HorizontalLayout
import Operations.Operations
import org.uqbar.arena.bindings.Adapter

abstract class EditionWindows<T> extends Dialog<T> {

	new(WindowOwner owner, T model) {
		super(owner, model)
	}

	override createMainTemplate(Panel mainPanel) {
		this.giveTitle

		val editorPanel = new Panel(mainPanel)
		editorPanel.setLayout(new VerticalLayout)

		// Lista
		this.addList(editorPanel)

		// Boton eliminar
		val botonEliminar = new Panel(editorPanel)
		botonEliminar.setLayout(new HorizontalLayout)
		this.addBotonDelete(botonEliminar)

		// input
		this.addInputYBoton(editorPanel)

		// Boton aceptar
		val botonAgregar = new Panel(editorPanel)
		botonAgregar.setLayout(new HorizontalLayout)

		Operations.addBoton(botonAgregar, "Aceptar", aceptarMetodo())

	}

	
	
	def addInputYBoton(Panel panel) {
		val panelAux = new Panel(panel)
		panelAux.setLayout(new HorizontalLayout)
		Operations.addTextBox(panelAux, caracteristicaEnModel())
		Operations.addBoton(panelAux, "Agregar", metodoAgregarEnModel())
	}

    def String caracteristicaEnModel(){
    	"nuevaCaracteristica"
    }
    
    def String componenteEnModel(){
		"caracteristicaSeleccionada"
	}
	
	def void addBotonDelete(Panel panel){
		Operations.addBoton(panel, "Eliminar", metodoEliminarEnModel())
	}
	
	def void addList(Panel panel){
		Operations.addList(panel, tituloDeListaAEditar(), listaAEditarEnModel(), componenteEnModel())
	}
	

	abstract def void giveTitle()
	abstract def String listaAEditarEnModel()
	abstract def ()=>void metodoAgregarEnModel()
	abstract def ()=>void metodoEliminarEnModel()
	abstract def String tituloDeListaAEditar()

	override protected createFormPanel(Panel mainPanel) {}
	
	def ()=>void aceptarMetodo(){
		return [|this.close]
	}
}
