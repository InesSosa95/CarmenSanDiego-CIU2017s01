package view

import Operations.Operations
import applicationModel.JuegoApplicationModel
import dominio.Pais
import org.uqbar.arena.bindings.PropertyAdapter
import org.uqbar.arena.layout.HorizontalLayout
import org.uqbar.arena.widgets.Panel
import org.uqbar.arena.windows.WindowOwner

class ViajarWindow extends EditionGameWindow {

	new(WindowOwner owner, JuegoApplicationModel model) {
		super(owner, model)
	}

	override obtenerTitulo() {
		title = "Viajar"
	}

	override agregartexto(Panel panel) {

		var panelAux = new Panel(panel)
		panelAux.layout = new HorizontalLayout

		Operations.addText(panelAux, "Estas en: ")
		Operations.addTextProperty(panelAux, "detective.paisActual.nombre")
	}

	override agregarInPut(Panel panel) {
		var selectorPanel = new Panel(panel)
		selectorPanel.layout = new HorizontalLayout

		Operations.addText(selectorPanel, "Conexiones:")
		Operations.addSelector(selectorPanel, "todasLasConexiones", "paisSeleccionado",
			new PropertyAdapter(Pais, "nombre"))

	}

	override agregarBotones(Panel panel) {

		var panelAux = new Panel(panel)
		panelAux.layout = new HorizontalLayout

		Operations.addBoton(panelAux, "Volver al Pais anterior", [|this.close])
		Operations.addBoton(panelAux, "Viajar", [|modelObject.viajar() this.close])
	}
}
