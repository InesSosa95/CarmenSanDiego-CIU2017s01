package view

import applicationModel.JuegoApplicationModel
import java.awt.Color
import org.uqbar.arena.layout.VerticalLayout
import org.uqbar.arena.widgets.Panel
import org.uqbar.arena.windows.Dialog
import org.uqbar.arena.windows.WindowOwner
import Operations.Operations

class InicioDeJuegoWindow extends Dialog<JuegoApplicationModel> {

	new(WindowOwner owner, JuegoApplicationModel model) {
		super(owner, model)
	}

	override protected createFormPanel(Panel mainPanel) {
		//No se usa porque se usa el createMainTemplate
	}

	override createMainTemplate(Panel mainPanel) {
		this.title = modelObject.nombreCaso

		var editorPanel = new Panel(mainPanel)
		editorPanel.layout = new VerticalLayout

		Operations.addText(editorPanel, "Detective, tenemos un caso para usted!")

		Operations.addText(editorPanel, modelObject.reporte)
		Operations.addText(editorPanel, modelObject.objetoRobado) => [
			fontSize = 20
			foreground = Color.BLUE
		]

		Operations.addBoton(editorPanel, "Aceptar el caso", [ |
			this.close
			new ResolverMisterioWindow(this, modelObject).open
		])

	}

}
