package view

import applicationModel.JuegoApplicationModel
import org.uqbar.arena.widgets.Panel
import org.uqbar.arena.windows.WindowOwner

class CasoResueltoWindow extends DialogoWindow<JuegoApplicationModel> {

	new(WindowOwner owner, JuegoApplicationModel model) {
		super(owner, model)
	}

	override aceptarMetodo() {
	}

	override giveTitle() {
		title = modelObject.caso.nombreDelCaso.concat(" - Resuelto")
	}

	override addName(Panel panel) {
		throw new UnsupportedOperationException("TODO: auto-generated method stub")
	}

	override protected createFormPanel(Panel arg0) {
		throw new UnsupportedOperationException("TODO: auto-generated method stub")
	}

}
