package view

import Operations.Operations
import applicationModel.ExpedienteApplicationModel
import applicationModel.JuegoApplicationModel
import dominio.Lugar
import dominio.Pais
import org.uqbar.arena.bindings.NotNullObservable
import org.uqbar.arena.bindings.PropertyAdapter
import org.uqbar.arena.layout.ColumnLayout
import org.uqbar.arena.layout.HorizontalLayout
import org.uqbar.arena.layout.VerticalLayout
import org.uqbar.arena.widgets.Panel
import org.uqbar.arena.windows.Dialog
import org.uqbar.arena.windows.WindowOwner

class ResolverMisterioWindow extends Dialog<JuegoApplicationModel> {

	new(WindowOwner owner, JuegoApplicationModel model) {
		super(owner, model)
	}

	override protected createFormPanel(Panel mainPanel) {
		throw new UnsupportedOperationException("TODO: auto-generated method stub")
	}

	override createMainTemplate(Panel mainPanel) {
		title = "Resolviendo: " + modelObject.caso.objeto

		var editorPanel = new Panel(mainPanel)
		editorPanel.layout = new VerticalLayout

		var botonera = new Panel(editorPanel)
		botonera.layout = new ColumnLayout(2)

		// Label Izquierdo
		var labelIzq = new Panel(botonera)
		labelIzq.layout = new VerticalLayout

		var nombrePanel = new Panel(labelIzq)
		nombrePanel.layout = new HorizontalLayout

		Operations.addText(nombrePanel, "Estás en: ")
		Operations.addTextProperty(nombrePanel, "detective.paisActual.nombre")

		Operations.addBoton(labelIzq, "Orden de Arresto", [|new OrdenDeArrestoWindow(this, modelObject).open])

		var ordenPanel = new Panel(labelIzq)
		ordenPanel.layout = new HorizontalLayout

		Operations.addText(ordenPanel, "Orden ya emitida: ") => [
			bindEnabled(new NotNullObservable("ordenDeArresto.nombre"))
		]
		Operations.addTextProperty(ordenPanel, "ordenDeArresto.nombre") => [
			bindEnabled(new NotNullObservable("detective.casoAResolver.ordenDeArresto"))
		]

		Operations.addBoton(labelIzq, "Viajar", [|new ViajarWindow(this, modelObject).open])

		Operations.addBoton(labelIzq, "Expedientes", [|
			new ExpedienteWindowVista(this, new ExpedienteApplicationModel).open
		])

		// Label Derecho
		var labelDer = new Panel(botonera)
		labelDer.layout = new VerticalLayout

		Operations.addText(labelDer, "Lugares")

		
//		Operations.addBoton(labelDer, modelObject.getNombreLugar(0), [|
//			modelObject.metodoPista()])
		Operations.addBoton(labelDer, modelObject.getNombreLugar(0), [|
			crearVentanaLugar(modelObject.getLugar(0), modelObject.caso.nombreDelCaso).open
		])
		Operations.addBoton(labelDer, modelObject.getNombreLugar(1), [|
			crearVentanaLugar(modelObject.getLugar(1), modelObject.caso.nombreDelCaso).open
		])
		Operations.addBoton(labelDer, modelObject.getNombreLugar(2), [|
			crearVentanaLugar(modelObject.getLugar(2), modelObject.caso.nombreDelCaso).open
		])
		// Label Recorrido criminal
		var recorridoPanel = new Panel(editorPanel)
		recorridoPanel.layout = new VerticalLayout

		Operations.addList(recorridoPanel, "Recorrido criminal: ", "recorridoCriminal",
			new PropertyAdapter(Pais, "nombre"))

		// Label destinos fallados
		var destinosFallados = new Panel(editorPanel)
		destinosFallados.layout = new VerticalLayout

		Operations.addText(destinosFallados, "Destinos fallidos: ")
		Operations.addList(destinosFallados, "País", "destinosFallidos", new PropertyAdapter(Pais, "nombre"))

	}

	def crearVentanaLugar(Lugar primerLugar, String nombreDelCaso) {
		var visitarLugarDialog = new VisitarLugarWindow(this, primerLugar)
		visitarLugarDialog.title = "Resolviendo: ".concat(nombreDelCaso)
		return visitarLugarDialog
	}

}
