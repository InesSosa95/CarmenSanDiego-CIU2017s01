package data

import org.uqbar.arena.windows.Dialog
import org.uqbar.arena.windows.WindowOwner
import org.uqbar.arena.widgets.Panel
import org.uqbar.arena.layout.ColumnLayout
import org.uqbar.arena.layout.VerticalLayout

abstract class DataWindow<T> extends Dialog<T> {

	new(WindowOwner owner, T model) {
		super(owner, model)
	}

	override protected createFormPanel(Panel mainPanel) {}

	override createMainTemplate(Panel mainPanel) {

		this.giveTitle()

		val panelAEditar = new Panel(mainPanel)
		panelAEditar.setLayout(new ColumnLayout(2))

		// Panel Izquierdo
		var panelLeft = new Panel(panelAEditar)
		panelLeft.setLayout(new VerticalLayout)
		this.addList(panelLeft)
		this.addBotons(panelLeft)

		// Panel Derecho		
		var panelRight = new Panel(panelAEditar)
		panelRight.setLayout(new VerticalLayout)
		this.addCharacteristic(panelRight)

	}

	abstract def void giveTitle()

	abstract def void addList(Panel panel)

	abstract def void addBotons(Panel panel)

	abstract def void addCharacteristic(Panel panel)

}
