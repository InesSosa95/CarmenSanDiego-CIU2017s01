package Minis;

import Minis.PaisMinificado;
import dominio.Mapamundi;
import dominio.Pais;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.eclipse.jetty.server.Request;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.ListExtensions;
import org.uqbar.xtrest.api.annotation.Controller;
import org.uqbar.xtrest.result.ResultFactory;

@Controller
@SuppressWarnings("all")
public class MapamundiMinificado extends ResultFactory {
  public static List<PaisMinificado> devolverListaDeVillanosMinificado() {
    final Function1<Pais, PaisMinificado> _function = (Pais it) -> {
      return new PaisMinificado(it);
    };
    return ListExtensions.<Pais, PaisMinificado>map(Mapamundi.obtenerInstancia().getPaises(), _function);
  }
  
  public static List<PaisMinificado> devolverListaDePaisesMinificado() {
    final Function1<Pais, PaisMinificado> _function = (Pais it) -> {
      return new PaisMinificado(it);
    };
    return ListExtensions.<Pais, PaisMinificado>map(Mapamundi.obtenerInstancia().getPaises(), _function);
  }
  
  public static PaisMinificado searchPais(final Integer integer) {
    final Function1<PaisMinificado, Boolean> _function = (PaisMinificado it) -> {
      int _id = it.getId();
      return Boolean.valueOf((_id == (integer).intValue()));
    };
    return IterableExtensions.<PaisMinificado>findFirst(MapamundiMinificado.devolverListaDePaisesMinificado(), _function);
  }
  
  public static boolean borrarPais(final String idI) {
    boolean _xblockexpression = false;
    {
      final Function1<Pais, Boolean> _function = (Pais it) -> {
        int _id = it.getId();
        Integer _valueOf = Integer.valueOf(idI);
        return Boolean.valueOf((_id == (_valueOf).intValue()));
      };
      Pais paisAEliminar = IterableExtensions.<Pais>findFirst(Mapamundi.obtenerInstancia().getPaises(), _function);
      _xblockexpression = Mapamundi.obtenerInstancia().eliminarPais(paisAEliminar);
    }
    return _xblockexpression;
  }
  
  public void handle(final String target, final Request baseRequest, final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException {
    this.pageNotFound(baseRequest, request, response);
  }
  
  public void pageNotFound(final Request baseRequest, final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException {
    response.getWriter().write(
    	"<html><head><title>XtRest - Page Not Found!</title></head>" 
    	+ "<body>"
    	+ "	<h1>Page Not Found !</h1>"
    	+ "	Supported resources:"
    	+ "	<table>"
    	+ "		<thead><tr><th>Verb</th><th>URL</th><th>Parameters</th></tr></thead>"
    	+ "		<tbody>"
    	+ "		</tbody>"
    	+ "	</table>"
    	+ "</body>"
    );
    response.setStatus(404);
    baseRequest.setHandled(true);
  }
}
