package Minis;

import Minis.VillanoMinimi;
import dominio.Pais;
import dominio.Sexo;
import dominio.Villano;
import java.util.ArrayList;
import java.util.List;
import org.eclipse.xtend.lib.annotations.Accessors;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Pure;
import org.uqbar.commons.utils.Observable;

@Accessors
@Observable
@SuppressWarnings("all")
public class SuperVillanoMinimi extends VillanoMinimi {
  private String sexo;
  
  private Pais paisActual;
  
  private List<String> senhas;
  
  private List<String> hobbies;
  
  public SuperVillanoMinimi(final Villano villano) {
    super(villano);
    this.sexo = villano.getSexo().toString();
    this.senhas = villano.getSenhas();
    this.hobbies = villano.getHobbies();
  }
  
  public Villano convertir() {
    Villano villano = null;
    villano.setNombre(this.getNombre());
    villano.setIdVillano(this.getId());
    villano.setSenhas(this.senhas);
    villano.setHobbies(this.hobbies);
    return villano;
  }
  
  public Villano convertirSinId() {
    Villano villano = null;
    Sexo sexo = null;
    String _nombre = this.getNombre();
    ArrayList<Pais> _newArrayList = CollectionLiterals.<Pais>newArrayList();
    Villano _villano = new Villano(_nombre, _newArrayList, sexo, this.senhas, this.hobbies);
    villano = _villano;
    return villano;
  }
  
  @Pure
  public String getSexo() {
    return this.sexo;
  }
  
  public void setSexo(final String sexo) {
    this.sexo = sexo;
  }
  
  @Pure
  public Pais getPaisActual() {
    return this.paisActual;
  }
  
  public void setPaisActual(final Pais paisActual) {
    this.paisActual = paisActual;
  }
  
  @Pure
  public List<String> getSenhas() {
    return this.senhas;
  }
  
  public void setSenhas(final List<String> senhas) {
    this.senhas = senhas;
  }
  
  @Pure
  public List<String> getHobbies() {
    return this.hobbies;
  }
  
  public void setHobbies(final List<String> hobbies) {
    this.hobbies = hobbies;
  }
}
