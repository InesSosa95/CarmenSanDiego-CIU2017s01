package Minis;

import Minis.SuperVillanoMinimi;
import Minis.VillanoMinimi;
import com.google.common.base.Objects;
import dominio.Expediente;
import dominio.Villano;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.eclipse.jetty.server.Request;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.ListExtensions;
import org.uqbar.xtrest.api.annotation.Controller;
import org.uqbar.xtrest.result.ResultFactory;

@Controller
@SuppressWarnings("all")
public class ExpedienteMinificado extends ResultFactory {
  public static List<VillanoMinimi> devolverListaDeVillanosMinificado() {
    final Function1<Villano, VillanoMinimi> _function = (Villano it) -> {
      return new VillanoMinimi(it);
    };
    return ListExtensions.<Villano, VillanoMinimi>map(Expediente.obtenerInstancia().getVillanos(), _function);
  }
  
  public static List<SuperVillanoMinimi> devolverListaDeVillanosMinificadoFull() {
    final Function1<Villano, SuperVillanoMinimi> _function = (Villano it) -> {
      return new SuperVillanoMinimi(it);
    };
    return ListExtensions.<Villano, SuperVillanoMinimi>map(Expediente.obtenerInstancia().getVillanos(), _function);
  }
  
  public static SuperVillanoMinimi searchVillano(final int idVillano) {
    final Function1<SuperVillanoMinimi, Boolean> _function = (SuperVillanoMinimi it) -> {
      int _id = it.getId();
      return Boolean.valueOf((_id == idVillano));
    };
    return IterableExtensions.<SuperVillanoMinimi>findFirst(ExpedienteMinificado.devolverListaDeVillanosMinificadoFull(), _function);
  }
  
  public static boolean eliminarVillano(final String id) {
    boolean _xblockexpression = false;
    {
      final Function1<Villano, Boolean> _function = (Villano it) -> {
        int _idVillano = it.getIdVillano();
        return Boolean.valueOf(Objects.equal(Integer.valueOf(_idVillano), id));
      };
      Villano villanoAEliminar = IterableExtensions.<Villano>findFirst(Expediente.obtenerInstancia().getVillanos(), _function);
      _xblockexpression = Expediente.obtenerInstancia().eliminarVillano(villanoAEliminar);
    }
    return _xblockexpression;
  }
  
  public static SuperVillanoMinimi editarVillano(final VillanoMinimi unVillano) {
    return ExpedienteMinificado.searchVillano(unVillano.getId());
  }
  
  public static boolean agregarVillanoNuevo(final SuperVillanoMinimi unVillano) {
    return Expediente.obtenerInstancia().agregarVillano(unVillano.convertirSinId());
  }
  
  public void handle(final String target, final Request baseRequest, final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException {
    this.pageNotFound(baseRequest, request, response);
  }
  
  public void pageNotFound(final Request baseRequest, final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException {
    response.getWriter().write(
    	"<html><head><title>XtRest - Page Not Found!</title></head>" 
    	+ "<body>"
    	+ "	<h1>Page Not Found !</h1>"
    	+ "	Supported resources:"
    	+ "	<table>"
    	+ "		<thead><tr><th>Verb</th><th>URL</th><th>Parameters</th></tr></thead>"
    	+ "		<tbody>"
    	+ "		</tbody>"
    	+ "	</table>"
    	+ "</body>"
    );
    response.setStatus(404);
    baseRequest.setHandled(true);
  }
}
