package ApiRest;

import Minis.ExpedienteMinificado;
import Minis.LugarMimi;
import Minis.MapamundiMinificado;
import Minis.SuperVillanoMinimi;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.eclipse.jetty.server.Request;
import org.eclipse.xtext.xbase.lib.Extension;
import org.uqbar.xtrest.api.Result;
import org.uqbar.xtrest.api.annotation.Body;
import org.uqbar.xtrest.api.annotation.Controller;
import org.uqbar.xtrest.api.annotation.Delete;
import org.uqbar.xtrest.api.annotation.Get;
import org.uqbar.xtrest.api.annotation.Post;
import org.uqbar.xtrest.api.annotation.Put;
import org.uqbar.xtrest.http.ContentType;
import org.uqbar.xtrest.json.JSONUtils;
import org.uqbar.xtrest.result.ResultFactory;

@Controller
@SuppressWarnings("all")
public class CarmenSanDiegoRestApi extends ResultFactory {
  @Extension
  private JSONUtils _jSONUtils = new JSONUtils();
  
  public CarmenSanDiegoRestApi() {
  }
  
  /**
   * VILLANO
   */
  @Get("/villanos")
  public Result getVillano(final String string, final String target, final Request baseRequest, final HttpServletRequest request, final HttpServletResponse response) {
    Result _xblockexpression = null;
    {
      response.setContentType(ContentType.APPLICATION_JSON);
      _xblockexpression = ResultFactory.ok(this._jSONUtils.toJson(ExpedienteMinificado.devolverListaDeVillanosMinificado()));
    }
    return _xblockexpression;
  }
  
  @Get("/villano/:id")
  public Result getVillan(final String id, final String target, final Request baseRequest, final HttpServletRequest request, final HttpServletResponse response) {
    Result _xblockexpression = null;
    {
      response.setContentType(ContentType.APPLICATION_JSON);
      _xblockexpression = ResultFactory.ok(this._jSONUtils.toJson(ExpedienteMinificado.searchVillano((Integer.valueOf(id)).intValue())));
    }
    return _xblockexpression;
  }
  
  @Delete("/villano/:id")
  public Result deleteVillano(final String id, final String target, final Request baseRequest, final HttpServletRequest request, final HttpServletResponse response) {
    Result _xblockexpression = null;
    {
      response.setContentType(ContentType.APPLICATION_JSON);
      _xblockexpression = ResultFactory.ok(this._jSONUtils.toJson(Boolean.valueOf(ExpedienteMinificado.eliminarVillano(id))));
    }
    return _xblockexpression;
  }
  
  @Post("/villano")
  public Result crearVillano(@Body final String substring, final String target, final Request baseRequest, final HttpServletRequest request, final HttpServletResponse response) {
    Result _xblockexpression = null;
    {
      response.setContentType(ContentType.APPLICATION_JSON);
      SuperVillanoMinimi villano = this._jSONUtils.<SuperVillanoMinimi>fromJson(substring, SuperVillanoMinimi.class);
      ExpedienteMinificado.agregarVillanoNuevo(villano);
      _xblockexpression = ResultFactory.ok("abc");
    }
    return _xblockexpression;
  }
  
  @Put("/villano/:villanoId")
  public Result editarVillano(final String villanoId, final String target, final Request baseRequest, final HttpServletRequest request, final HttpServletResponse response) {
    Result _xblockexpression = null;
    {
      response.setContentType(ContentType.APPLICATION_JSON);
      _xblockexpression = ResultFactory.ok(this._jSONUtils.toJson(ExpedienteMinificado.editarVillano(this._jSONUtils.<SuperVillanoMinimi>fromJson(villanoId, SuperVillanoMinimi.class))));
    }
    return _xblockexpression;
  }
  
  /**
   * PAIS
   */
  @Get("/paises")
  public Result getPaises(final String string, final String target, final Request baseRequest, final HttpServletRequest request, final HttpServletResponse response) {
    Result _xblockexpression = null;
    {
      response.setContentType(ContentType.APPLICATION_JSON);
      _xblockexpression = ResultFactory.ok(this._jSONUtils.toJson(MapamundiMinificado.devolverListaDePaisesMinificado()));
    }
    return _xblockexpression;
  }
  
  @Get("/pais/:id")
  public Result getPais(final String id, final String target, final Request baseRequest, final HttpServletRequest request, final HttpServletResponse response) {
    Result _xblockexpression = null;
    {
      response.setContentType(ContentType.APPLICATION_JSON);
      _xblockexpression = ResultFactory.ok(this._jSONUtils.toJson(MapamundiMinificado.searchPais(Integer.valueOf(id))));
    }
    return _xblockexpression;
  }
  
  @Delete("/eliminarPais/:id")
  public Result deletePais(final String id, final String target, final Request baseRequest, final HttpServletRequest request, final HttpServletResponse response) {
    Result _xblockexpression = null;
    {
      response.setContentType(ContentType.APPLICATION_JSON);
      _xblockexpression = ResultFactory.ok(this._jSONUtils.toJson(Boolean.valueOf(MapamundiMinificado.borrarPais(id))));
    }
    return _xblockexpression;
  }
  
  @Post("/pais")
  public Result crearPais(final String target, final Request baseRequest, final HttpServletRequest request, final HttpServletResponse response) {
    return ResultFactory.ok("asd");
  }
  
  @Put("/pais/{paisId}")
  public Result editarPais(final String target, final Request baseRequest, final HttpServletRequest request, final HttpServletResponse response) {
    return ResultFactory.ok("asd");
  }
  
  /**
   * LUGAR
   */
  @Get("/pistaDelLugar")
  public Result getPistas(@Body final String body, final String target, final Request baseRequest, final HttpServletRequest request, final HttpServletResponse response) {
    Result _xblockexpression = null;
    {
      response.setContentType(ContentType.APPLICATION_JSON);
      LugarMimi lugar = this._jSONUtils.<LugarMimi>fromJson(body, LugarMimi.class);
      _xblockexpression = ResultFactory.ok();
    }
    return _xblockexpression;
  }
  
  public void handle(final String target, final Request baseRequest, final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException {
    {
    	Matcher matcher = 
    		Pattern.compile("/villanos").matcher(target);
    	if (request.getMethod().equalsIgnoreCase("Get") && matcher.matches()) {
    		// take parameters from request
    		String string = request.getParameter("string");
    		
    		// take variables from url
    		
    		
    	    Result result = getVillano(string, target, baseRequest, request, response);
    	    result.process(response);
    	    
    		response.addHeader("Access-Control-Allow-Origin", "*");
    	    baseRequest.setHandled(true);
    	    return;
    	}
    }
    {
    	Matcher matcher = 
    		Pattern.compile("/villano/(\\w+)").matcher(target);
    	if (request.getMethod().equalsIgnoreCase("Get") && matcher.matches()) {
    		// take parameters from request
    		
    		// take variables from url
    		String id = matcher.group(1);
    		
    		
    	    Result result = getVillan(id, target, baseRequest, request, response);
    	    result.process(response);
    	    
    		response.addHeader("Access-Control-Allow-Origin", "*");
    	    baseRequest.setHandled(true);
    	    return;
    	}
    }
    {
    	Matcher matcher = 
    		Pattern.compile("/paises").matcher(target);
    	if (request.getMethod().equalsIgnoreCase("Get") && matcher.matches()) {
    		// take parameters from request
    		String string = request.getParameter("string");
    		
    		// take variables from url
    		
    		
    	    Result result = getPaises(string, target, baseRequest, request, response);
    	    result.process(response);
    	    
    		response.addHeader("Access-Control-Allow-Origin", "*");
    	    baseRequest.setHandled(true);
    	    return;
    	}
    }
    {
    	Matcher matcher = 
    		Pattern.compile("/pais/(\\w+)").matcher(target);
    	if (request.getMethod().equalsIgnoreCase("Get") && matcher.matches()) {
    		// take parameters from request
    		
    		// take variables from url
    		String id = matcher.group(1);
    		
    		
    	    Result result = getPais(id, target, baseRequest, request, response);
    	    result.process(response);
    	    
    		response.addHeader("Access-Control-Allow-Origin", "*");
    	    baseRequest.setHandled(true);
    	    return;
    	}
    }
    {
    	Matcher matcher = 
    		Pattern.compile("/pistaDelLugar").matcher(target);
    	if (request.getMethod().equalsIgnoreCase("Get") && matcher.matches()) {
    		// take parameters from request
    		String body = readBodyAsString(request);
    		
    		// take variables from url
    		
    		
    	    Result result = getPistas(body, target, baseRequest, request, response);
    	    result.process(response);
    	    
    		response.addHeader("Access-Control-Allow-Origin", "*");
    	    baseRequest.setHandled(true);
    	    return;
    	}
    }
    {
    	Matcher matcher = 
    		Pattern.compile("/villano").matcher(target);
    	if (request.getMethod().equalsIgnoreCase("Post") && matcher.matches()) {
    		// take parameters from request
    		String substring = readBodyAsString(request);
    		
    		// take variables from url
    		
    		
    	    Result result = crearVillano(substring, target, baseRequest, request, response);
    	    result.process(response);
    	    
    		response.addHeader("Access-Control-Allow-Origin", "*");
    	    baseRequest.setHandled(true);
    	    return;
    	}
    }
    {
    	Matcher matcher = 
    		Pattern.compile("/pais").matcher(target);
    	if (request.getMethod().equalsIgnoreCase("Post") && matcher.matches()) {
    		// take parameters from request
    		
    		// take variables from url
    		
    		
    	    Result result = crearPais(target, baseRequest, request, response);
    	    result.process(response);
    	    
    		response.addHeader("Access-Control-Allow-Origin", "*");
    	    baseRequest.setHandled(true);
    	    return;
    	}
    }
    {
    	Matcher matcher = 
    		Pattern.compile("/villano/(\\w+)").matcher(target);
    	if (request.getMethod().equalsIgnoreCase("Delete") && matcher.matches()) {
    		// take parameters from request
    		
    		// take variables from url
    		String id = matcher.group(1);
    		
    		
    	    Result result = deleteVillano(id, target, baseRequest, request, response);
    	    result.process(response);
    	    
    		response.addHeader("Access-Control-Allow-Origin", "*");
    	    baseRequest.setHandled(true);
    	    return;
    	}
    }
    {
    	Matcher matcher = 
    		Pattern.compile("/eliminarPais/(\\w+)").matcher(target);
    	if (request.getMethod().equalsIgnoreCase("Delete") && matcher.matches()) {
    		// take parameters from request
    		
    		// take variables from url
    		String id = matcher.group(1);
    		
    		
    	    Result result = deletePais(id, target, baseRequest, request, response);
    	    result.process(response);
    	    
    		response.addHeader("Access-Control-Allow-Origin", "*");
    	    baseRequest.setHandled(true);
    	    return;
    	}
    }
    {
    	Matcher matcher = 
    		Pattern.compile("/villano/(\\w+)").matcher(target);
    	if (request.getMethod().equalsIgnoreCase("Put") && matcher.matches()) {
    		// take parameters from request
    		
    		// take variables from url
    		String villanoId = matcher.group(1);
    		
    		
    	    Result result = editarVillano(villanoId, target, baseRequest, request, response);
    	    result.process(response);
    	    
    		response.addHeader("Access-Control-Allow-Origin", "*");
    	    baseRequest.setHandled(true);
    	    return;
    	}
    }
    {
    	Matcher matcher = 
    		Pattern.compile("/pais/{paisId}").matcher(target);
    	if (request.getMethod().equalsIgnoreCase("Put") && matcher.matches()) {
    		// take parameters from request
    		
    		// take variables from url
    		
    		
    	    Result result = editarPais(target, baseRequest, request, response);
    	    result.process(response);
    	    
    		response.addHeader("Access-Control-Allow-Origin", "*");
    	    baseRequest.setHandled(true);
    	    return;
    	}
    }
    this.pageNotFound(baseRequest, request, response);
  }
  
  public void pageNotFound(final Request baseRequest, final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException {
    response.getWriter().write(
    	"<html><head><title>XtRest - Page Not Found!</title></head>" 
    	+ "<body>"
    	+ "	<h1>Page Not Found !</h1>"
    	+ "	Supported resources:"
    	+ "	<table>"
    	+ "		<thead><tr><th>Verb</th><th>URL</th><th>Parameters</th></tr></thead>"
    	+ "		<tbody>"
    	+ "			<tr>"
    	+ "				<td>GET</td>"
    	+ "				<td>/villanos</td>"
    	+ "				<td>string</td>"
    	+ "			</tr>"
    	+ "			<tr>"
    	+ "				<td>GET</td>"
    	+ "				<td>/villano/:id</td>"
    	+ "				<td>id</td>"
    	+ "			</tr>"
    	+ "			<tr>"
    	+ "				<td>GET</td>"
    	+ "				<td>/paises</td>"
    	+ "				<td>string</td>"
    	+ "			</tr>"
    	+ "			<tr>"
    	+ "				<td>GET</td>"
    	+ "				<td>/pais/:id</td>"
    	+ "				<td>id</td>"
    	+ "			</tr>"
    	+ "			<tr>"
    	+ "				<td>GET</td>"
    	+ "				<td>/pistaDelLugar</td>"
    	+ "				<td>body</td>"
    	+ "			</tr>"
    	+ "			<tr>"
    	+ "				<td>POST</td>"
    	+ "				<td>/villano</td>"
    	+ "				<td>substring</td>"
    	+ "			</tr>"
    	+ "			<tr>"
    	+ "				<td>POST</td>"
    	+ "				<td>/pais</td>"
    	+ "				<td></td>"
    	+ "			</tr>"
    	+ "			<tr>"
    	+ "				<td>DELETE</td>"
    	+ "				<td>/villano/:id</td>"
    	+ "				<td>id</td>"
    	+ "			</tr>"
    	+ "			<tr>"
    	+ "				<td>DELETE</td>"
    	+ "				<td>/eliminarPais/:id</td>"
    	+ "				<td>id</td>"
    	+ "			</tr>"
    	+ "			<tr>"
    	+ "				<td>PUT</td>"
    	+ "				<td>/villano/:villanoId</td>"
    	+ "				<td>villanoId</td>"
    	+ "			</tr>"
    	+ "			<tr>"
    	+ "				<td>PUT</td>"
    	+ "				<td>/pais/{paisId}</td>"
    	+ "				<td></td>"
    	+ "			</tr>"
    	+ "		</tbody>"
    	+ "	</table>"
    	+ "</body>"
    );
    response.setStatus(404);
    baseRequest.setHandled(true);
  }
}
