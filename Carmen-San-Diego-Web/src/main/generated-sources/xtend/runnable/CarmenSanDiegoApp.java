package runnable;

import ApiRest.CarmenSanDiegoRestApi;
import dummyData.DummyData;
import org.uqbar.xtrest.api.XTRest;

@SuppressWarnings("all")
public class CarmenSanDiegoApp {
  public static void main(final String[] args) {
    final DummyData dummyData = new DummyData();
    CarmenSanDiegoRestApi _carmenSanDiegoRestApi = new CarmenSanDiegoRestApi();
    XTRest.startInstance(_carmenSanDiegoRestApi, 9001);
  }
}
