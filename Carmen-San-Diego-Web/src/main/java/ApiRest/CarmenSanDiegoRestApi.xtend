package ApiRest

import Minis.ExpedienteMinificado
import Minis.LugarMimi
import Minis.MapamundiMinificado
import Minis.SuperVillanoMinimi
import org.uqbar.xtrest.api.annotation.Body
import org.uqbar.xtrest.api.annotation.Controller
import org.uqbar.xtrest.api.annotation.Delete
import org.uqbar.xtrest.api.annotation.Get
import org.uqbar.xtrest.api.annotation.Post
import org.uqbar.xtrest.api.annotation.Put
import org.uqbar.xtrest.http.ContentType
import org.uqbar.xtrest.json.JSONUtils

@Controller
class CarmenSanDiegoRestApi {

	extension JSONUtils = new JSONUtils

	new() {
	}

	/**************************
	 * 		   VILLANO        *
	 *************************/
	// devuelve el nombre de todos los villanos
//FUNCIONA	
	@Get("/villanos")
	def getVillano(String string) {
		response.contentType = ContentType.APPLICATION_JSON
		ok(ExpedienteMinificado.devolverListaDeVillanosMinificado.toJson)
	}
//FUNCIONA
	@Get("/villano/:id")
	def getVillan() {
		response.contentType = ContentType.APPLICATION_JSON
		ok(ExpedienteMinificado.searchVillano(Integer.valueOf(id)).toJson)
	}
//FUNCIONA
	@Delete("/villano/:id")
	def deleteVillano() {
		response.contentType = ContentType.APPLICATION_JSON
		ok(ExpedienteMinificado.eliminarVillano(id).toJson)
	}

	@Post("/villano")
	def crearVillano(@Body String substring) {
		response.contentType = ContentType.APPLICATION_JSON
		var SuperVillanoMinimi villano = substring.fromJson(SuperVillanoMinimi)
		ExpedienteMinificado.agregarVillanoNuevo(villano)
		ok("abc")
	}
	
	@Put("/villano/:villanoId")
	def editarVillano() {
		response.contentType = ContentType.APPLICATION_JSON
		ok(ExpedienteMinificado.editarVillano(villanoId.fromJson(SuperVillanoMinimi)).toJson)
	}

	/**************************
	 * 		    PAIS          *
	 *************************/
//FUNCIONA	
	@Get("/paises")
	def getPaises(String string) {
		response.contentType = ContentType.APPLICATION_JSON
		ok(MapamundiMinificado.devolverListaDePaisesMinificado.toJson)
	}
//FUNCIONA
	@Get("/pais/:id")
	def getPais() {
		response.contentType = ContentType.APPLICATION_JSON
		ok(MapamundiMinificado.searchPais(Integer.valueOf(id)).toJson)
	}
//FUNCIONA
	@Delete("/eliminarPais/:id")
	def deletePais() {
		response.contentType = ContentType.APPLICATION_JSON
		ok(MapamundiMinificado.borrarPais(id).toJson)
	}

	@Post("/pais")
	def crearPais() {
		
		ok("asd")
	}

	@Put("/pais/{paisId}")
	def editarPais() {
		ok("asd")
	}

	/**************************
	 * 		   LUGAR          *
	 *************************/
	@Get("/pistaDelLugar")
	def getPistas(@Body String body){//idCaso lugarVisitado 
		response.contentType = ContentType.APPLICATION_JSON
		var LugarMimi lugar = body.fromJson(LugarMimi)
		ok()
	}
	
}
