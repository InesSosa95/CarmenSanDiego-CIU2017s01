package runnable

import dummyData.DummyData
import org.uqbar.xtrest.api.XTRest
import ApiRest.CarmenSanDiegoRestApi

class CarmenSanDiegoApp {

def static void main(String[] args) {
		val dummyData =  new DummyData 
		
         XTRest.startInstance(new CarmenSanDiegoRestApi(), 9001)
    }
}

