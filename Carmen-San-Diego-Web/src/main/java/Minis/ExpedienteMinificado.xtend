package Minis

import dominio.Expediente
import dominio.Villano
import org.uqbar.xtrest.api.annotation.Controller

@Controller
class ExpedienteMinificado {

	def static devolverListaDeVillanosMinificado() {
		Expediente.obtenerInstancia.villanos.map[new VillanoMinimi(it)]
	}
	
	def static devolverListaDeVillanosMinificadoFull() {
		Expediente.obtenerInstancia.villanos.map[new SuperVillanoMinimi(it)]
	}
	
	def static searchVillano(int idVillano) {
		devolverListaDeVillanosMinificadoFull.findFirst[it.id == idVillano] 
	}
	
	def static eliminarVillano(String id){
		var Villano villanoAEliminar = Expediente.obtenerInstancia.villanos.findFirst[it.idVillano == id]
		Expediente.obtenerInstancia.eliminarVillano(villanoAEliminar)
	}
	
	def static editarVillano(VillanoMinimi unVillano) {
		ExpedienteMinificado.searchVillano(unVillano.id)
	}
	
	def static agregarVillanoNuevo(SuperVillanoMinimi unVillano){
		Expediente.obtenerInstancia.agregarVillano(unVillano.convertirSinId())
	}
	
}
