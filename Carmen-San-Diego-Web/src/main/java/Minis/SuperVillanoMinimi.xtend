package Minis

import dominio.Pais
import dominio.Villano
import java.util.List
import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.commons.utils.Observable
import dominio.Sexo
import org.uqbar.xtrest.api.annotation.Controller


@Accessors
@Observable

class SuperVillanoMinimi extends VillanoMinimi{

	String sexo
	Pais paisActual
	List<String> senhas
	List<String> hobbies

	
	new (Villano villano) {
		super(villano)
		sexo = villano.sexo.toString
		senhas = villano.senhas
		hobbies = villano.hobbies	
		//paisActual = villano.paisActual	
	}
	
	def convertir() {
		var Villano villano
		villano.nombre = this.nombre
		villano.idVillano = this.id
		//villano.sexo = this.sexo  //NECESITO QUE ME AYUDEN A CONVERTIR STRING A SEXO
		villano.senhas = this.senhas
		villano.hobbies = this.hobbies
		//villano.paisActual = this.paisActual
		return villano
	}

	def convertirSinId() {
		var Villano villano
		var Sexo sexo
		villano = new Villano(this.nombre, newArrayList(), sexo, this.senhas, this.hobbies) 
		return villano
	}
	
}
