package Minis

import dominio.Pais
import java.util.List
import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.commons.utils.Observable
import org.uqbar.xtrest.api.annotation.Controller

@Controller
@Accessors
@Observable
class PaisMinificado {
	String nombre
	int id	
	List<String> conexiones

	new(Pais pais){
		nombre = pais.nombre
		id = pais.id
		conexiones = pais.conexiones.map[it.nombre]	
	}
	
}