package Minis

import dominio.Mapamundi
import dominio.Pais
import org.uqbar.xtrest.api.annotation.Controller

@Controller
class MapamundiMinificado {

	def static devolverListaDeVillanosMinificado() {
		Mapamundi.obtenerInstancia.paises.map[new PaisMinificado(it)]
	}
	
	def static devolverListaDePaisesMinificado() {
		Mapamundi.obtenerInstancia.paises.map[new PaisMinificado(it)]
	}
	
	def static searchPais(Integer integer) {
		devolverListaDePaisesMinificado.findFirst[it.id == integer]
	}
	
	def static borrarPais(String idI) {
		var Pais paisAEliminar = Mapamundi.obtenerInstancia.paises.findFirst[it.id == Integer.valueOf(idI)]
		Mapamundi.obtenerInstancia.eliminarPais(paisAEliminar)
	}
}