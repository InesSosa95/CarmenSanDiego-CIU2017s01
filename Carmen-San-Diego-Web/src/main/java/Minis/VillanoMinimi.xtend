package Minis

import dominio.Expediente
import dominio.Villano
import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.commons.utils.Observable
import org.uqbar.xtrest.api.annotation.Controller

@Controller
@Accessors
@Observable
class VillanoMinimi{

	String nombre
	int id

	new (Villano villano) { 
		nombre = villano.nombre
		id = villano.idVillano
	}

	def static crearVillanoMini(){
		new VillanoMinimi(new Villano())
	}
	
}
