package dominio

import java.util.List
import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.commons.model.UserException
import org.uqbar.commons.utils.Observable

@Accessors
@Observable
class Caso {

	String nombreDelCaso
	Villano responsable
	List<Pais> recorridoCriminal
	String reporte
	String objeto
	Pais paisDelHecho
	List<Pais> destinosFallidos
	Villano ordenDeArresto

	private static Caso instancia = null

	private new() {
	}

	private static def crearInstancia() {
		if (instancia === null) {
			instancia = new Caso()
		}
	}

	public static def obtenerInstancia() {
		if (instancia === null)
			crearInstancia();
		return instancia
	}

	def setearInstancia(Villano unVillano, String unReporte, String unObjeto, Pais unPaisDelHecho, String nombreCaso) {
		responsable = unVillano

		reporte = unReporte
		objeto = unObjeto
		paisDelHecho = unPaisDelHecho
		nombreDelCaso = nombreCaso
		recorridoCriminal = newArrayList
		destinosFallidos = newArrayList
		recorridoCriminal.add(unPaisDelHecho)
	}

	def agregarPaisAPlanDeEscape(Pais pais) {
		if (responsable.planDeEscape.contains(pais))
			throw new UserException("Ya existe ese pais en la ruta de escape")
		else
			responsable.planDeEscape.add(pais)
	}

	def estaResuelto(Villano capturado) {
		return responsable == capturado
	}

	def agregarDestinoFallido(Pais unPais) {
		destinosFallidos.add(unPais)
	}

	def getPaisDelHecho() {
		return paisDelHecho
	}

	def emitirOrdenDeArresto(Villano unVillano) {
		ordenDeArresto = unVillano
	}

	def agregarVisita(Pais pais) {
		if (responsable.pasoPor(pais)) {
			recorridoCriminal.add(pais)
		} else {
			destinosFallidos.add(pais)
		}
	}

}
