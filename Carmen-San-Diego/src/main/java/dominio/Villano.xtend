package dominio

import java.util.List
import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.commons.utils.Observable

@Accessors
@Observable
class Villano extends Ocupante {

	String nombre
	Sexo sexo
	List<String> senhas = newArrayList
	List<String> hobbies = newArrayList
	public List<Pais> planDeEscape = newArrayList
	Pais paisActual

	new(String suNombre, List<Pais> unPlanDeEscape, Sexo suSexo, List<String> susSenhas, List<String> susPasatiempos) {
		nombre = suNombre
		sexo = suSexo
		senhas = susSenhas
		hobbies = susPasatiempos
		planDeEscape = unPlanDeEscape
		paisActual = unPlanDeEscape.head()
		Expediente.obtenerInstancia.agregarVillano(this)
		this.informe = newArrayList
		informe.add('Tengo un tenedor en la mano y mis espinas son dignas de un cactus')
	}

	new() {
	}

	def agregarSenha(String unaSenha) {
		senhas.add(unaSenha)
	}

	def quitarSenha(String unaSenha) {
		senhas.remove(unaSenha)
	}

	def agregarHobbie(String unHobbie) {
		hobbies.add(unHobbie)
	}

	def quitarHobbie(String unHobbie) {
		hobbies.remove(unHobbie)
	}

	def equals(Villano unVillano) {
		unVillano.getNombre == this.getNombre
	}
	
	def cambiarPlanDeEscape(List<Pais> paises){
		planDeEscape = paises
	}

	def visitar(Lugar unLugar) {
		unLugar.dejarSenhasDe(this)
	}

	def getSenhas(int i) {
		senhas.take(i)
	}

	def getPistasDePais(Pais unPais, int i) {
		this.proximoPais(planDeEscape, paisActual).caracteristicas.take(i)
	}

	def proximoPais(List<Pais> unaListaDePaises, Pais unPais) {
		var int idProximoPais = unaListaDePaises.indexOf(unPais) + 1
		return unaListaDePaises.get(idProximoPais)
	}

	def getPistasDeHobbies(int i) {
		hobbies.take(i)
	}

	def visitarLugarDePaisFinal(Lugar unLugar) {
		unLugar.dejarPistasFinales()
	}
	

	def iniciarEscape() {	
		recorroPaisesDelPlanDeEscape()
		recorroUltimoPaisDePlanDeEscape()
	}
	
	private def recorroUltimoPaisDePlanDeEscape(){
		planDeEscape.last.dejarPistasFinales(this)
		
	}
	
	private def recorroPaisesDelPlanDeEscape(){
		for(Pais pais : planDeEscape){
				pais.seteaTusLugares(this)
				}
	}

	def pasoPor(Pais pais) {
		return planDeEscape.contains(pais)
	}

}
