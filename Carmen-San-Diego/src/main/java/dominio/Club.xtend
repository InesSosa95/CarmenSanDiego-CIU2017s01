package dominio

import org.eclipse.xtend.lib.annotations.Accessors
import java.util.Random

@Accessors
class Club extends Lugar {

	new() {
		ocupante = new Cuidador
		nombreDelLugar = "Club"
	}

	override dejarSenhasDe(Villano villano) {
		ocupante = new Informante()
		ocupante.reiniciarInformes()
		ocupante.informe.addAll(villano.getSenhas(2))
		if (new Random().nextDouble > 0.30) {
			ocupante.informe.addAll(villano.getPistasDeHobbies(1))
		}
	}

}
