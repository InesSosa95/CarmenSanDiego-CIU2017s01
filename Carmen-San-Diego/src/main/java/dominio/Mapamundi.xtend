package dominio

import java.util.List
import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.commons.model.UserException
import org.uqbar.commons.utils.Observable
import org.uqbar.commons.model.ObservableUtils
import org.apache.commons.lang.StringUtils

@Accessors
@Observable
public class Mapamundi {
	List<Pais> paises = newArrayList
	private static Mapamundi instancia = null

	private new() {
	}

	private static def crearInstancia() {
		if (instancia === null) {
			instancia = new Mapamundi()
		}
	}

	public static def obtenerInstancia() {
		if (instancia === null)
			crearInstancia();
		return instancia
	}

	def agregarPais(Pais unPais) {
		if (paises.contains(unPais))
			throw new UserException("Ya existe ese Pais")
		else {
			paises.add(unPais)
			ObservableUtils.firePropertyChanged(this, "paises")
		}
	}

	def eliminarPais(Pais unPais) {
		eliminarPaisDeConexiones(unPais)
		paises.remove(unPais)
		ObservableUtils.firePropertyChanged(this, "paises")
	}

	def eliminarPaisDeConexiones(Pais pais) {
		paises.forEach(p|p.quitarConexion(pais))
	}

	def existePais(Pais unPais) {
		paises.contains(unPais)
	}

	def cantidadDePaisesAgregados() {
		paises.size()
	}

	def reiniciarMapamundi() {
		paises = newArrayList()
	}
	
	def searchPaises(String substring) {
		if (StringUtils.isBlank(substring)) {
			this.paises.map[it.nombre]
		} else {
			this.paises.filter[it.nombre.toLowerCase.contains(substring.toLowerCase)].toList
		}
	}
	
	def searchPais(String substring) {
		try {
			return paises.filter[it.nombre.toLowerCase.contains(substring.toLowerCase)].head()
		} catch (UserException ex) {
			throw new UserException("El pais que intenta buscar no existe o ya lo destruyo el imperialismo yanky")
		}
	}
	
	def borrarPais(String nombreDelPais) {
		//this.eliminarPais(this.getPais(nombreDelPais))//ojo que me esta rompiendo las bolas el getPais
	}
	
	def getPais(String nombreDelPais) {
		this.paises.filter[it.nombre == nombreDelPais]
	}

}
