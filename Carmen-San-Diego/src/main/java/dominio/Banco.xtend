package dominio

import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.commons.utils.Observable

@Accessors
@Observable
class Banco extends Lugar {
	
	new(){
		ocupante = new Cuidador()
		nombreDelLugar = "Banco"
	}
	
	override dejarSenhasDe(Villano villano) {
		ocupante = new Informante()
		ocupante.informe.addAll(villano.getSenhas(1))
		ocupante.informe.addAll(villano.getPistasDePais(villano.getPaisActual(), 1))
	}
	
}