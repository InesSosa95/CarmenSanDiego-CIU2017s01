package dominio

class Embajada extends Lugar {

	new() {
		ocupante = new Cuidador
		nombreDelLugar = "Embajada"
	}

	override dejarSenhasDe(Villano villano) {
		ocupante = new Informante()
		ocupante.informe.addAll(villano.getPistasDePais(villano.getPaisActual(), 2))
	}
}
