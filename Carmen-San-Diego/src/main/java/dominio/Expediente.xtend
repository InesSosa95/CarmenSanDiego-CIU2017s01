package dominio

import java.util.List
import org.apache.commons.lang.StringUtils
import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.commons.model.UserException
import org.uqbar.commons.utils.Observable

@Accessors
@Observable
class Expediente {
	List<Villano> villanos = newArrayList
	private static Expediente instancia = null

	private new() {
	}

	private static def crearInstancia() {
		if (instancia === null) {
			instancia = new Expediente()
		}
	}

	public static def obtenerInstancia() {
		if (instancia === null)
			crearInstancia();
		return instancia
	}

	def agregarVillano(Villano unVillano) {
		if (villanos.contains(unVillano))
			throw new UserException("Ya existe un villano con ese nombre")
		else
			villanos.add(unVillano)
	}

	def eliminarVillano(Villano unVillano) {
		villanos.remove(unVillano)
	}

	def existeVillano(Villano unVillano) {
		villanos.contains(unVillano)
	}

	def cantidadDeVillanosEnExpediente() {
		villanos.size()
	}

	def reiniciarExpediente() {
		villanos = newArrayList()
	}

	def eliminarVillano(String nombreDeVillano) {
		this.eliminarVillano(this.getVillano(nombreDeVillano))
	}

	def getVillano(String substring) {
		try {
			return villanos.filter[it.nombre.toLowerCase.contains(substring.toLowerCase)].head()
		} catch (UserException ex) {
			throw new UserException("El villano que intenta buscar no existe, ups")
		}
	}

	def searchVillanos(String substring) {
		if (StringUtils.isBlank(substring)) {
			this.villanos.map[it.nombre]
		} else {
			this.villanos.filter[it.nombre.toLowerCase.contains(substring.toLowerCase)].toList
		}
	}
		
}
