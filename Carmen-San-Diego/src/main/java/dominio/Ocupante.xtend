package dominio

import java.util.List
import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.commons.utils.Observable

@Accessors
@Observable
abstract class Ocupante {
	List<String> informe

	new() {
		informe = newArrayList
		informe.add('Por aquí no se ha visto ningún delincuente que no sea parte del gobierno')
	}

	def reiniciarInformes() {
		informe = newArrayList
	}

	def void addPista(String string) {
		informe.add(string)
	}

}
