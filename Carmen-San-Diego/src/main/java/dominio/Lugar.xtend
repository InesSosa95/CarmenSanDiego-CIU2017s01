package dominio

import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.commons.utils.Observable

@Accessors
@Observable
abstract class Lugar {

	Ocupante ocupante
	String nombreDelLugar

	def void dejarSenhasDe(Villano villano)

	def void dejarPistasFinales() {
		ocupante = new Informante()
		ocupante.reiniciarInformes
		ocupante.addPista("juro q es medicin... ah! no el villano está cerca")
	}
	
	def damePistas(){
		 ocupante.informe
	}

}
