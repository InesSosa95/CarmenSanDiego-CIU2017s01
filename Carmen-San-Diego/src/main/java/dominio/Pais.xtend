package dominio

import java.util.List
import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.commons.model.UserException
import org.uqbar.commons.utils.Observable

@Observable
@Accessors
class Pais {

	String nombre
	List<String> caracteristicas = newArrayList
	List<Pais> conexiones = newArrayList
	public List<Lugar> lugares = newArrayList
	Cuidador ocupante
	List<String> conexionesComoString

	new(String unNombre, List<String> unasCaracteristicas, List<Pais> unasConexiones, List<Lugar> unosLugares) {
		this()
		nombre = unNombre
		caracteristicas = unasCaracteristicas
		conexiones = unasConexiones
		lugares = unosLugares
		verificarCantidadDeLugares()
		verificarNombreValido()
		Mapamundi.obtenerInstancia.agregarPais(this)
	}

	new() {
		nombre = "nuevoPais"
		ocupante = new Cuidador()
	}	

	def verificarNombreValido() {
		if (nombre === null)
			throw new UserException("El pais debe tener un nombre valido")
	}

	def verificarCantidadDeLugares() {
		if (lugares.size != 3 || lugares === null)
			throw new UserException("La cantidad de lugares es incorrecto (cantidad correcta:3)")
	}

	def agregarCaracteristica(String caracteristica) {
		if (caracteristicas.contains(caracteristica))
			throw new UserException("Ya existe esa caracteristica")
		else
			caracteristicas.add(caracteristica)
	}
	
	override toString(){
		this.nombre
	}

	def quitarCaracteristica(String caracteristica) {
		caracteristicas.remove(caracteristica)
	}

	def agregarConexion(Pais unaConexion) {
		verificarQueElPaisEsteEnElMapamundi(unaConexion)
		evitarDuplicacionDeConexiones(unaConexion)
		conexiones.add(unaConexion)
	}

	def evitarDuplicacionDeConexiones(Pais unaConexion) {
		if (conexiones.contains(unaConexion))
			throw new UserException("Ya existe una conexión con ese país")
	}


	def verificarQueElPaisEsteEnElMapamundi(Pais unPais) {
		val Mapamundi mapamundi = Mapamundi.obtenerInstancia()
		if (!mapamundi.existePais(unPais)) {
			throw new UserException("El pais no esta guardado en el mapamundi")
		}
	}

	def quitarConexion(Pais unaConexion) {
		conexiones.remove(unaConexion)
	}

	def agregarLugar(Lugar unLugar) {
		verificarQueNoSeaNull(unLugar)
		verificarEspacioDisponible()
		evitarDuplicacionDeLugares(unLugar)
		lugares.add(unLugar)
	}
	
	private def verificarQueNoSeaNull(Object objeto){
		if (objeto===null){
			throw new UserException("El lugar no puede ser vacio!")
		}
	}

	private def evitarDuplicacionDeLugares(Lugar unLugar) {
		if (lugares.contains(unLugar))
			throw new UserException("Ya existe ese Lugar")
	}

	private def verificarEspacioDisponible() {
		if (lugares.size == 3)
			throw new UserException("No se puede agregar mas de 3 lugares")
	}

	def quitarLugar(Lugar unLugar) {
		lugares.remove(unLugar)
	}

	def estaConectadoCon(Pais otroPais) {
		conexiones.contains(otroPais)
	}

	def equals(Pais unPais) {
		unPais.getNombre == this.getNombre
	}
	
	def seteaTusLugares(Villano unVillano){
		for(Lugar u: lugares){
				u.dejarSenhasDe(unVillano)
		}
	}
	
	def dejarPistasFinales(Villano unVillano){
		for(Lugar l: lugares){
				l.dejarPistasFinales()
		}
		lugares.get(2).ocupante = unVillano
	}	
}
