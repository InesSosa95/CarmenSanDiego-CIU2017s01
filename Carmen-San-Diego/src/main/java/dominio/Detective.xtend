package dominio

import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.commons.utils.Observable

@Accessors
@Observable
class Detective {
	String nombreDetective
	Caso casoAResolver
	Pais paisActual
	Sexo sexo
	int edad

	private static Detective instancia = null

	private new() {
	}

	private static def crearInstancia() {
		if (instancia === null) {
			instancia = new Detective()
		}
	}

	public static def obtenerInstancia() {
		if (instancia === null)
			crearInstancia();
		return instancia
	}

	def pedirPistas(Lugar unLugar) {
		return unLugar.damePistas()
	}

	def void setearDetective(String nombreUsuario, int unaEdad, Sexo suSexo, Caso unCaso) {
		nombreDetective = nombreUsuario
		casoAResolver = Caso.obtenerInstancia
		edad = unaEdad
		sexo = suSexo
		paisActual = casoAResolver.getPaisDelHecho
	}

	def viajarA(Pais unPais) {
		paisActual = unPais
		casoAResolver.agregarVisita(paisActual)
	}

	def pedirOrdenDeArresto(Villano unVillano) {
		casoAResolver.emitirOrdenDeArresto(unVillano)
	}
}
