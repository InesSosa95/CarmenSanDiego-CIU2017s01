package dominio

import java.util.Random
import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.commons.utils.Observable

@Accessors
@Observable
class Biblioteca extends Lugar {

	new() {
		ocupante = new Cuidador
		nombreDelLugar = "Biblioteca"
	}

	override dejarSenhasDe(Villano villano) {
		ocupante = new Informante()
		if (new Random().nextDouble > 0.50) {
			ocupante.informe.addAll(villano.getPistasDeHobbies(1))
		}
		ocupante.informe.addAll(villano.getSenhas(1))
		ocupante.informe.addAll(villano.getPistasDePais(villano.getPaisActual(), 1))
	}
	
	
	
// var double valorRandom = new java.util.Random().nextDouble
// SE PUEDE HACER COMO ESTA O CAMBIAR LA LINEA DEL IF POR i>O.5 
}
