package applicationModel

import dominio.Mapamundi
import dominio.Pais
import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.commons.utils.Observable

@Observable
@Accessors
class MapamundiApplicationModel {

	Mapamundi mapamundi

	Pais paisSeleccionado
	

	new() {
		mapamundi = Mapamundi.obtenerInstancia
	}

	def nuevoPais() {
		var pais = new Pais
		pais
	}

	def eliminarPais() {
		mapamundi.eliminarPais(paisSeleccionado)
	}

	
}
