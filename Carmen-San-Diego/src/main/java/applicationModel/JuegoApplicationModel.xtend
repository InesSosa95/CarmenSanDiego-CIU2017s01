package applicationModel

import dominio.Caso
import dominio.Detective
import dominio.Expediente
import dominio.Lugar
import dominio.Pais
import dominio.Villano
import java.util.List
import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.commons.utils.Observable

@Observable
@Accessors
class JuegoApplicationModel {

	Detective detective

	List<Pais> destinosFallidos

	Pais paisSeleccionado //x

	Villano villanoSeleccionado //x

	List<Lugar> lugares

	Caso caso

	Villano ordenDeArresto

	List<Pais> planDeEscape

	Pais paisDelHecho
	List<Villano> todosVillanos //x

	List<Pais> todasConexiones //x

	List<Pais> recorridoCriminal

	new() {
		caso = Caso.obtenerInstancia
		detective = Detective.obtenerInstancia
		destinosFallidos = caso.destinosFallidos
		ordenDeArresto = caso.ordenDeArresto
		planDeEscape = caso.responsable.planDeEscape
		todosVillanos = Expediente.obtenerInstancia.villanos
		recorridoCriminal = caso.recorridoCriminal

	}

	def recorridoCriminal() {
		recorridoCriminal = caso.recorridoCriminal
	}

	def paisDelHecho() {
		paisDelHecho = caso.paisDelHecho
	}

	def planDeEscape() {
		planDeEscape = caso.responsable.planDeEscape
	}

	def todasLasConexiones() {
		detective.paisActual.conexiones
	}

	def viajar() {
		detective.viajarA(paisSeleccionado)
	}

	def paisActualNombre() {
		detective.paisActual.nombre
	}

	def ordenDeArrestoNombre() {
		ordenDeArresto.nombre
	}

	def nombreCaso() {

		caso.nombreDelCaso
	}

	def todosLosVillanos() {
		todosVillanos
	}

	def reporte() {

		caso.reporte
	}

	def objetoRobado() {

		caso.objeto

	}

	def getNombreLugar(int n) {

		detective.paisActual.lugares.get(n).nombreDelLugar
	}

	def getLugar(int n) {

		detective.paisActual.lugares.get(n)
	}

	def pedirOrdenDeArresto() {
		ordenDeArresto = villanoSeleccionado
	}

}
