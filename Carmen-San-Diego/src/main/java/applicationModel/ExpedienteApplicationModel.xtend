package applicationModel

import dominio.Expediente
import dominio.Villano
import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.commons.utils.Observable

@Accessors
@Observable
class ExpedienteApplicationModel {
	
	Villano villanoSeleccionado
	Expediente expediente

	new() {
		expediente = Expediente.obtenerInstancia
	}
	
//	def metodoPista(int n){
//		[|
//			crearVentanaLugar(modelObject.getLugar(), modelObject.caso.nombreDelCaso).open
//		]
//	}

}
