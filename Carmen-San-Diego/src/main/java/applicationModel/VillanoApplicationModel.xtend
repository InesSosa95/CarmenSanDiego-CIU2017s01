package applicationModel

import dominio.Sexo
import dominio.Villano
import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.commons.utils.Observable

@Accessors
@Observable
class VillanoApplicationModel {
	
	Villano villanoSeleccionado
	String nombre
	Sexo sexo
	String nuevaCaracteristica
	String caracteristicaSeleccionada
	
	new(Villano villano){
		villanoSeleccionado=villano
	}
	
	new(){
		villanoSeleccionado = new Villano
	}
	
	def cambiarNombreVillano(){
		villanoSeleccionado.nombre=nombre
	}
	
	def setearSexo(){
		villanoSeleccionado.sexo=sexo
	} 
	
	def agregarSenha(){
		villanoSeleccionado.agregarSenha(nuevaCaracteristica)
	}
	
	def eliminarSenha(){
		villanoSeleccionado.quitarSenha(caracteristicaSeleccionada)
	}
	
	def agregarHobbies(){
		villanoSeleccionado.agregarHobbie(nuevaCaracteristica)
	}
	
	def eliminarHobbies(){
		villanoSeleccionado.quitarHobbie(caracteristicaSeleccionada)
	}
}