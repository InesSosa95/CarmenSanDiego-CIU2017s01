package applicationModel

import dominio.Banco
import dominio.Biblioteca
import dominio.Club
import dominio.Embajada
import dominio.Lugar
import dominio.Mapamundi
import dominio.Pais
import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.commons.model.ObservableUtils
import org.uqbar.commons.utils.Observable

@Observable
@Accessors
class PaisApplicationModel {

	Pais pais

	String nombre
	String caracteristicaSeleccionada
	String nuevaCaracteristica

	Pais conexionSeleccionada
	Pais nuevaConexion

	Lugar lugarSeleccionada
	Lugar nuevoLugar

	new(Pais paisSeleccionado) {
		pais = paisSeleccionado

	}

	def cambiarNombrePais() {
		pais.nombre = nombre
	}

	def agregarCaracteristica() {
		pais.agregarCaracteristica(nuevaCaracteristica)
		nuevaCaracteristica = null
		ObservableUtils.firePropertyChanged(this, "nuevaCaracteristica", nuevaCaracteristica)
	}

	def eliminarCaracteristica() {
		pais.quitarCaracteristica(caracteristicaSeleccionada)
	}

	def agregarConexion() {
		pais.agregarConexion(conexionSeleccionada)
		ObservableUtils.firePropertyChanged(this, "paisesPosibles", paisesPosibles)
		ObservableUtils.firePropertyChanged(this, "nuevaConexion", nuevaConexion)
	}

	def eliminarConexion() {
		pais.quitarConexion(conexionSeleccionada)
	}

	def agregarLugar() {
		pais.agregarLugar(nuevoLugar)
		ObservableUtils.firePropertyChanged(this, "lugaresPosibles", lugaresPosibles)
		ObservableUtils.firePropertyChanged(this, "nuevoLugar", nuevoLugar)

	}

	def eliminarLugar() {
		pais.quitarLugar(lugarSeleccionada)
		ObservableUtils.firePropertyChanged(this, "lugaresPosibles", lugaresPosibles)
	}

	def getPaisesPosibles() {
		Mapamundi.obtenerInstancia.paises.filter [
			!it.equals(pais)

		].filter[!pais.conexiones.contains(it)].toList
	}

	def getLugaresPosibles() {
		var lista = #[new Biblioteca, new Banco, new Club, new Embajada]
		lista.filter [
			var aux = pais.lugares.map[nombreDelLugar]
			!aux.contains(it.nombreDelLugar)
		].toList
	}

	/*def creaPais(){
	 * 	new Pais(unNombre, 
	 * 			 unasCaracteristicas, 
	 * 			 unasConexiones, 
	 * 			 unosLugares
	 * 	)
	 }*/
	def verificarLugares() {
		pais.verificarCantidadDeLugares
	}

	def verificarQueNoExistaPaisConIgualNombre() {
		Mapamundi.obtenerInstancia.existePais(pais)
	}

}
