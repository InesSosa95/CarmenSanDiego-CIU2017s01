package dummyData

import dominio.Banco
import dominio.Biblioteca
import dominio.Caso
import dominio.Club
import dominio.Detective
import dominio.Embajada
import dominio.Mapamundi
import dominio.Pais
import dominio.Sexo
import dominio.Villano
import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.commons.utils.Observable
import dominio.Lugar
import java.util.List

@Accessors
@Observable
class DummyData {

	var Villano villano
	var Villano villano2

	var Pais argentina
	var Pais rusia
	var Pais egipto
	var Pais albania
	var List<Lugar> lugares = newArrayList()

	var Caso caso

	Detective detective

	new() {
		lugares = newArrayList
		inicializarPaises
		caso = Caso.obtenerInstancia

		inicializarVillano1
		inicializarVillano2
		inicializarCaso
		inicializarDetective()
		inicializarLugares()
		inicializarPlanDeEscape()
//		inicializarJuego
	}

	def void inicializarPlanDeEscape() {
		var List<Pais> prueba
		prueba= newArrayList()
		prueba.add(argentina)
		prueba.add(rusia)
		prueba.add(egipto)
		caso.responsable.cambiarPlanDeEscape(prueba)
	}

	def inicializarPaises() {
		inicializarArgentina
		inicializarAlbania
		inicializarEgipto
		inicializarRusia
		argentina.conexiones = conexionesArgentinas
		albania.conexiones = conexionesDeAlbania
		rusia.conexiones = conexionesRusas
		egipto.conexiones = conexionesEgipcias
	}

	def inicializarLugaresAlbanos() {
		var ret = newArrayList()
		ret.add(new Banco())
		ret.add(new Embajada())
		ret.add(new Biblioteca())
		return ret
	}

	def inicializarEgipto() {
		val caracteristicas = newArrayList
		caracteristicas => [
			add("Su capital es el cairo")
			add("Su bandera es roja, blanca y negra y tiene un aguila en el centro")
			add("Su moneda es la libra")
		]

		egipto = new Pais("Egipto", caracteristicas, newArrayList(), inicializarLugaresEgiptos)
	}

	def inicializarLugaresEgiptos() {
		var ret = newArrayList()
		ret.add(new Banco())
		ret.add(new Club())
		ret.add(new Biblioteca())
		return ret
	}

	def inicializarRusia() {
		val caracteristicas = newArrayList()
		caracteristicas => [
			add("Formaban parte de la union sovietica")
			add("Su moneda es la rupia")
			add("Su capital es Moscú")
		]

		rusia = new Pais("Rusia", caracteristicas, newArrayList(), inicializarLugaresRusos)
	}

	def inicializarLugaresRusos() {
		var ret = newArrayList()
		ret.add(new Embajada())
		ret.add(new Club())
		ret.add(new Biblioteca())
		return ret
	}

	def inicializarArgentina() {
		val caracteristicas = newArrayList
		caracteristicas => [
			add("Toman una bebida llamada Mate")
			add("Su moneda es el peso")
			add("Tuvo 5 presidentes en una semana")
		]

		argentina = new Pais("Argentina", caracteristicas, newArrayList(), inicializarLugaresArgentinos)
	}

	def inicializarLugaresArgentinos() {
		var ret = newArrayList()
		ret.add(new Banco())
		ret.add(new Embajada())
		ret.add(new Biblioteca())
		return ret
	}

	def inicializarAlbania() {
		val caracteristicas = newArrayList
		caracteristicas => [
			add("En su territorio solian habitar los turcos otomanos")
			add("Su moneda es el lek")
			add("Su economia esta centrada en la agricultura y la minería")
		]

		albania = new Pais("Albania", caracteristicas, newArrayList(), inicializarLugaresAlbanos)
	}

	def conexionesDeAlbania() {
		val conexiones = newArrayList
		conexiones.add(egipto)
		conexiones.add(argentina)
		return conexiones
	}

	def conexionesEgipcias() {
		val conexiones = newArrayList
		conexiones => [
			add(argentina)
			add(albania)
			add(rusia)
		]
		return conexiones
	}

	def conexionesArgentinas() {
		val conexiones = newArrayList()
		conexiones => [
			add(rusia)
			add(egipto)
		]
	}

	def conexionesRusas() {
		val conexiones = newArrayList
		conexiones => [
			add(argentina)
			add(egipto)
		]
	}

	def inicializarVillano1() {
		val senhas = newArrayList
		senhas => [
			add("Cabello castaño")
			add("Alta")
			add("Tatuaje en el hombro")
		]

		val pasaTiempos = newArrayList
		pasaTiempos => [
			add("Jugar al tenis")
			add("Pasear en yfate")
			add("Catar vinos")
		]

		val planDeEscape1 = newArrayList()		
		villano = new Villano("Carmen San Diego", planDeEscape1, Sexo.FEMENINO, senhas, pasaTiempos)
	}

	def inicializarVillano2() {
		val senhas = newArrayList
		senhas => [
			add("Cabello Rubio")
			add("Estatura media")
			add("Muchos tatuajes")
		]

		val pasaTiempos = newArrayList
		pasaTiempos => [
			add("Tomar Vino")
			add("Escuchar cumbia")
			add("Planchar autos")
		]

		val planDeEscape2 = newArrayList()

		villano2 = new Villano("El cristian de lugano", planDeEscape2, Sexo.MASCULINO, senhas, pasaTiempos)
	}

	def inicializarCaso() {
		val objetoo = "Silla del presidente"
		val reportee = "Tesoro nacional robado en Buenos Aires, ha desaparecido " + objetoo
		Caso.obtenerInstancia.setearInstancia(villano2, reportee, objetoo, argentina, "Robo del sillon de Rivadavia")
	}

	def inicializarJuego() {
		caso.responsable.iniciarEscape
	}

	def inicializarDetective() {
		detective = Detective.obtenerInstancia()
		detective.setearDetective("sherlock", 28, Sexo.MASCULINO, caso)
	}

	def inicializarLugares() {

		val banco = new Banco()
		val club = new Club()
		val biblioteca = new Biblioteca()

	}

}
