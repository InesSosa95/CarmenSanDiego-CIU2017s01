package dummyData;

import dominio.Banco;
import dominio.Biblioteca;
import dominio.Caso;
import dominio.Club;
import dominio.Detective;
import dominio.Embajada;
import dominio.Lugar;
import dominio.Pais;
import dominio.Sexo;
import dominio.Villano;
import java.util.ArrayList;
import java.util.List;
import org.eclipse.xtend.lib.annotations.Accessors;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.ObjectExtensions;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1;
import org.eclipse.xtext.xbase.lib.Pure;
import org.uqbar.commons.utils.Observable;

@Accessors
@Observable
@SuppressWarnings("all")
public class DummyData {
  private Villano villano;
  
  private Villano villano2;
  
  private Pais argentina;
  
  private Pais rusia;
  
  private Pais egipto;
  
  private Pais albania;
  
  private List<Lugar> lugares = CollectionLiterals.<Lugar>newArrayList();
  
  private Caso caso;
  
  private Detective detective;
  
  public DummyData() {
    this.lugares = CollectionLiterals.<Lugar>newArrayList();
    this.inicializarPaises();
    this.caso = Caso.obtenerInstancia();
    this.inicializarVillano1();
    this.inicializarVillano2();
    this.inicializarCaso();
    this.inicializarDetective();
    this.inicializarLugares();
    this.inicializarPlanDeEscape();
  }
  
  public void inicializarPlanDeEscape() {
    List<Pais> prueba = null;
    prueba = CollectionLiterals.<Pais>newArrayList();
    prueba.add(this.argentina);
    prueba.add(this.rusia);
    prueba.add(this.egipto);
    this.caso.getResponsable().cambiarPlanDeEscape(prueba);
  }
  
  public void inicializarPaises() {
    this.inicializarArgentina();
    this.inicializarAlbania();
    this.inicializarEgipto();
    this.inicializarRusia();
    this.argentina.setConexiones(this.conexionesArgentinas());
    this.albania.setConexiones(this.conexionesDeAlbania());
    this.rusia.setConexiones(this.conexionesRusas());
    this.egipto.setConexiones(this.conexionesEgipcias());
  }
  
  public ArrayList<Lugar> inicializarLugaresAlbanos() {
    ArrayList<Lugar> ret = CollectionLiterals.<Lugar>newArrayList();
    Banco _banco = new Banco();
    ret.add(_banco);
    Embajada _embajada = new Embajada();
    ret.add(_embajada);
    Biblioteca _biblioteca = new Biblioteca();
    ret.add(_biblioteca);
    return ret;
  }
  
  public Pais inicializarEgipto() {
    Pais _xblockexpression = null;
    {
      final ArrayList<String> caracteristicas = CollectionLiterals.<String>newArrayList();
      final Procedure1<ArrayList<String>> _function = new Procedure1<ArrayList<String>>() {
        public void apply(final ArrayList<String> it) {
          it.add("Su capital es el cairo");
          it.add("Su bandera es roja, blanca y negra y tiene un aguila en el centro");
          it.add("Su moneda es la libra");
        }
      };
      ObjectExtensions.<ArrayList<String>>operator_doubleArrow(caracteristicas, _function);
      ArrayList<Pais> _newArrayList = CollectionLiterals.<Pais>newArrayList();
      ArrayList<Lugar> _inicializarLugaresEgiptos = this.inicializarLugaresEgiptos();
      Pais _pais = new Pais("Egipto", caracteristicas, _newArrayList, _inicializarLugaresEgiptos);
      _xblockexpression = this.egipto = _pais;
    }
    return _xblockexpression;
  }
  
  public ArrayList<Lugar> inicializarLugaresEgiptos() {
    ArrayList<Lugar> ret = CollectionLiterals.<Lugar>newArrayList();
    Banco _banco = new Banco();
    ret.add(_banco);
    Club _club = new Club();
    ret.add(_club);
    Biblioteca _biblioteca = new Biblioteca();
    ret.add(_biblioteca);
    return ret;
  }
  
  public Pais inicializarRusia() {
    Pais _xblockexpression = null;
    {
      final ArrayList<String> caracteristicas = CollectionLiterals.<String>newArrayList();
      final Procedure1<ArrayList<String>> _function = new Procedure1<ArrayList<String>>() {
        public void apply(final ArrayList<String> it) {
          it.add("Formaban parte de la union sovietica");
          it.add("Su moneda es la rupia");
          it.add("Su capital es Moscú");
        }
      };
      ObjectExtensions.<ArrayList<String>>operator_doubleArrow(caracteristicas, _function);
      ArrayList<Pais> _newArrayList = CollectionLiterals.<Pais>newArrayList();
      ArrayList<Lugar> _inicializarLugaresRusos = this.inicializarLugaresRusos();
      Pais _pais = new Pais("Rusia", caracteristicas, _newArrayList, _inicializarLugaresRusos);
      _xblockexpression = this.rusia = _pais;
    }
    return _xblockexpression;
  }
  
  public ArrayList<Lugar> inicializarLugaresRusos() {
    ArrayList<Lugar> ret = CollectionLiterals.<Lugar>newArrayList();
    Embajada _embajada = new Embajada();
    ret.add(_embajada);
    Club _club = new Club();
    ret.add(_club);
    Biblioteca _biblioteca = new Biblioteca();
    ret.add(_biblioteca);
    return ret;
  }
  
  public Pais inicializarArgentina() {
    Pais _xblockexpression = null;
    {
      final ArrayList<String> caracteristicas = CollectionLiterals.<String>newArrayList();
      final Procedure1<ArrayList<String>> _function = new Procedure1<ArrayList<String>>() {
        public void apply(final ArrayList<String> it) {
          it.add("Toman una bebida llamada Mate");
          it.add("Su moneda es el peso");
          it.add("Tuvo 5 presidentes en una semana");
        }
      };
      ObjectExtensions.<ArrayList<String>>operator_doubleArrow(caracteristicas, _function);
      ArrayList<Pais> _newArrayList = CollectionLiterals.<Pais>newArrayList();
      ArrayList<Lugar> _inicializarLugaresArgentinos = this.inicializarLugaresArgentinos();
      Pais _pais = new Pais("Argentina", caracteristicas, _newArrayList, _inicializarLugaresArgentinos);
      _xblockexpression = this.argentina = _pais;
    }
    return _xblockexpression;
  }
  
  public ArrayList<Lugar> inicializarLugaresArgentinos() {
    ArrayList<Lugar> ret = CollectionLiterals.<Lugar>newArrayList();
    Banco _banco = new Banco();
    ret.add(_banco);
    Embajada _embajada = new Embajada();
    ret.add(_embajada);
    Biblioteca _biblioteca = new Biblioteca();
    ret.add(_biblioteca);
    return ret;
  }
  
  public Pais inicializarAlbania() {
    Pais _xblockexpression = null;
    {
      final ArrayList<String> caracteristicas = CollectionLiterals.<String>newArrayList();
      final Procedure1<ArrayList<String>> _function = new Procedure1<ArrayList<String>>() {
        public void apply(final ArrayList<String> it) {
          it.add("En su territorio solian habitar los turcos otomanos");
          it.add("Su moneda es el lek");
          it.add("Su economia esta centrada en la agricultura y la minería");
        }
      };
      ObjectExtensions.<ArrayList<String>>operator_doubleArrow(caracteristicas, _function);
      ArrayList<Pais> _newArrayList = CollectionLiterals.<Pais>newArrayList();
      ArrayList<Lugar> _inicializarLugaresAlbanos = this.inicializarLugaresAlbanos();
      Pais _pais = new Pais("Albania", caracteristicas, _newArrayList, _inicializarLugaresAlbanos);
      _xblockexpression = this.albania = _pais;
    }
    return _xblockexpression;
  }
  
  public ArrayList<Pais> conexionesDeAlbania() {
    final ArrayList<Pais> conexiones = CollectionLiterals.<Pais>newArrayList();
    conexiones.add(this.egipto);
    conexiones.add(this.argentina);
    return conexiones;
  }
  
  public ArrayList<Pais> conexionesEgipcias() {
    final ArrayList<Pais> conexiones = CollectionLiterals.<Pais>newArrayList();
    final Procedure1<ArrayList<Pais>> _function = new Procedure1<ArrayList<Pais>>() {
      public void apply(final ArrayList<Pais> it) {
        it.add(DummyData.this.argentina);
        it.add(DummyData.this.albania);
        it.add(DummyData.this.rusia);
      }
    };
    ObjectExtensions.<ArrayList<Pais>>operator_doubleArrow(conexiones, _function);
    return conexiones;
  }
  
  public ArrayList<Pais> conexionesArgentinas() {
    ArrayList<Pais> _xblockexpression = null;
    {
      final ArrayList<Pais> conexiones = CollectionLiterals.<Pais>newArrayList();
      final Procedure1<ArrayList<Pais>> _function = new Procedure1<ArrayList<Pais>>() {
        public void apply(final ArrayList<Pais> it) {
          it.add(DummyData.this.rusia);
          it.add(DummyData.this.egipto);
        }
      };
      _xblockexpression = ObjectExtensions.<ArrayList<Pais>>operator_doubleArrow(conexiones, _function);
    }
    return _xblockexpression;
  }
  
  public ArrayList<Pais> conexionesRusas() {
    ArrayList<Pais> _xblockexpression = null;
    {
      final ArrayList<Pais> conexiones = CollectionLiterals.<Pais>newArrayList();
      final Procedure1<ArrayList<Pais>> _function = new Procedure1<ArrayList<Pais>>() {
        public void apply(final ArrayList<Pais> it) {
          it.add(DummyData.this.argentina);
          it.add(DummyData.this.egipto);
        }
      };
      _xblockexpression = ObjectExtensions.<ArrayList<Pais>>operator_doubleArrow(conexiones, _function);
    }
    return _xblockexpression;
  }
  
  public Villano inicializarVillano1() {
    Villano _xblockexpression = null;
    {
      final ArrayList<String> senhas = CollectionLiterals.<String>newArrayList();
      final Procedure1<ArrayList<String>> _function = new Procedure1<ArrayList<String>>() {
        public void apply(final ArrayList<String> it) {
          it.add("Cabello castaño");
          it.add("Alta");
          it.add("Tatuaje en el hombro");
        }
      };
      ObjectExtensions.<ArrayList<String>>operator_doubleArrow(senhas, _function);
      final ArrayList<String> pasaTiempos = CollectionLiterals.<String>newArrayList();
      final Procedure1<ArrayList<String>> _function_1 = new Procedure1<ArrayList<String>>() {
        public void apply(final ArrayList<String> it) {
          it.add("Jugar al tenis");
          it.add("Pasear en yfate");
          it.add("Catar vinos");
        }
      };
      ObjectExtensions.<ArrayList<String>>operator_doubleArrow(pasaTiempos, _function_1);
      final ArrayList<Pais> planDeEscape1 = CollectionLiterals.<Pais>newArrayList();
      Villano _villano = new Villano("Carmen San Diego", planDeEscape1, Sexo.FEMENINO, senhas, pasaTiempos);
      _xblockexpression = this.villano = _villano;
    }
    return _xblockexpression;
  }
  
  public Villano inicializarVillano2() {
    Villano _xblockexpression = null;
    {
      final ArrayList<String> senhas = CollectionLiterals.<String>newArrayList();
      final Procedure1<ArrayList<String>> _function = new Procedure1<ArrayList<String>>() {
        public void apply(final ArrayList<String> it) {
          it.add("Cabello Rubio");
          it.add("Estatura media");
          it.add("Muchos tatuajes");
        }
      };
      ObjectExtensions.<ArrayList<String>>operator_doubleArrow(senhas, _function);
      final ArrayList<String> pasaTiempos = CollectionLiterals.<String>newArrayList();
      final Procedure1<ArrayList<String>> _function_1 = new Procedure1<ArrayList<String>>() {
        public void apply(final ArrayList<String> it) {
          it.add("Tomar Vino");
          it.add("Escuchar cumbia");
          it.add("Planchar autos");
        }
      };
      ObjectExtensions.<ArrayList<String>>operator_doubleArrow(pasaTiempos, _function_1);
      final ArrayList<Pais> planDeEscape2 = CollectionLiterals.<Pais>newArrayList();
      Villano _villano = new Villano("El cristian de lugano", planDeEscape2, Sexo.MASCULINO, senhas, pasaTiempos);
      _xblockexpression = this.villano2 = _villano;
    }
    return _xblockexpression;
  }
  
  public boolean inicializarCaso() {
    boolean _xblockexpression = false;
    {
      final String objetoo = "Silla del presidente";
      final String reportee = ("Tesoro nacional robado en Buenos Aires, ha desaparecido " + objetoo);
      _xblockexpression = Caso.obtenerInstancia().setearInstancia(this.villano2, reportee, objetoo, this.argentina, "Robo del sillon de Rivadavia");
    }
    return _xblockexpression;
  }
  
  public void inicializarJuego() {
    this.caso.getResponsable().iniciarEscape();
  }
  
  public void inicializarDetective() {
    this.detective = Detective.obtenerInstancia();
    this.detective.setearDetective("sherlock", 28, Sexo.MASCULINO, this.caso);
  }
  
  public void inicializarLugares() {
    final Banco banco = new Banco();
    final Club club = new Club();
    final Biblioteca biblioteca = new Biblioteca();
  }
  
  @Pure
  public Villano getVillano() {
    return this.villano;
  }
  
  public void setVillano(final Villano villano) {
    this.villano = villano;
  }
  
  @Pure
  public Villano getVillano2() {
    return this.villano2;
  }
  
  public void setVillano2(final Villano villano2) {
    this.villano2 = villano2;
  }
  
  @Pure
  public Pais getArgentina() {
    return this.argentina;
  }
  
  public void setArgentina(final Pais argentina) {
    this.argentina = argentina;
  }
  
  @Pure
  public Pais getRusia() {
    return this.rusia;
  }
  
  public void setRusia(final Pais rusia) {
    this.rusia = rusia;
  }
  
  @Pure
  public Pais getEgipto() {
    return this.egipto;
  }
  
  public void setEgipto(final Pais egipto) {
    this.egipto = egipto;
  }
  
  @Pure
  public Pais getAlbania() {
    return this.albania;
  }
  
  public void setAlbania(final Pais albania) {
    this.albania = albania;
  }
  
  @Pure
  public List<Lugar> getLugares() {
    return this.lugares;
  }
  
  public void setLugares(final List<Lugar> lugares) {
    this.lugares = lugares;
  }
  
  @Pure
  public Caso getCaso() {
    return this.caso;
  }
  
  public void setCaso(final Caso caso) {
    this.caso = caso;
  }
  
  @Pure
  public Detective getDetective() {
    return this.detective;
  }
  
  public void setDetective(final Detective detective) {
    this.detective = detective;
  }
}
