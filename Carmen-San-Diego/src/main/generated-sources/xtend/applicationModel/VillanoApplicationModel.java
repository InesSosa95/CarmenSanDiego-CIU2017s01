package applicationModel;

import dominio.Sexo;
import dominio.Villano;
import org.eclipse.xtend.lib.annotations.Accessors;
import org.eclipse.xtext.xbase.lib.Pure;
import org.uqbar.commons.utils.Observable;

@Accessors
@Observable
@SuppressWarnings("all")
public class VillanoApplicationModel {
  private Villano villanoSeleccionado;
  
  private String nombre;
  
  private Sexo sexo;
  
  private String nuevaCaracteristica;
  
  private String caracteristicaSeleccionada;
  
  public VillanoApplicationModel(final Villano villano) {
    this.villanoSeleccionado = villano;
  }
  
  public VillanoApplicationModel() {
    Villano _villano = new Villano();
    this.villanoSeleccionado = _villano;
  }
  
  public void cambiarNombreVillano() {
    this.villanoSeleccionado.setNombre(this.nombre);
  }
  
  public void setearSexo() {
    this.villanoSeleccionado.setSexo(this.sexo);
  }
  
  public boolean agregarSenha() {
    return this.villanoSeleccionado.agregarSenha(this.nuevaCaracteristica);
  }
  
  public boolean eliminarSenha() {
    return this.villanoSeleccionado.quitarSenha(this.caracteristicaSeleccionada);
  }
  
  public boolean agregarHobbies() {
    return this.villanoSeleccionado.agregarHobbie(this.nuevaCaracteristica);
  }
  
  public boolean eliminarHobbies() {
    return this.villanoSeleccionado.quitarHobbie(this.caracteristicaSeleccionada);
  }
  
  @Pure
  public Villano getVillanoSeleccionado() {
    return this.villanoSeleccionado;
  }
  
  public void setVillanoSeleccionado(final Villano villanoSeleccionado) {
    this.villanoSeleccionado = villanoSeleccionado;
  }
  
  @Pure
  public String getNombre() {
    return this.nombre;
  }
  
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }
  
  @Pure
  public Sexo getSexo() {
    return this.sexo;
  }
  
  public void setSexo(final Sexo sexo) {
    this.sexo = sexo;
  }
  
  @Pure
  public String getNuevaCaracteristica() {
    return this.nuevaCaracteristica;
  }
  
  public void setNuevaCaracteristica(final String nuevaCaracteristica) {
    this.nuevaCaracteristica = nuevaCaracteristica;
  }
  
  @Pure
  public String getCaracteristicaSeleccionada() {
    return this.caracteristicaSeleccionada;
  }
  
  public void setCaracteristicaSeleccionada(final String caracteristicaSeleccionada) {
    this.caracteristicaSeleccionada = caracteristicaSeleccionada;
  }
}
