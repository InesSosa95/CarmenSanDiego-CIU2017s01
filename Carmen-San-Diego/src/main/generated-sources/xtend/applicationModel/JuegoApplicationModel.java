package applicationModel;

import dominio.Caso;
import dominio.Detective;
import dominio.Expediente;
import dominio.Lugar;
import dominio.Pais;
import dominio.Villano;
import java.util.List;
import org.eclipse.xtend.lib.annotations.Accessors;
import org.eclipse.xtext.xbase.lib.Pure;
import org.uqbar.commons.utils.Observable;

@Observable
@Accessors
@SuppressWarnings("all")
public class JuegoApplicationModel {
  private Detective detective;
  
  private List<Pais> destinosFallidos;
  
  private Pais paisSeleccionado;
  
  private Villano villanoSeleccionado;
  
  private List<Lugar> lugares;
  
  private Caso caso;
  
  private Villano ordenDeArresto;
  
  private List<Pais> planDeEscape;
  
  private Pais paisDelHecho;
  
  private List<Villano> todosVillanos;
  
  private List<Pais> todasConexiones;
  
  private List<Pais> recorridoCriminal;
  
  public JuegoApplicationModel() {
    this.caso = Caso.obtenerInstancia();
    this.detective = Detective.obtenerInstancia();
    this.destinosFallidos = this.caso.getDestinosFallidos();
    this.ordenDeArresto = this.caso.getOrdenDeArresto();
    this.planDeEscape = this.caso.getResponsable().planDeEscape;
    this.todosVillanos = Expediente.obtenerInstancia().getVillanos();
    this.recorridoCriminal = this.caso.getRecorridoCriminal();
  }
  
  public List<Pais> recorridoCriminal() {
    return this.recorridoCriminal = this.caso.getRecorridoCriminal();
  }
  
  public Pais paisDelHecho() {
    return this.paisDelHecho = this.caso.getPaisDelHecho();
  }
  
  public List<Pais> planDeEscape() {
    return this.planDeEscape = this.caso.getResponsable().planDeEscape;
  }
  
  public List<Pais> todasLasConexiones() {
    return this.detective.getPaisActual().getConexiones();
  }
  
  public boolean viajar() {
    return this.detective.viajarA(this.paisSeleccionado);
  }
  
  public String paisActualNombre() {
    return this.detective.getPaisActual().getNombre();
  }
  
  public String ordenDeArrestoNombre() {
    return this.ordenDeArresto.getNombre();
  }
  
  public String nombreCaso() {
    return this.caso.getNombreDelCaso();
  }
  
  public List<Villano> todosLosVillanos() {
    return this.todosVillanos;
  }
  
  public String reporte() {
    return this.caso.getReporte();
  }
  
  public String objetoRobado() {
    return this.caso.getObjeto();
  }
  
  public String getNombreLugar(final int n) {
    return this.detective.getPaisActual().lugares.get(n).getNombreDelLugar();
  }
  
  public Lugar getLugar(final int n) {
    return this.detective.getPaisActual().lugares.get(n);
  }
  
  public Villano pedirOrdenDeArresto() {
    return this.ordenDeArresto = this.villanoSeleccionado;
  }
  
  @Pure
  public Detective getDetective() {
    return this.detective;
  }
  
  public void setDetective(final Detective detective) {
    this.detective = detective;
  }
  
  @Pure
  public List<Pais> getDestinosFallidos() {
    return this.destinosFallidos;
  }
  
  public void setDestinosFallidos(final List<Pais> destinosFallidos) {
    this.destinosFallidos = destinosFallidos;
  }
  
  @Pure
  public Pais getPaisSeleccionado() {
    return this.paisSeleccionado;
  }
  
  public void setPaisSeleccionado(final Pais paisSeleccionado) {
    this.paisSeleccionado = paisSeleccionado;
  }
  
  @Pure
  public Villano getVillanoSeleccionado() {
    return this.villanoSeleccionado;
  }
  
  public void setVillanoSeleccionado(final Villano villanoSeleccionado) {
    this.villanoSeleccionado = villanoSeleccionado;
  }
  
  @Pure
  public List<Lugar> getLugares() {
    return this.lugares;
  }
  
  public void setLugares(final List<Lugar> lugares) {
    this.lugares = lugares;
  }
  
  @Pure
  public Caso getCaso() {
    return this.caso;
  }
  
  public void setCaso(final Caso caso) {
    this.caso = caso;
  }
  
  @Pure
  public Villano getOrdenDeArresto() {
    return this.ordenDeArresto;
  }
  
  public void setOrdenDeArresto(final Villano ordenDeArresto) {
    this.ordenDeArresto = ordenDeArresto;
  }
  
  @Pure
  public List<Pais> getPlanDeEscape() {
    return this.planDeEscape;
  }
  
  public void setPlanDeEscape(final List<Pais> planDeEscape) {
    this.planDeEscape = planDeEscape;
  }
  
  @Pure
  public Pais getPaisDelHecho() {
    return this.paisDelHecho;
  }
  
  public void setPaisDelHecho(final Pais paisDelHecho) {
    this.paisDelHecho = paisDelHecho;
  }
  
  @Pure
  public List<Villano> getTodosVillanos() {
    return this.todosVillanos;
  }
  
  public void setTodosVillanos(final List<Villano> todosVillanos) {
    this.todosVillanos = todosVillanos;
  }
  
  @Pure
  public List<Pais> getTodasConexiones() {
    return this.todasConexiones;
  }
  
  public void setTodasConexiones(final List<Pais> todasConexiones) {
    this.todasConexiones = todasConexiones;
  }
  
  @Pure
  public List<Pais> getRecorridoCriminal() {
    return this.recorridoCriminal;
  }
  
  public void setRecorridoCriminal(final List<Pais> recorridoCriminal) {
    this.recorridoCriminal = recorridoCriminal;
  }
}
