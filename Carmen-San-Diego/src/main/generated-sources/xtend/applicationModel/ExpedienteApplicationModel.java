package applicationModel;

import dominio.Expediente;
import dominio.Villano;
import org.eclipse.xtend.lib.annotations.Accessors;
import org.eclipse.xtext.xbase.lib.Pure;
import org.uqbar.commons.utils.Observable;

@Accessors
@Observable
@SuppressWarnings("all")
public class ExpedienteApplicationModel {
  private Villano villanoSeleccionado;
  
  private Expediente expediente;
  
  public ExpedienteApplicationModel() {
    this.expediente = Expediente.obtenerInstancia();
  }
  
  @Pure
  public Villano getVillanoSeleccionado() {
    return this.villanoSeleccionado;
  }
  
  public void setVillanoSeleccionado(final Villano villanoSeleccionado) {
    this.villanoSeleccionado = villanoSeleccionado;
  }
  
  @Pure
  public Expediente getExpediente() {
    return this.expediente;
  }
  
  public void setExpediente(final Expediente expediente) {
    this.expediente = expediente;
  }
}
