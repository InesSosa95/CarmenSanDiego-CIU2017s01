package applicationModel;

import dominio.Mapamundi;
import dominio.Pais;
import org.eclipse.xtend.lib.annotations.Accessors;
import org.eclipse.xtext.xbase.lib.Pure;
import org.uqbar.commons.utils.Observable;

@Observable
@Accessors
@SuppressWarnings("all")
public class MapamundiApplicationModel {
  private Mapamundi mapamundi;
  
  private Pais paisSeleccionado;
  
  public MapamundiApplicationModel() {
    this.mapamundi = Mapamundi.obtenerInstancia();
  }
  
  public Pais nuevoPais() {
    Pais _xblockexpression = null;
    {
      Pais pais = new Pais();
      _xblockexpression = pais;
    }
    return _xblockexpression;
  }
  
  public void eliminarPais() {
    this.mapamundi.eliminarPais(this.paisSeleccionado);
  }
  
  @Pure
  public Mapamundi getMapamundi() {
    return this.mapamundi;
  }
  
  public void setMapamundi(final Mapamundi mapamundi) {
    this.mapamundi = mapamundi;
  }
  
  @Pure
  public Pais getPaisSeleccionado() {
    return this.paisSeleccionado;
  }
  
  public void setPaisSeleccionado(final Pais paisSeleccionado) {
    this.paisSeleccionado = paisSeleccionado;
  }
}
