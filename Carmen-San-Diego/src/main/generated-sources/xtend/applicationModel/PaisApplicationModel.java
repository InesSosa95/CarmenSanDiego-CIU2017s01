package applicationModel;

import dominio.Banco;
import dominio.Biblioteca;
import dominio.Club;
import dominio.Embajada;
import dominio.Lugar;
import dominio.Mapamundi;
import dominio.Pais;
import java.util.Collections;
import java.util.List;
import org.eclipse.xtend.lib.annotations.Accessors;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.ListExtensions;
import org.eclipse.xtext.xbase.lib.Pure;
import org.uqbar.commons.model.ObservableUtils;
import org.uqbar.commons.utils.Observable;

@Observable
@Accessors
@SuppressWarnings("all")
public class PaisApplicationModel {
  private Pais pais;
  
  private String nombre;
  
  private String caracteristicaSeleccionada;
  
  private String nuevaCaracteristica;
  
  private Pais conexionSeleccionada;
  
  private Pais nuevaConexion;
  
  private Lugar lugarSeleccionada;
  
  private Lugar nuevoLugar;
  
  public PaisApplicationModel(final Pais paisSeleccionado) {
    this.pais = paisSeleccionado;
  }
  
  public void cambiarNombrePais() {
    this.pais.setNombre(this.nombre);
  }
  
  public void agregarCaracteristica() {
    this.pais.agregarCaracteristica(this.nuevaCaracteristica);
    this.nuevaCaracteristica = null;
    ObservableUtils.firePropertyChanged(this, "nuevaCaracteristica", this.nuevaCaracteristica);
  }
  
  public boolean eliminarCaracteristica() {
    return this.pais.quitarCaracteristica(this.caracteristicaSeleccionada);
  }
  
  public void agregarConexion() {
    this.pais.agregarConexion(this.conexionSeleccionada);
    ObservableUtils.firePropertyChanged(this, "paisesPosibles", this.getPaisesPosibles());
    ObservableUtils.firePropertyChanged(this, "nuevaConexion", this.nuevaConexion);
  }
  
  public boolean eliminarConexion() {
    return this.pais.quitarConexion(this.conexionSeleccionada);
  }
  
  public void agregarLugar() {
    this.pais.agregarLugar(this.nuevoLugar);
    ObservableUtils.firePropertyChanged(this, "lugaresPosibles", this.getLugaresPosibles());
    ObservableUtils.firePropertyChanged(this, "nuevoLugar", this.nuevoLugar);
  }
  
  public void eliminarLugar() {
    this.pais.quitarLugar(this.lugarSeleccionada);
    ObservableUtils.firePropertyChanged(this, "lugaresPosibles", this.getLugaresPosibles());
  }
  
  public List<Pais> getPaisesPosibles() {
    final Function1<Pais, Boolean> _function = new Function1<Pais, Boolean>() {
      public Boolean apply(final Pais it) {
        boolean _equals = it.equals(PaisApplicationModel.this.pais);
        return Boolean.valueOf((!_equals));
      }
    };
    final Function1<Pais, Boolean> _function_1 = new Function1<Pais, Boolean>() {
      public Boolean apply(final Pais it) {
        boolean _contains = PaisApplicationModel.this.pais.getConexiones().contains(it);
        return Boolean.valueOf((!_contains));
      }
    };
    return IterableExtensions.<Pais>toList(IterableExtensions.<Pais>filter(IterableExtensions.<Pais>filter(Mapamundi.obtenerInstancia().getPaises(), _function), _function_1));
  }
  
  public List<? extends Lugar> getLugaresPosibles() {
    List<? extends Lugar> _xblockexpression = null;
    {
      Biblioteca _biblioteca = new Biblioteca();
      Banco _banco = new Banco();
      Club _club = new Club();
      Embajada _embajada = new Embajada();
      List<? extends Lugar> lista = Collections.<Lugar>unmodifiableList(CollectionLiterals.<Lugar>newArrayList(_biblioteca, _banco, _club, _embajada));
      final Function1<Lugar, Boolean> _function = new Function1<Lugar, Boolean>() {
        public Boolean apply(final Lugar it) {
          boolean _xblockexpression = false;
          {
            final Function1<Lugar, String> _function = new Function1<Lugar, String>() {
              public String apply(final Lugar it) {
                return it.getNombreDelLugar();
              }
            };
            List<String> aux = ListExtensions.<Lugar, String>map(PaisApplicationModel.this.pais.lugares, _function);
            boolean _contains = aux.contains(it.getNombreDelLugar());
            _xblockexpression = (!_contains);
          }
          return Boolean.valueOf(_xblockexpression);
        }
      };
      _xblockexpression = IterableExtensions.toList(IterableExtensions.filter(lista, _function));
    }
    return _xblockexpression;
  }
  
  /**
   * def creaPais(){
   * 	new Pais(unNombre,
   * 			 unasCaracteristicas,
   * 			 unasConexiones,
   * 			 unosLugares
   * 	)
   * }
   */
  public void verificarLugares() {
    this.pais.verificarCantidadDeLugares();
  }
  
  public boolean verificarQueNoExistaPaisConIgualNombre() {
    return Mapamundi.obtenerInstancia().existePais(this.pais);
  }
  
  @Pure
  public Pais getPais() {
    return this.pais;
  }
  
  public void setPais(final Pais pais) {
    this.pais = pais;
  }
  
  @Pure
  public String getNombre() {
    return this.nombre;
  }
  
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }
  
  @Pure
  public String getCaracteristicaSeleccionada() {
    return this.caracteristicaSeleccionada;
  }
  
  public void setCaracteristicaSeleccionada(final String caracteristicaSeleccionada) {
    this.caracteristicaSeleccionada = caracteristicaSeleccionada;
  }
  
  @Pure
  public String getNuevaCaracteristica() {
    return this.nuevaCaracteristica;
  }
  
  public void setNuevaCaracteristica(final String nuevaCaracteristica) {
    this.nuevaCaracteristica = nuevaCaracteristica;
  }
  
  @Pure
  public Pais getConexionSeleccionada() {
    return this.conexionSeleccionada;
  }
  
  public void setConexionSeleccionada(final Pais conexionSeleccionada) {
    this.conexionSeleccionada = conexionSeleccionada;
  }
  
  @Pure
  public Pais getNuevaConexion() {
    return this.nuevaConexion;
  }
  
  public void setNuevaConexion(final Pais nuevaConexion) {
    this.nuevaConexion = nuevaConexion;
  }
  
  @Pure
  public Lugar getLugarSeleccionada() {
    return this.lugarSeleccionada;
  }
  
  public void setLugarSeleccionada(final Lugar lugarSeleccionada) {
    this.lugarSeleccionada = lugarSeleccionada;
  }
  
  @Pure
  public Lugar getNuevoLugar() {
    return this.nuevoLugar;
  }
  
  public void setNuevoLugar(final Lugar nuevoLugar) {
    this.nuevoLugar = nuevoLugar;
  }
}
