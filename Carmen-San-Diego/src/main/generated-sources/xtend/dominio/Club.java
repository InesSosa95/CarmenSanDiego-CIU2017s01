package dominio;

import com.google.common.collect.Iterables;
import dominio.Cuidador;
import dominio.Informante;
import dominio.Lugar;
import dominio.Villano;
import java.util.Random;
import org.eclipse.xtend.lib.annotations.Accessors;

@Accessors
@SuppressWarnings("all")
public class Club extends Lugar {
  public Club() {
    Cuidador _cuidador = new Cuidador();
    this.setOcupante(_cuidador);
    this.setNombreDelLugar("Club");
  }
  
  public void dejarSenhasDe(final Villano villano) {
    Informante _informante = new Informante();
    this.setOcupante(_informante);
    this.getOcupante().reiniciarInformes();
    Iterables.<String>addAll(this.getOcupante().getInforme(), villano.getSenhas(2));
    double _nextDouble = new Random().nextDouble();
    boolean _greaterThan = (_nextDouble > 0.30);
    if (_greaterThan) {
      Iterables.<String>addAll(this.getOcupante().getInforme(), villano.getPistasDeHobbies(1));
    }
  }
}
