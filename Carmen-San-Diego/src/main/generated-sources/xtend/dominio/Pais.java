package dominio;

import com.google.common.base.Objects;
import dominio.Cuidador;
import dominio.Lugar;
import dominio.Mapamundi;
import dominio.Villano;
import java.util.List;
import org.eclipse.xtend.lib.annotations.Accessors;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Pure;
import org.uqbar.commons.model.UserException;
import org.uqbar.commons.utils.Observable;

@Observable
@Accessors
@SuppressWarnings("all")
public class Pais {
  private String nombre;
  
  private List<String> caracteristicas = CollectionLiterals.<String>newArrayList();
  
  private List<Pais> conexiones = CollectionLiterals.<Pais>newArrayList();
  
  public List<Lugar> lugares = CollectionLiterals.<Lugar>newArrayList();
  
  private Cuidador ocupante;
  
  private List<String> conexionesComoString;
  
  public Pais(final String unNombre, final List<String> unasCaracteristicas, final List<Pais> unasConexiones, final List<Lugar> unosLugares) {
    this();
    this.nombre = unNombre;
    this.caracteristicas = unasCaracteristicas;
    this.conexiones = unasConexiones;
    this.lugares = unosLugares;
    this.verificarCantidadDeLugares();
    this.verificarNombreValido();
    Mapamundi.obtenerInstancia().agregarPais(this);
  }
  
  public Pais() {
    this.nombre = "nuevoPais";
    Cuidador _cuidador = new Cuidador();
    this.ocupante = _cuidador;
  }
  
  public void verificarNombreValido() {
    if ((this.nombre == null)) {
      throw new UserException("El pais debe tener un nombre valido");
    }
  }
  
  public void verificarCantidadDeLugares() {
    if (((this.lugares.size() != 3) || (this.lugares == null))) {
      throw new UserException("La cantidad de lugares es incorrecto (cantidad correcta:3)");
    }
  }
  
  public boolean agregarCaracteristica(final String caracteristica) {
    boolean _xifexpression = false;
    boolean _contains = this.caracteristicas.contains(caracteristica);
    if (_contains) {
      throw new UserException("Ya existe esa caracteristica");
    } else {
      _xifexpression = this.caracteristicas.add(caracteristica);
    }
    return _xifexpression;
  }
  
  public String toString() {
    return this.nombre;
  }
  
  public boolean quitarCaracteristica(final String caracteristica) {
    return this.caracteristicas.remove(caracteristica);
  }
  
  public boolean agregarConexion(final Pais unaConexion) {
    boolean _xblockexpression = false;
    {
      this.verificarQueElPaisEsteEnElMapamundi(unaConexion);
      this.evitarDuplicacionDeConexiones(unaConexion);
      _xblockexpression = this.conexiones.add(unaConexion);
    }
    return _xblockexpression;
  }
  
  public void evitarDuplicacionDeConexiones(final Pais unaConexion) {
    boolean _contains = this.conexiones.contains(unaConexion);
    if (_contains) {
      throw new UserException("Ya existe una conexión con ese país");
    }
  }
  
  public void verificarQueElPaisEsteEnElMapamundi(final Pais unPais) {
    final Mapamundi mapamundi = Mapamundi.obtenerInstancia();
    boolean _existePais = mapamundi.existePais(unPais);
    boolean _not = (!_existePais);
    if (_not) {
      throw new UserException("El pais no esta guardado en el mapamundi");
    }
  }
  
  public boolean quitarConexion(final Pais unaConexion) {
    return this.conexiones.remove(unaConexion);
  }
  
  public boolean agregarLugar(final Lugar unLugar) {
    boolean _xblockexpression = false;
    {
      this.verificarQueNoSeaNull(unLugar);
      this.verificarEspacioDisponible();
      this.evitarDuplicacionDeLugares(unLugar);
      _xblockexpression = this.lugares.add(unLugar);
    }
    return _xblockexpression;
  }
  
  private void verificarQueNoSeaNull(final Object objeto) {
    if ((objeto == null)) {
      throw new UserException("El lugar no puede ser vacio!");
    }
  }
  
  private void evitarDuplicacionDeLugares(final Lugar unLugar) {
    boolean _contains = this.lugares.contains(unLugar);
    if (_contains) {
      throw new UserException("Ya existe ese Lugar");
    }
  }
  
  private void verificarEspacioDisponible() {
    int _size = this.lugares.size();
    boolean _equals = (_size == 3);
    if (_equals) {
      throw new UserException("No se puede agregar mas de 3 lugares");
    }
  }
  
  public boolean quitarLugar(final Lugar unLugar) {
    return this.lugares.remove(unLugar);
  }
  
  public boolean estaConectadoCon(final Pais otroPais) {
    return this.conexiones.contains(otroPais);
  }
  
  public boolean equals(final Pais unPais) {
    String _nombre = unPais.getNombre();
    String _nombre_1 = this.getNombre();
    return Objects.equal(_nombre, _nombre_1);
  }
  
  public void seteaTusLugares(final Villano unVillano) {
    for (final Lugar u : this.lugares) {
      u.dejarSenhasDe(unVillano);
    }
  }
  
  public void dejarPistasFinales(final Villano unVillano) {
    for (final Lugar l : this.lugares) {
      l.dejarPistasFinales();
    }
    Lugar _get = this.lugares.get(2);
    _get.setOcupante(unVillano);
  }
  
  @Pure
  public String getNombre() {
    return this.nombre;
  }
  
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }
  
  @Pure
  public List<String> getCaracteristicas() {
    return this.caracteristicas;
  }
  
  public void setCaracteristicas(final List<String> caracteristicas) {
    this.caracteristicas = caracteristicas;
  }
  
  @Pure
  public List<Pais> getConexiones() {
    return this.conexiones;
  }
  
  public void setConexiones(final List<Pais> conexiones) {
    this.conexiones = conexiones;
  }
  
  @Pure
  public List<Lugar> getLugares() {
    return this.lugares;
  }
  
  public void setLugares(final List<Lugar> lugares) {
    this.lugares = lugares;
  }
  
  @Pure
  public Cuidador getOcupante() {
    return this.ocupante;
  }
  
  public void setOcupante(final Cuidador ocupante) {
    this.ocupante = ocupante;
  }
  
  @Pure
  public List<String> getConexionesComoString() {
    return this.conexionesComoString;
  }
  
  public void setConexionesComoString(final List<String> conexionesComoString) {
    this.conexionesComoString = conexionesComoString;
  }
}
