package dominio;

import com.google.common.base.Objects;
import dominio.Pais;
import dominio.Villano;
import java.util.List;
import org.eclipse.xtend.lib.annotations.Accessors;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Pure;
import org.uqbar.commons.model.UserException;
import org.uqbar.commons.utils.Observable;

@Accessors
@Observable
@SuppressWarnings("all")
public class Caso {
  private String nombreDelCaso;
  
  private Villano responsable;
  
  private List<Pais> recorridoCriminal;
  
  private String reporte;
  
  private String objeto;
  
  private Pais paisDelHecho;
  
  private List<Pais> destinosFallidos;
  
  private Villano ordenDeArresto;
  
  private static Caso instancia = null;
  
  private Caso() {
  }
  
  private static Caso crearInstancia() {
    Caso _xifexpression = null;
    if ((Caso.instancia == null)) {
      Caso _caso = new Caso();
      _xifexpression = Caso.instancia = _caso;
    }
    return _xifexpression;
  }
  
  public static Caso obtenerInstancia() {
    if ((Caso.instancia == null)) {
      Caso.crearInstancia();
    }
    return Caso.instancia;
  }
  
  public boolean setearInstancia(final Villano unVillano, final String unReporte, final String unObjeto, final Pais unPaisDelHecho, final String nombreCaso) {
    boolean _xblockexpression = false;
    {
      this.responsable = unVillano;
      this.reporte = unReporte;
      this.objeto = unObjeto;
      this.paisDelHecho = unPaisDelHecho;
      this.nombreDelCaso = nombreCaso;
      this.recorridoCriminal = CollectionLiterals.<Pais>newArrayList();
      this.destinosFallidos = CollectionLiterals.<Pais>newArrayList();
      _xblockexpression = this.recorridoCriminal.add(unPaisDelHecho);
    }
    return _xblockexpression;
  }
  
  public boolean agregarPaisAPlanDeEscape(final Pais pais) {
    boolean _xifexpression = false;
    boolean _contains = this.responsable.planDeEscape.contains(pais);
    if (_contains) {
      throw new UserException("Ya existe ese pais en la ruta de escape");
    } else {
      _xifexpression = this.responsable.planDeEscape.add(pais);
    }
    return _xifexpression;
  }
  
  public boolean estaResuelto(final Villano capturado) {
    return Objects.equal(this.responsable, capturado);
  }
  
  public boolean agregarDestinoFallido(final Pais unPais) {
    return this.destinosFallidos.add(unPais);
  }
  
  public Pais getPaisDelHecho() {
    return this.paisDelHecho;
  }
  
  public Villano emitirOrdenDeArresto(final Villano unVillano) {
    return this.ordenDeArresto = unVillano;
  }
  
  public boolean agregarVisita(final Pais pais) {
    boolean _xifexpression = false;
    boolean _pasoPor = this.responsable.pasoPor(pais);
    if (_pasoPor) {
      _xifexpression = this.recorridoCriminal.add(pais);
    } else {
      _xifexpression = this.destinosFallidos.add(pais);
    }
    return _xifexpression;
  }
  
  @Pure
  public String getNombreDelCaso() {
    return this.nombreDelCaso;
  }
  
  public void setNombreDelCaso(final String nombreDelCaso) {
    this.nombreDelCaso = nombreDelCaso;
  }
  
  @Pure
  public Villano getResponsable() {
    return this.responsable;
  }
  
  public void setResponsable(final Villano responsable) {
    this.responsable = responsable;
  }
  
  @Pure
  public List<Pais> getRecorridoCriminal() {
    return this.recorridoCriminal;
  }
  
  public void setRecorridoCriminal(final List<Pais> recorridoCriminal) {
    this.recorridoCriminal = recorridoCriminal;
  }
  
  @Pure
  public String getReporte() {
    return this.reporte;
  }
  
  public void setReporte(final String reporte) {
    this.reporte = reporte;
  }
  
  @Pure
  public String getObjeto() {
    return this.objeto;
  }
  
  public void setObjeto(final String objeto) {
    this.objeto = objeto;
  }
  
  public void setPaisDelHecho(final Pais paisDelHecho) {
    this.paisDelHecho = paisDelHecho;
  }
  
  @Pure
  public List<Pais> getDestinosFallidos() {
    return this.destinosFallidos;
  }
  
  public void setDestinosFallidos(final List<Pais> destinosFallidos) {
    this.destinosFallidos = destinosFallidos;
  }
  
  @Pure
  public Villano getOrdenDeArresto() {
    return this.ordenDeArresto;
  }
  
  public void setOrdenDeArresto(final Villano ordenDeArresto) {
    this.ordenDeArresto = ordenDeArresto;
  }
}
