package dominio;

import dominio.Informante;
import dominio.Ocupante;
import dominio.Villano;
import java.util.List;
import org.eclipse.xtend.lib.annotations.Accessors;
import org.eclipse.xtext.xbase.lib.Pure;
import org.uqbar.commons.utils.Observable;

@Accessors
@Observable
@SuppressWarnings("all")
public abstract class Lugar {
  private Ocupante ocupante;
  
  private String nombreDelLugar;
  
  public abstract void dejarSenhasDe(final Villano villano);
  
  public void dejarPistasFinales() {
    Informante _informante = new Informante();
    this.ocupante = _informante;
    this.ocupante.reiniciarInformes();
    this.ocupante.addPista("juro q es medicin... ah! no el villano está cerca");
  }
  
  public List<String> damePistas() {
    return this.ocupante.getInforme();
  }
  
  @Pure
  public Ocupante getOcupante() {
    return this.ocupante;
  }
  
  public void setOcupante(final Ocupante ocupante) {
    this.ocupante = ocupante;
  }
  
  @Pure
  public String getNombreDelLugar() {
    return this.nombreDelLugar;
  }
  
  public void setNombreDelLugar(final String nombreDelLugar) {
    this.nombreDelLugar = nombreDelLugar;
  }
}
