package dominio;

import com.google.common.base.Objects;
import dominio.Expediente;
import dominio.Lugar;
import dominio.Ocupante;
import dominio.Pais;
import dominio.Sexo;
import java.util.List;
import org.eclipse.xtend.lib.annotations.Accessors;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.Pure;
import org.uqbar.commons.utils.Observable;

@Accessors
@Observable
@SuppressWarnings("all")
public class Villano extends Ocupante {
  private String nombre;
  
  private Sexo sexo;
  
  private List<String> senhas = CollectionLiterals.<String>newArrayList();
  
  private List<String> hobbies = CollectionLiterals.<String>newArrayList();
  
  public List<Pais> planDeEscape = CollectionLiterals.<Pais>newArrayList();
  
  private Pais paisActual;
  
  public Villano(final String suNombre, final List<Pais> unPlanDeEscape, final Sexo suSexo, final List<String> susSenhas, final List<String> susPasatiempos) {
    this.nombre = suNombre;
    this.sexo = suSexo;
    this.senhas = susSenhas;
    this.hobbies = susPasatiempos;
    this.planDeEscape = unPlanDeEscape;
    this.paisActual = IterableExtensions.<Pais>head(unPlanDeEscape);
    Expediente.obtenerInstancia().agregarVillano(this);
    this.setInforme(CollectionLiterals.<String>newArrayList());
    this.getInforme().add("Tengo un tenedor en la mano y mis espinas son dignas de un cactus");
  }
  
  public Villano() {
  }
  
  public boolean agregarSenha(final String unaSenha) {
    return this.senhas.add(unaSenha);
  }
  
  public boolean quitarSenha(final String unaSenha) {
    return this.senhas.remove(unaSenha);
  }
  
  public boolean agregarHobbie(final String unHobbie) {
    return this.hobbies.add(unHobbie);
  }
  
  public boolean quitarHobbie(final String unHobbie) {
    return this.hobbies.remove(unHobbie);
  }
  
  public boolean equals(final Villano unVillano) {
    String _nombre = unVillano.getNombre();
    String _nombre_1 = this.getNombre();
    return Objects.equal(_nombre, _nombre_1);
  }
  
  public List<Pais> cambiarPlanDeEscape(final List<Pais> paises) {
    return this.planDeEscape = paises;
  }
  
  public void visitar(final Lugar unLugar) {
    unLugar.dejarSenhasDe(this);
  }
  
  public Iterable<String> getSenhas(final int i) {
    return IterableExtensions.<String>take(this.senhas, i);
  }
  
  public Iterable<String> getPistasDePais(final Pais unPais, final int i) {
    return IterableExtensions.<String>take(this.proximoPais(this.planDeEscape, this.paisActual).getCaracteristicas(), i);
  }
  
  public Pais proximoPais(final List<Pais> unaListaDePaises, final Pais unPais) {
    int _indexOf = unaListaDePaises.indexOf(unPais);
    int idProximoPais = (_indexOf + 1);
    return unaListaDePaises.get(idProximoPais);
  }
  
  public Iterable<String> getPistasDeHobbies(final int i) {
    return IterableExtensions.<String>take(this.hobbies, i);
  }
  
  public void visitarLugarDePaisFinal(final Lugar unLugar) {
    unLugar.dejarPistasFinales();
  }
  
  public void iniciarEscape() {
    this.recorroPaisesDelPlanDeEscape();
    this.recorroUltimoPaisDePlanDeEscape();
  }
  
  private void recorroUltimoPaisDePlanDeEscape() {
    IterableExtensions.<Pais>last(this.planDeEscape).dejarPistasFinales(this);
  }
  
  private void recorroPaisesDelPlanDeEscape() {
    for (final Pais pais : this.planDeEscape) {
      pais.seteaTusLugares(this);
    }
  }
  
  public boolean pasoPor(final Pais pais) {
    return this.planDeEscape.contains(pais);
  }
  
  @Pure
  public String getNombre() {
    return this.nombre;
  }
  
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }
  
  @Pure
  public Sexo getSexo() {
    return this.sexo;
  }
  
  public void setSexo(final Sexo sexo) {
    this.sexo = sexo;
  }
  
  @Pure
  public List<String> getSenhas() {
    return this.senhas;
  }
  
  public void setSenhas(final List<String> senhas) {
    this.senhas = senhas;
  }
  
  @Pure
  public List<String> getHobbies() {
    return this.hobbies;
  }
  
  public void setHobbies(final List<String> hobbies) {
    this.hobbies = hobbies;
  }
  
  @Pure
  public List<Pais> getPlanDeEscape() {
    return this.planDeEscape;
  }
  
  public void setPlanDeEscape(final List<Pais> planDeEscape) {
    this.planDeEscape = planDeEscape;
  }
  
  @Pure
  public Pais getPaisActual() {
    return this.paisActual;
  }
  
  public void setPaisActual(final Pais paisActual) {
    this.paisActual = paisActual;
  }
}
