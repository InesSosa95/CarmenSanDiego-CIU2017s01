package dominio;

import com.google.common.collect.Iterables;
import dominio.Cuidador;
import dominio.Informante;
import dominio.Lugar;
import dominio.Villano;
import java.util.Random;
import org.eclipse.xtend.lib.annotations.Accessors;
import org.uqbar.commons.utils.Observable;

@Accessors
@Observable
@SuppressWarnings("all")
public class Biblioteca extends Lugar {
  public Biblioteca() {
    Cuidador _cuidador = new Cuidador();
    this.setOcupante(_cuidador);
    this.setNombreDelLugar("Biblioteca");
  }
  
  public void dejarSenhasDe(final Villano villano) {
    Informante _informante = new Informante();
    this.setOcupante(_informante);
    double _nextDouble = new Random().nextDouble();
    boolean _greaterThan = (_nextDouble > 0.50);
    if (_greaterThan) {
      Iterables.<String>addAll(this.getOcupante().getInforme(), villano.getPistasDeHobbies(1));
    }
    Iterables.<String>addAll(this.getOcupante().getInforme(), villano.getSenhas(1));
    Iterables.<String>addAll(this.getOcupante().getInforme(), villano.getPistasDePais(villano.getPaisActual(), 1));
  }
}
