package dominio;

import com.google.common.base.Objects;
import dominio.Pais;
import java.util.List;
import java.util.function.Consumer;
import org.apache.commons.lang.StringUtils;
import org.eclipse.xtend.lib.annotations.Accessors;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.ListExtensions;
import org.eclipse.xtext.xbase.lib.Pure;
import org.uqbar.commons.model.ObservableUtils;
import org.uqbar.commons.model.UserException;
import org.uqbar.commons.utils.Observable;

@Accessors
@Observable
@SuppressWarnings("all")
public class Mapamundi {
  private List<Pais> paises = CollectionLiterals.<Pais>newArrayList();
  
  private static Mapamundi instancia = null;
  
  private Mapamundi() {
  }
  
  private static Mapamundi crearInstancia() {
    Mapamundi _xifexpression = null;
    if ((Mapamundi.instancia == null)) {
      Mapamundi _mapamundi = new Mapamundi();
      _xifexpression = Mapamundi.instancia = _mapamundi;
    }
    return _xifexpression;
  }
  
  public static Mapamundi obtenerInstancia() {
    if ((Mapamundi.instancia == null)) {
      Mapamundi.crearInstancia();
    }
    return Mapamundi.instancia;
  }
  
  public void agregarPais(final Pais unPais) {
    boolean _contains = this.paises.contains(unPais);
    if (_contains) {
      throw new UserException("Ya existe ese Pais");
    } else {
      this.paises.add(unPais);
      ObservableUtils.firePropertyChanged(this, "paises");
    }
  }
  
  public void eliminarPais(final Pais unPais) {
    this.eliminarPaisDeConexiones(unPais);
    this.paises.remove(unPais);
    ObservableUtils.firePropertyChanged(this, "paises");
  }
  
  public void eliminarPaisDeConexiones(final Pais pais) {
    final Consumer<Pais> _function = new Consumer<Pais>() {
      public void accept(final Pais p) {
        p.quitarConexion(pais);
      }
    };
    this.paises.forEach(_function);
  }
  
  public boolean existePais(final Pais unPais) {
    return this.paises.contains(unPais);
  }
  
  public int cantidadDePaisesAgregados() {
    return this.paises.size();
  }
  
  public List<Pais> reiniciarMapamundi() {
    return this.paises = CollectionLiterals.<Pais>newArrayList();
  }
  
  public List<?> searchPaises(final String substring) {
    List<?> _xifexpression = null;
    boolean _isBlank = StringUtils.isBlank(substring);
    if (_isBlank) {
      final Function1<Pais, String> _function = new Function1<Pais, String>() {
        public String apply(final Pais it) {
          return it.getNombre();
        }
      };
      _xifexpression = ListExtensions.<Pais, String>map(this.paises, _function);
    } else {
      final Function1<Pais, Boolean> _function_1 = new Function1<Pais, Boolean>() {
        public Boolean apply(final Pais it) {
          return Boolean.valueOf(it.getNombre().toLowerCase().contains(substring.toLowerCase()));
        }
      };
      _xifexpression = IterableExtensions.<Pais>toList(IterableExtensions.<Pais>filter(this.paises, _function_1));
    }
    return _xifexpression;
  }
  
  public Pais searchPais(final String substring) {
    try {
      final Function1<Pais, Boolean> _function = new Function1<Pais, Boolean>() {
        public Boolean apply(final Pais it) {
          return Boolean.valueOf(it.getNombre().toLowerCase().contains(substring.toLowerCase()));
        }
      };
      return IterableExtensions.<Pais>head(IterableExtensions.<Pais>filter(this.paises, _function));
    } catch (final Throwable _t) {
      if (_t instanceof UserException) {
        final UserException ex = (UserException)_t;
        throw new UserException("El pais que intenta buscar no existe o ya lo destruyo el imperialismo yanky");
      } else {
        throw Exceptions.sneakyThrow(_t);
      }
    }
  }
  
  public Object borrarPais(final String nombreDelPais) {
    return null;
  }
  
  public Iterable<Pais> getPais(final String nombreDelPais) {
    final Function1<Pais, Boolean> _function = new Function1<Pais, Boolean>() {
      public Boolean apply(final Pais it) {
        String _nombre = it.getNombre();
        return Boolean.valueOf(Objects.equal(_nombre, nombreDelPais));
      }
    };
    return IterableExtensions.<Pais>filter(this.paises, _function);
  }
  
  @Pure
  public List<Pais> getPaises() {
    return this.paises;
  }
  
  public void setPaises(final List<Pais> paises) {
    this.paises = paises;
  }
}
