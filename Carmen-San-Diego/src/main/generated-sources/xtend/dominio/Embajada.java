package dominio;

import com.google.common.collect.Iterables;
import dominio.Cuidador;
import dominio.Informante;
import dominio.Lugar;
import dominio.Villano;

@SuppressWarnings("all")
public class Embajada extends Lugar {
  public Embajada() {
    Cuidador _cuidador = new Cuidador();
    this.setOcupante(_cuidador);
    this.setNombreDelLugar("Embajada");
  }
  
  public void dejarSenhasDe(final Villano villano) {
    Informante _informante = new Informante();
    this.setOcupante(_informante);
    Iterables.<String>addAll(this.getOcupante().getInforme(), villano.getPistasDePais(villano.getPaisActual(), 2));
  }
}
