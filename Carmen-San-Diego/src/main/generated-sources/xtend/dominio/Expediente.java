package dominio;

import dominio.Villano;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.eclipse.xtend.lib.annotations.Accessors;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.ListExtensions;
import org.eclipse.xtext.xbase.lib.Pure;
import org.uqbar.commons.model.UserException;
import org.uqbar.commons.utils.Observable;

@Accessors
@Observable
@SuppressWarnings("all")
public class Expediente {
  private List<Villano> villanos = CollectionLiterals.<Villano>newArrayList();
  
  private static Expediente instancia = null;
  
  private Expediente() {
  }
  
  private static Expediente crearInstancia() {
    Expediente _xifexpression = null;
    if ((Expediente.instancia == null)) {
      Expediente _expediente = new Expediente();
      _xifexpression = Expediente.instancia = _expediente;
    }
    return _xifexpression;
  }
  
  public static Expediente obtenerInstancia() {
    if ((Expediente.instancia == null)) {
      Expediente.crearInstancia();
    }
    return Expediente.instancia;
  }
  
  public boolean agregarVillano(final Villano unVillano) {
    boolean _xifexpression = false;
    boolean _contains = this.villanos.contains(unVillano);
    if (_contains) {
      throw new UserException("Ya existe un villano con ese nombre");
    } else {
      _xifexpression = this.villanos.add(unVillano);
    }
    return _xifexpression;
  }
  
  public boolean eliminarVillano(final Villano unVillano) {
    return this.villanos.remove(unVillano);
  }
  
  public boolean existeVillano(final Villano unVillano) {
    return this.villanos.contains(unVillano);
  }
  
  public int cantidadDeVillanosEnExpediente() {
    return this.villanos.size();
  }
  
  public List<Villano> reiniciarExpediente() {
    return this.villanos = CollectionLiterals.<Villano>newArrayList();
  }
  
  public boolean eliminarVillano(final String nombreDeVillano) {
    return this.eliminarVillano(this.getVillano(nombreDeVillano));
  }
  
  public Villano getVillano(final String substring) {
    try {
      final Function1<Villano, Boolean> _function = new Function1<Villano, Boolean>() {
        public Boolean apply(final Villano it) {
          return Boolean.valueOf(it.getNombre().toLowerCase().contains(substring.toLowerCase()));
        }
      };
      return IterableExtensions.<Villano>head(IterableExtensions.<Villano>filter(this.villanos, _function));
    } catch (final Throwable _t) {
      if (_t instanceof UserException) {
        final UserException ex = (UserException)_t;
        throw new UserException("El villano que intenta buscar no existe, ups");
      } else {
        throw Exceptions.sneakyThrow(_t);
      }
    }
  }
  
  public List<?> searchVillanos(final String substring) {
    List<?> _xifexpression = null;
    boolean _isBlank = StringUtils.isBlank(substring);
    if (_isBlank) {
      final Function1<Villano, String> _function = new Function1<Villano, String>() {
        public String apply(final Villano it) {
          return it.getNombre();
        }
      };
      _xifexpression = ListExtensions.<Villano, String>map(this.villanos, _function);
    } else {
      final Function1<Villano, Boolean> _function_1 = new Function1<Villano, Boolean>() {
        public Boolean apply(final Villano it) {
          return Boolean.valueOf(it.getNombre().toLowerCase().contains(substring.toLowerCase()));
        }
      };
      _xifexpression = IterableExtensions.<Villano>toList(IterableExtensions.<Villano>filter(this.villanos, _function_1));
    }
    return _xifexpression;
  }
  
  @Pure
  public List<Villano> getVillanos() {
    return this.villanos;
  }
  
  public void setVillanos(final List<Villano> villanos) {
    this.villanos = villanos;
  }
}
