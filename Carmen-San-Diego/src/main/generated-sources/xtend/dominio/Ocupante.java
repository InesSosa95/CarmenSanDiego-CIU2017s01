package dominio;

import java.util.List;
import org.eclipse.xtend.lib.annotations.Accessors;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Pure;
import org.uqbar.commons.utils.Observable;

@Accessors
@Observable
@SuppressWarnings("all")
public abstract class Ocupante {
  private List<String> informe;
  
  public Ocupante() {
    this.informe = CollectionLiterals.<String>newArrayList();
    this.informe.add("Por aquí no se ha visto ningún delincuente que no sea parte del gobierno");
  }
  
  public List<String> reiniciarInformes() {
    return this.informe = CollectionLiterals.<String>newArrayList();
  }
  
  public void addPista(final String string) {
    this.informe.add(string);
  }
  
  @Pure
  public List<String> getInforme() {
    return this.informe;
  }
  
  public void setInforme(final List<String> informe) {
    this.informe = informe;
  }
}
