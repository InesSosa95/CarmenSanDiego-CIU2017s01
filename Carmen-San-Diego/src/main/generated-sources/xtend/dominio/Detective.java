package dominio;

import dominio.Caso;
import dominio.Lugar;
import dominio.Pais;
import dominio.Sexo;
import dominio.Villano;
import java.util.List;
import org.eclipse.xtend.lib.annotations.Accessors;
import org.eclipse.xtext.xbase.lib.Pure;
import org.uqbar.commons.utils.Observable;

@Accessors
@Observable
@SuppressWarnings("all")
public class Detective {
  private String nombreDetective;
  
  private Caso casoAResolver;
  
  private Pais paisActual;
  
  private Sexo sexo;
  
  private int edad;
  
  private static Detective instancia = null;
  
  private Detective() {
  }
  
  private static Detective crearInstancia() {
    Detective _xifexpression = null;
    if ((Detective.instancia == null)) {
      Detective _detective = new Detective();
      _xifexpression = Detective.instancia = _detective;
    }
    return _xifexpression;
  }
  
  public static Detective obtenerInstancia() {
    if ((Detective.instancia == null)) {
      Detective.crearInstancia();
    }
    return Detective.instancia;
  }
  
  public List<String> pedirPistas(final Lugar unLugar) {
    return unLugar.damePistas();
  }
  
  public void setearDetective(final String nombreUsuario, final int unaEdad, final Sexo suSexo, final Caso unCaso) {
    this.nombreDetective = nombreUsuario;
    this.casoAResolver = Caso.obtenerInstancia();
    this.edad = unaEdad;
    this.sexo = suSexo;
    this.paisActual = this.casoAResolver.getPaisDelHecho();
  }
  
  public boolean viajarA(final Pais unPais) {
    boolean _xblockexpression = false;
    {
      this.paisActual = unPais;
      _xblockexpression = this.casoAResolver.agregarVisita(this.paisActual);
    }
    return _xblockexpression;
  }
  
  public Villano pedirOrdenDeArresto(final Villano unVillano) {
    return this.casoAResolver.emitirOrdenDeArresto(unVillano);
  }
  
  @Pure
  public String getNombreDetective() {
    return this.nombreDetective;
  }
  
  public void setNombreDetective(final String nombreDetective) {
    this.nombreDetective = nombreDetective;
  }
  
  @Pure
  public Caso getCasoAResolver() {
    return this.casoAResolver;
  }
  
  public void setCasoAResolver(final Caso casoAResolver) {
    this.casoAResolver = casoAResolver;
  }
  
  @Pure
  public Pais getPaisActual() {
    return this.paisActual;
  }
  
  public void setPaisActual(final Pais paisActual) {
    this.paisActual = paisActual;
  }
  
  @Pure
  public Sexo getSexo() {
    return this.sexo;
  }
  
  public void setSexo(final Sexo sexo) {
    this.sexo = sexo;
  }
  
  @Pure
  public int getEdad() {
    return this.edad;
  }
  
  public void setEdad(final int edad) {
    this.edad = edad;
  }
}
