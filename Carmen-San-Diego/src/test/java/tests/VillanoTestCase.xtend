package tests

import static org.junit.Assert.*
import dominio.Sexo
import dominio.Villano
import org.junit.Before
import org.junit.Test

class VillanoTestsCase {

	var Villano villano

	@Before def void setUp() {
		val femenino = Sexo.FEMENINO
		val señas = newArrayList("Tatto", "Rubia")
		val planDeEscape = newArrayList()
		val pasatiempos = newArrayList("Jugar Tenis", "Viajar en yate")
		villano = new Villano("Dazzle Annie", planDeEscape, femenino, señas, pasatiempos)
	}

	@Test def testValues() {
		val nombre = villano.nombre
		val sexo = villano.sexo
		val señas = villano.senhas
		val pasatiempos = villano.hobbies

		assertEquals(nombre, "Dazzle Annie")
		assertEquals(sexo, Sexo.FEMENINO)
		assertEquals(señas.size, 2)
		assertEquals(pasatiempos.size, 2)
	}

	@Test def testQuitarSeña() {
		val señas = villano.senhas
		señas.remove("Tatto")
		assertEquals(señas.size, 1)
		assertFalse(señas.contains("Tatto"))
	}

	@Test def testAgregarSeña() {
		val señas = villano.senhas
		señas.add("6 dedos en las manos")

		assertEquals(señas.size, 3)
		assertTrue(señas.contains("6 dedos en las manos"))
	}

	@Test def testAgregarHobbie() {
		val hobbies = villano.hobbies

		assertEquals(hobbies.size, 2)

		hobbies.add("Pintar cuadros")

		assertEquals(hobbies.size, 3)
		assertTrue(hobbies.contains("Pintar cuadros"))
	}

	@Test def testQuitarHobbie() {
		val hobbies = villano.hobbies

		assertEquals(hobbies.size, 2)

		hobbies.remove("Viajar en yate")

		assertEquals(hobbies.size, 1)
		assertFalse(hobbies.contains("Viajar en yate"))
	}
}
