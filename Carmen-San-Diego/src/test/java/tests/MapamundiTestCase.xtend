package tests


import dominio.Lugar
import dominio.Mapamundi
import dominio.Pais
import java.util.List
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.uqbar.commons.model.UserException

import static org.junit.Assert.*

class MapamundiTestCase {
	
	var Mapamundi america
	var List<Pais> listaDePaises 
	var List<Lugar> listaDeLugaresMock = Mockito.mock(newArrayList.class)
	var Pais argentina
	var Pais brasil
	var Pais colombia
	var Pais uruguay
	var Pais venezuela
	var Pais bolivia

	@Before def void SetUp() {
		MockitoAnnotations.initMocks(this); 
		Mockito.when(listaDeLugaresMock.size).thenReturn(3)	
		argentina = new Pais("argentina", newArrayList, listaDePaises, listaDeLugaresMock)
		brasil = new Pais("brasil", newArrayList, listaDePaises, listaDeLugaresMock)
		bolivia = new Pais("bolivia", newArrayList, listaDePaises, listaDeLugaresMock)
		colombia = new Pais("colombia", newArrayList, listaDePaises, listaDeLugaresMock)
		uruguay = new Pais("uruguay", newArrayList, listaDePaises, listaDeLugaresMock)
		venezuela = new Pais("venezuela", newArrayList, listaDePaises, listaDeLugaresMock)
		america = Mapamundi.obtenerInstancia()
		listaDePaises = newArrayList()
		listaDePaises.add(argentina)
		listaDePaises.add(bolivia)
		listaDePaises.add(colombia)
		listaDePaises.add(uruguay)
		listaDePaises.add(venezuela)
	}

	@Test
	def test01_CrearMapamundi() {
		america.setPaises(listaDePaises)
		assertEquals(america.getPaises.size, 5)
		assertEquals(america.paises.findFirst[Pais p|p.nombre == "argentina"].nombre, "argentina")
		assertEquals(america.paises.findFirst[Pais p|p.nombre == "bolivia"].nombre, "bolivia")
		assertEquals(america.paises.findFirst[Pais p|p.nombre == "colombia"].nombre, "colombia")
		assertEquals(america.paises.findFirst[Pais p|p.nombre == "uruguay"].nombre, "uruguay")
		assertEquals(america.paises.findFirst[Pais p|p.nombre == "venezuela"].nombre, "venezuela")
		assertNull(america.paises.findFirst[Pais p|p.nombre == "brasil"])
	}

	/*@Test
	def test02_EliminarArgentinaAlMapamundi() {
		america.setPaises(listaDePaises)
		assertEquals(america.paises.size, 5)
		assertEquals(america.paises.findFirst[Pais p|p.nombre == "argentina"].nombre, "argentina")
		america.eliminarPais(argentina)
		assertEquals(america.paises.size, 4)
		assertNull(america.paises.findFirst[Pais p|p.nombre == "argentina"])
	}*/

	@Test(expected=UserException)
	def void test03_AGregarPaisQueYaExisteEnElMapamundi() {
		america.setPaises(listaDePaises)
		america.agregarPais(argentina)
	}

	@Test // es igual al anterior pero mas bonito
	def void test04_AGregarPaisQueYaExisteEnElMapamundi() {
		america.setPaises(listaDePaises)
		try {
			america.agregarPais(argentina)
		} catch (UserException e)
			assertEquals(america.paises.size, 5)
	}

}
