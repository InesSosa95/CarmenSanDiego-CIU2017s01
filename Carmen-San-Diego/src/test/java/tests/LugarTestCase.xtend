package tests

import dominio.Pais
import org.junit.*
import static org.junit.Assert.*
import dominio.Biblioteca
import dominio.Lugar
import dominio.Banco
import dominio.Embajada
import dominio.Club
import dominio.Villano
import dominio.Cuidador
import dominio.Informante
import dominio.Sexo
import java.util.List
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import java.util.Random

class LugarTestCase {
	var Banco bbva
	var Biblioteca biblioteca
	var Embajada embajada
	var Club club
	var Villano riddleyTom
	var Pais argentina
	var Pais brasil
	var List<Pais> unPlanDeEscape = newArrayList
	var List<String> caracteristicasDeArgentina = newArrayList("su bandera es celeste y blanca",
		"tiene un protestodromo en Rivadavia y Entre Rios")
	var List<String> caracteristicasDeBrasil = newArrayList("su maximo idolo debuto con un pibe", "ordem e progreso")
	var List<Pais> conexiones = newArrayList()
	var List<Pais> conexiones1 = newArrayList()
	var List<Lugar> lugares = newArrayList()
	val senhas = newArrayList("sin nariz", "pelado", "varita de sauce")
	var List<String> pasatiempos = newArrayList("matar a Harry", "hacer magia", "acaricia su boa constrictora")

	@Before def void SetUp() {
		conexiones.add(brasil)
		conexiones1.add(argentina)
		lugares.add(bbva)
		lugares.add(biblioteca)
		lugares.add(embajada)
		argentina = new Pais('Argentina', caracteristicasDeArgentina, conexiones, lugares)
		brasil = new Pais('Brasil', caracteristicasDeBrasil, conexiones1, lugares)
		unPlanDeEscape.add(argentina)
		unPlanDeEscape.add(brasil)
		bbva = new Banco()
		biblioteca = new Biblioteca()
		embajada = new Embajada()
		club = new Club()
		riddleyTom = new Villano('Tom Riddley', unPlanDeEscape, Sexo.MASCULINO, senhas, pasatiempos)

	}

	@Test
	def test01_CreoUnBancoYPidoUnInformeAlOcupante() {

		assertEquals(bbva.getOcupante().getInforme().head(),
			'Por aquí no se ha visto ningún delincuente que no sea parte del gobierno')
	}

	@Test
	def test02_UnVillanoVisitaElBancoYPidoUnInformeAlOcupante() {
		riddleyTom.visitar(bbva)
		assertEquals(bbva.getOcupante().getInforme().size(), 2)
		assertTrue(bbva.getOcupante().getInforme().contains("su maximo idolo debuto con un pibe"))
		assertTrue(bbva.getOcupante().getInforme().contains("sin nariz"))
	}

	@Test
	def test03_UnVillanoVisitaUnaBibliotecaYPidoUnInformeAlOcupante() {
		riddleyTom.visitar(biblioteca)
		assertEquals(biblioteca.getOcupante().getInforme().size(), 2.5, 0.5)
	}

	@Test
	def test04_UnVillanoVisitaUnaEmbajadaYPidoUnInformeAlOcupante() {
		riddleyTom.visitar(embajada)
		assertEquals(embajada.getOcupante().getInforme().size(), 1.5, 0.5)
	}

	@Test
	def test05_UnVillanoVisitaUnClubYPidoUnInformeAlOcupante() {
		riddleyTom.visitar(club)
		assertEquals(club.getOcupante().getInforme().size(), 2.50, 0.5)
	}
}
