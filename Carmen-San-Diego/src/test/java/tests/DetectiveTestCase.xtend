package tests

import dominio.Caso
import dominio.Detective
import dominio.Pais
import dominio.Sexo
import java.util.ArrayList
import java.util.List
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

import static org.junit.Assert.*

class DetectiveTestCase {

	var Detective sherlock
	var Sexo masculino = Sexo.MASCULINO
	var Caso casoMock = Mockito.mock(Caso)
	var Pais argentinaMock = Mockito.mock(Pais)


	@Before def void SetUp() {

		sherlock = Detective.obtenerInstancia
		MockitoAnnotations.initMocks(this);
		Mockito.when(casoMock.getPaisDelHecho).thenReturn(argentinaMock)
	}

	@Test
	def test01_CrearDetective() {

		var Detective unDetective
		unDetective = Detective.obtenerInstancia
		unDetective.setearDetective('Jacques Clouseau', 50, masculino, casoMock)
		assertEquals(unDetective.nombreDetective, 'Jacques Clouseau')
		assertEquals(unDetective.edad, 50)
		assertEquals(unDetective.casoAResolver, casoMock)
		assertEquals(unDetective.getPaisActual, argentinaMock)
	}

}
