package tests

import dominio.Pais
import org.junit.*
import java.util.List
import static org.junit.Assert.*
import dominio.Biblioteca
import dominio.Lugar
import dominio.Banco
import dominio.Embajada
import dominio.Club
import org.uqbar.commons.model.UserException
import dominio.Mapamundi

class PaisTestCase {
	var Pais paisTest
	var List<Pais> listaDePaises
	var Pais argentina
	var Pais brasil
	var Pais bolivia
	var List<String> listaVaciaCarac
	var List<Lugar> listaVaciaLugar
	var List<Lugar> lista3Lugares
	var List<Lugar> lista4Lugares
	var Lugar biblioteca
	var Lugar banco
	var Lugar embajada
	var Lugar club

	@Before def void SetUp() {
		biblioteca = new Biblioteca()
		banco = new Banco()
		embajada = new Embajada()
		club = new Club()

		listaDePaises = newArrayList
		listaVaciaCarac = newArrayList
		listaVaciaLugar = newArrayList

		lista3Lugares = newArrayList
		lista3Lugares.add(banco)
		lista3Lugares.add(embajada)
		lista3Lugares.add(biblioteca)

		lista4Lugares = newArrayList
		lista4Lugares.add(banco)
		lista4Lugares.add(embajada)
		lista4Lugares.add(biblioteca)
		lista4Lugares.add(club)

		argentina = new Pais("argentina", listaVaciaCarac, listaDePaises, lista3Lugares)
		listaDePaises.add(argentina)
		brasil = new Pais("brasil", listaVaciaCarac, listaDePaises, lista3Lugares)
		listaDePaises.add(brasil)
		bolivia = new Pais("bolivia", listaVaciaCarac, listaDePaises, lista3Lugares)
		listaDePaises.add(bolivia)
	}

	@Test
	def test01_CrearPais() {
		var caracteristicas = newArrayList
		caracteristicas.add("bonito")
		caracteristicas.add("bananero")
		paisTest = new Pais("venezuela", caracteristicas, listaDePaises, lista3Lugares)
		assertEquals(paisTest.getNombre, "venezuela")
	}

	@Test
	def void test02_SacarEmbajadaYAgregarUnClubAPais() {
		var Club club = new Club()
		argentina.quitarLugar(embajada)
		argentina.agregarLugar(club)

	}

	@Test(expected=UserException)
	def void test03_NoCrearPaisSinCantidadCorrectaDeLugares() {
		var Pais prueba
		prueba = new Pais("Venezuela", listaVaciaCarac, listaDePaises, listaVaciaLugar)
	}

	@Test(expected=UserException)
	def void test04_NoCrearPaisSinNombre() {
		var Pais prueba
		prueba = new Pais(null, listaVaciaCarac, listaDePaises, lista3Lugares)
	}

	@Test(expected=UserException)
	def void test05_NoAgregarCaracteristicaRepetida() {
		argentina.agregarCaracteristica("banana")
		argentina.agregarCaracteristica("banana")
	}

	@Test(expected=UserException)
	def void test06_NoAgregarConexionRepetida() {
		argentina.agregarConexion(bolivia)
		argentina.agregarConexion(bolivia)
	}

	@Test(expected=UserException)
	def void test07_NoPoderAgregarDosLugaresIguales() {
		argentina.quitarLugar(embajada)
		argentina.agregarLugar(biblioteca)
	}

	@Test(expected=UserException)
	def void test08_NoPoderCrearUnPaisCon4Lugares() {
		var Pais prueba
		prueba = new Pais("Venezuela", listaVaciaCarac, listaDePaises, lista4Lugares)
	}
}
